package kacper.barszczewski.utils;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import utils.MapBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class UtilsTest {

    @Test
    void getMapValue() {
        Map<String, Object> map = new MapBuilder()
                .put("test", "text")
                .put("obj1", new MapBuilder()
                        .put("test2", "text2")
                        .build())
                .put("array1", Arrays.asList(
                        new MapBuilder()
                                .put("a1", "a11")
                                .put("a2", "a12")
                                .build(),
                        new MapBuilder()
                                .put("a1", "a21")
                                .put("a2", "a22")
                                .build()
                ))
                .build();
        List<Object> mapValue = Utils.getMapValue(map, "array1.a1");
        Assert.assertEquals("a11", mapValue.get(0));
        Assert.assertEquals("a21", mapValue.get(1));
    }

    @Test
    void getMapValue2() {
        Map<String, Object> map = new MapBuilder()
                .put("test", "text")
                .put("obj1", new MapBuilder()
                        .put("test2", "text2")
                        .build())
                .put("array1", Arrays.asList(
                        new MapBuilder()
                                .put("a1", "a11")
                                .put("a2", "a12")
                                .build(),
                        new MapBuilder()
                                .put("a1", "a21")
                                .put("a2", "a22")
                                .build()
                ))
                .put("obj2", new MapBuilder()
                        .put("array2", Arrays.asList(
                                Arrays.asList(
                                        new MapBuilder()
                                                .put("key1", "test1")
                                                .put("key2", "test2")
                                                .build(),
                                        new MapBuilder()
                                                .put("key1", "test11")
                                                .put("key2", "test12")
                                                .build()
                                ),
                                Arrays.asList(
                                        new MapBuilder()
                                                .put("key1", "test21")
                                                .put("key2", "test22")
                                                .build(),
                                        new MapBuilder()
                                                .put("key1", "test31")
                                                .put("key2", "test32")
                                                .build()
                                )
                        ))
                        .build())
                .build();
        List<Object> mapValue = Utils.getMapValue(map, "obj2.array2.key1");
        Assert.assertEquals("test1", mapValue.get(0));
        Assert.assertEquals("test11", mapValue.get(1));
        Assert.assertEquals("test21", mapValue.get(2));
        Assert.assertEquals("test31", mapValue.get(3));
        Assert.assertEquals(4, mapValue.size());
    }

    @Test
    void putMapValue() {
        Map<String, Object> map = new MapBuilder()
                .put("test", "text")
                .put("obj1", new MapBuilder()
                        .put("test2", "text2")
                        .build())
                .put("array1", Arrays.asList(
                        new MapBuilder()
                                .put("a1", "a11")
                                .put("a2", "a12")
                                .build(),
                        new MapBuilder()
                                .put("a1", "a21")
                                .put("a2", "a22")
                                .build()
                ))
                .put("obj2", new MapBuilder()
                        .put("array2", Arrays.asList(
                                Arrays.asList(
                                        new MapBuilder()
                                                .put("key1", "test1")
                                                .put("key2", "test2")
                                                .build(),
                                        new MapBuilder()
                                                .put("key1", "test11")
                                                .put("key2", "test12")
                                                .build()
                                ),
                                Arrays.asList(
                                        new MapBuilder()
                                                .put("key1", "test21")
                                                .put("key2", "test22")
                                                .build(),
                                        new MapBuilder()
                                                .put("key1", "test31")
                                                .put("key2", "test32")
                                                .build()
                                )
                        ))
                        .build())
                .build();
        List<Object> mapValue = Utils.getMapValue(map, "obj2.array2.key1");
        HashMap<String, Object> newMap = new HashMap<>();
        Utils.putMapValue(map, newMap, "obj2.array2.key1", "1");
        Utils.putMapValue(map, newMap, "obj2.array2.key1", "2");
        Utils.putMapValue(map, newMap, "obj2.array2.key1", "3");
        Utils.putMapValue(map, newMap, "obj2.array2.key1", "4");

        List list = (List) ((Map) newMap.get("obj2")).get("array2");

        Assert.assertEquals("1", ((Map) ((List) list.get(0)).get(0)).get("key1"));
        Assert.assertEquals("2", ((Map) ((List) list.get(0)).get(1)).get("key1"));
        Assert.assertEquals("3", ((Map) ((List) list.get(1)).get(0)).get("key1"));
        Assert.assertEquals("4", ((Map) ((List) list.get(1)).get(1)).get("key1"));
    }
}