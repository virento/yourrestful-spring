package kacper.barszczewski.utils.factories;

import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.MyCache;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class TextFactoryTest {

    private void putParameter(String name, String valueOf) {
        ParameterM parameterM = new ParameterM();
        parameterM.setCode(name);
        parameterM.setValue(valueOf);
        MyCacheImpl.add(MyCache.Domain.PARAMETER, parameterM);
    }

    @SneakyThrows
    @Test
    void generate() {
        TextFactory factory = TextFactory.getInstance();
        putParameter("RANDOM_TEXT", String.join(";", getRandomTexts()));

        Assert.assertEquals("Record 5", factory.generate(5, "Record {n}"));
        Assert.assertEquals("Record 10", factory.generate(10, "Record {n}"));
        Assert.assertEquals("Record 50", factory.generate(50, "Record {n}"));
        Assert.assertEquals("Record a5", factory.generate(5, "Record a{n}"));

        Assert.assertEquals("Record number Lorem ipsum dolor sit amet, consectetur adipiscing elit. text", factory.generate(5, "Record number {random} text"));
        Assert.assertEquals("Random recordc Lorem ipsum dolor si testowanie", factory.generate(5, "Random recordc {random%20} testowanie"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc", factory.generate(5, "{random%20} Random recordc"));


        Assert.assertEquals("Lorem ipsum dolor si Random recordc", factory.generate(5, "{ random % 20 } Random recordc"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc", factory.generate(5, "{      random      %       20} Random recordc"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc", factory.generate(5, "{random% 20} Random recordc"));

        Assert.assertEquals("Lorem ipsum dolor si Random recordc Lorem ipsum dolor sit amet, consectetur adipiscing elit.", factory.generate(5, "{random% 20} Random recordc {random}"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc 5", factory.generate(5, "{random% 20} Random recordc {n}"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc 5 Lorem ipsum dolor sit amet, consectetur adipiscing elit.", factory.generate(5, "{random% 20} Random recordc {n} {random}"));

        Assert.assertEquals("Lorem ipsum dolor si Random recordc Lorem ipsum dolor sit amet, consectetur adipiscing elit.", factory.generate(5, "{     random    % 20  } Random recordc {random}"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc 5", factory.generate(5, "{   random   % 20   } Random recordc {n}"));
        Assert.assertEquals("Lorem ipsum dolor si Random recordc 5 Lorem ipsum dolor sit amet, consectetur adipiscing elit.", factory.generate(5, "{  random   % 20   } Random recordc {n} {  random  }"));


        Assert.assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adi Random recordc 5 Lorem ipsum dolor sit amet, consectetur adipiscing elit.", factory.generate(5, "{  random   % 100   } Random recordc {n} {  random  }"));
    }

    private List<String> getRandomTexts() {
        return Arrays.asList("Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                .split(";"));
    }
}