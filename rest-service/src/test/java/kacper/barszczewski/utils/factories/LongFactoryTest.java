package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.Test;

class LongFactoryTest {

    @SneakyThrows
    @Test
    void generate() {
        LongFactory factory = LongFactory.getInstance();

        Assert.assertEquals((long) 30, (long) factory.generate(5, "5+5*n"));
        Assert.assertEquals((long) 200, (long) factory.generate(2, "10*10*n"));
        Assert.assertEquals((long) -5, (long) factory.generate(5, "20-5*n"));
        Assert.assertEquals((long) 2, (long) factory.generate(5, "50/(5*n)"));

        assertThrows(ApplicationException.class, () -> factory.generate(30, "500*22^n"));
        assertThrows(ApplicationException.class, () -> factory.generate(20, "500+1753255926290448864*n"));
        assertThrows(ApplicationException.class, () -> factory.generate(20, "500-1753255926290448864*n"));

        assertThrows(ApplicationException.class, () -> factory.generate(20, "500-a*n"));

        assertThrows(ApplicationException.class, () -> factory.generate(20, "500-a*POW(2)"));
        assertThrows(ApplicationException.class, () -> factory.generate(20, "500-a*rand(2)"));
        Long generate = factory.generate(20, "10-n*(rand(2) + 1)");
        Assert.assertTrue(-10 == generate ||
                -30 == generate);
    }

    public static void assertThrows(Class<ApplicationException> applicationExceptionClass, ThrowingRunnable o) {
        try {
            o.run();
        } catch (Exception e) {
            Assert.assertEquals(applicationExceptionClass, e.getClass());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}