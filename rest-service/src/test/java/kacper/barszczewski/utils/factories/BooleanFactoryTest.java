package kacper.barszczewski.utils.factories;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class BooleanFactoryTest {

    private static BooleanFactory factory;

    @BeforeAll
    static void setup() {
        factory = (BooleanFactory) BooleanFactory.getInstance();
    }

    @Test
    void createObjectGood1() {
        Assert.assertFalse(factory.createObject("false"));
        Assert.assertFalse(factory.createObject("False"));
        Assert.assertFalse(factory.createObject("0"));

        Assert.assertTrue(factory.createObject("true"));
        Assert.assertTrue(factory.createObject("True"));
        Assert.assertTrue(factory.createObject("321"));
    }

    @Test
    void validateValue() {
        Assert.assertNull(factory.validateValue("false"));
        Assert.assertNull(factory.validateValue("true"));
        Assert.assertNull(factory.validateValue("0"));
        Assert.assertNull(factory.validateValue("1"));
        Assert.assertNull(factory.validateValue("False"));
        Assert.assertNull(factory.validateValue("True"));

        Assert.assertNotNull(factory.validateValue("Truea"));
        Assert.assertNotNull(factory.validateValue("True1"));
        Assert.assertNotNull(factory.validateValue("0a"));
        Assert.assertNotNull(factory.validateValue("false1"));
    }

    @SneakyThrows
    @Test
    void generate() {
        String generator1 = "n%2";
        Assert.assertFalse(factory.generate(0, generator1));
        Assert.assertFalse(factory.generate(2, generator1));
        Assert.assertTrue(factory.generate(3, generator1));
        Assert.assertFalse(factory.generate(4, generator1));
        Assert.assertFalse(factory.generate(6, generator1));
        Assert.assertFalse(factory.generate(8, generator1));

        String generator2 = "true";
        Assert.assertTrue(factory.generate(8, generator2));
        Assert.assertTrue(factory.generate(54, generator2));
        Assert.assertTrue(factory.generate(0, generator2));

        String generator3 = "false";
        Assert.assertFalse(factory.generate(8, generator3));
        Assert.assertFalse(factory.generate(54, generator3));
        Assert.assertFalse(factory.generate(0, generator3));

        Assert.assertTrue(factory.generate(1, "n*(10/5) == 2"));

        Assert.assertTrue(factory.generate(1, "500"));
        Assert.assertFalse(factory.generate(1, "0"));
        Assert.assertFalse(factory.generate(1, "n > 2"));
        Assert.assertTrue(factory.generate(2, "n >= 2"));
    }
}