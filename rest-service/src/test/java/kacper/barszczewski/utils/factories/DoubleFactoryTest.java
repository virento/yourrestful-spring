package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoubleFactoryTest {

    @SneakyThrows
    @Test
    void generate() {
        DoubleFactory factory = DoubleFactory.getInstance();

        Assert.assertEquals(5 * 5 + 20.2, factory.generate(5, "20.2+(5*5)"));
        Assert.assertEquals(-3.8, factory.generate(10, "11.2-(5 + 10)"));
        Assert.assertEquals(20.2 * 5 * 5, factory.generate(5, "20.2*5*5"));
        Assert.assertEquals(1.35, factory.generate(3, "20.2/(5*3)"));


        DateFactoryTest.assertThrows(ApplicationException.class, () -> factory.generate(5, "/+5"));
        DateFactoryTest.assertThrows(ApplicationException.class, () -> factory.generate(5, "+-5"));
        DateFactoryTest.assertThrows(ApplicationException.class, () -> factory.generate(5, "+5a"));
        DateFactoryTest.assertThrows(ApplicationException.class, () -> factory.generate(5, "dsa+5"));
        DateFactoryTest.assertThrows(ApplicationException.class, () -> factory.generate(5, "1a05"));
    }
}