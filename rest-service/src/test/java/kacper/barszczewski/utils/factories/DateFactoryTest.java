package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import org.junit.Assert;
import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class DateFactoryTest {

    @Test
    void createObject() {
        DateFactory instance = DateFactory.getInstance();
        System.out.println(instance.createObject("2020/01/02 12:23:11"));
        System.out.println(instance.createObject("2020/01/2 14:23:11"));
        System.out.println(instance.createObject("2020/1/02 02:03:11"));
    }

    @Test
    void validateValue() {
        DateFactory instance = DateFactory.getInstance();
        instance.validateValue("2020/01/02 12:23:11");
        instance.validateValue("2020/01/2 14:23:11");
        instance.validateValue("2020/1/02 02:03:11");
    }

    @Test
    void generate() throws ApplicationException, ParseException {
        DateFactory instance = DateFactory.getInstance();

        assertDate(getNowDate(Calendar.DAY_OF_MONTH, 5), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(5, "now();+ 1d")));
        assertDate(getNowDate(Calendar.SECOND, 10), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(10, "now();+ 1s")));
        assertDate(getNowDate(Calendar.YEAR, 66), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(66, "now();+ 1y")));
        assertDate(getNowDate(Calendar.MONTH, 8), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(4, "now();+ 2M")));
        assertDate(getNowDate(Calendar.MINUTE, 50), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(5, "now(); + 10m")));
        assertDate(getNowDate(Calendar.MINUTE, -50), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(5, "now(); - 10m")));


        assertDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse("2020/06/10 11:23:02"), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(5, "2020/06/10 12:13:02; - 10m")));
        assertDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse("2024/08/10 12:13:02"), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(5, "2020/06/10 12:13:02; + 10M")));
        assertDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse("2030/06/10 12:13:02"), (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")).parse(instance.generate(5, "2020/06/10 12:13:02; + 2y")));

        assertThrows(ApplicationException.class, () -> instance.generate(5, "now();+10a"));
        assertThrows(ApplicationException.class, () -> instance.generate(5, "nowa();+10a"));
        assertThrows(ApplicationException.class, () -> instance.generate(5, "now2();+10a"));
        assertThrows(ApplicationException.class, () -> instance.generate(5, "now();+ a"));

    }

    public static void assertThrows(Class<ApplicationException> applicationExceptionClass, ThrowingRunnable o) {
        try {
            o.run();
        } catch (Exception e) {
            Assert.assertEquals(applicationExceptionClass, e.getClass());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private Date getNowDate(int dayOfMonth, int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(dayOfMonth, i);
        return calendar.getTime();
    }

    void assertDate(Date d1, Date d2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(d1);
        calendar2.setTime(d2);

        Assert.assertEquals(calendar1.get(Calendar.YEAR), calendar2.get(Calendar.YEAR));
        Assert.assertEquals(calendar1.get(Calendar.MONTH), calendar2.get(Calendar.MONTH));
        Assert.assertEquals(calendar1.get(Calendar.DAY_OF_MONTH), calendar2.get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals(calendar1.get(Calendar.HOUR_OF_DAY), calendar2.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(calendar1.get(Calendar.MINUTE), calendar2.get(Calendar.MINUTE));
        Assert.assertEquals(calendar1.get(Calendar.SECOND), calendar2.get(Calendar.SECOND));
    }
}