package kacper.barszczewski.services;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.pojo.resource.request.WSResourceGeneration;
import kacper.barszczewski.resources.ResourceRecordRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.services.iterfaces.ResourceService;
import kacper.barszczewski.utils.Utils;
import kacper.barszczewski.utils.factories.BooleanFactory;
import kacper.barszczewski.utils.factories.DateFactory;
import kacper.barszczewski.utils.factories.DoubleFactory;
import kacper.barszczewski.utils.factories.LongFactory;
import kacper.barszczewski.utils.factories.ObjectFactory;
import kacper.barszczewski.utils.factories.TextFactory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import utils.MapBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class ResourceServiceImplTest {

    private ResourceService resourceService;

    @Autowired
    private MyCache myCache;

    @MockBean
    private ResourceRepository resourceRepository;
    @MockBean
    private ResourceRecordRepository resourceRecordRepository;

    @Before
    public void beforeEach() {
        ResourceServiceImpl resourceService = new ResourceServiceImpl();
        resourceService.setResourceRecordRepository(resourceRecordRepository);
        resourceService.setResourceRepository(resourceRepository);

        ObjectFactory.register(ObjectFactory.Type.BOOLEAN.name(), BooleanFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DOUBLE.name(), DoubleFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.LONG.name(), LongFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.TEXT.name(), TextFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DATE.name(), DateFactory.getInstance());
        this.resourceService = resourceService;
    }

    @Test(expected = ApplicationException.class)
    public void createResourceWithRecords() throws ApplicationException {
        final Long ownerId = 100L;
        final String title = "Test resource";

        ResourceM resource = new ResourceM();
        resource.setStructure("{\"name\":\"text\"}");
        resource.setOwnerId(ownerId);
        resource.setName(title);

        ObjectFactory.register(ObjectFactory.Type.TEXT.name(), TextFactory.getInstance());

        Mockito.when(resourceRepository.existsByNameAndOwnerIdAndInfoRDIsNull(title, ownerId)).thenReturn(Boolean.TRUE);
        resourceService.createResourceWithRecords(resource, Collections.emptyList(), null);
    }

    @Test
    public void generateRecords() throws ApplicationException {
        final String url = "url2";
        int TO_GENERATE = 10;
        ResourceM resourceM = getResourceM(url);
        WSResourceGeneration recordGenerationObject = getRecordGenerationObject(TO_GENERATE);

        putParameter(Parameters.MAX_RECORDS.name(), String.valueOf(TO_GENERATE));
        putParameter(Parameters.RANDOM_TEXT.name(), "");
        ArrayList<ResourceRecordM> records = new ArrayList<>();
        Mockito.when(resourceRecordRepository.countResourceRecordMByResourceIdAndInfoRDIsNull(any())).thenReturn(0);
        Mockito.when(resourceRepository.getByUrl(url)).thenReturn(resourceM);
        Mockito.when(resourceRecordRepository.save(any())).then(invocationOnMock -> {
            records.add(invocationOnMock.getArgument(0));
            return null;
        });

        long created = resourceService.generateRecords(url, recordGenerationObject);
        Assert.assertEquals("{\"id\":0,\"text\":\"is 0 age\",\"age\":18,\"paycheck\":0.0,\"isDead\":true}", records.get(0).getValue());
        Assert.assertEquals("{\"id\":1,\"text\":\"is 1 age\",\"age\":20,\"paycheck\":36.4,\"isDead\":false}", records.get(1).getValue());
        Assert.assertEquals("{\"id\":2,\"text\":\"is 2 age\",\"age\":22,\"paycheck\":72.8,\"isDead\":true}", records.get(2).getValue());
        Assert.assertEquals("{\"id\":3,\"text\":\"is 3 age\",\"age\":24,\"paycheck\":109.2,\"isDead\":false}", records.get(3).getValue());
        Assert.assertEquals("{\"id\":4,\"text\":\"is 4 age\",\"age\":26,\"paycheck\":145.6,\"isDead\":true}", records.get(4).getValue());
        Assert.assertEquals("{\"id\":5,\"text\":\"is 5 age\",\"age\":28,\"paycheck\":182.0,\"isDead\":false}", records.get(5).getValue());
        Assert.assertEquals("{\"id\":6,\"text\":\"is 6 age\",\"age\":30,\"paycheck\":218.4,\"isDead\":true}", records.get(6).getValue());
        Assert.assertEquals("{\"id\":7,\"text\":\"is 7 age\",\"age\":32,\"paycheck\":254.8,\"isDead\":false}", records.get(7).getValue());
        Assert.assertEquals("{\"id\":8,\"text\":\"is 8 age\",\"age\":34,\"paycheck\":291.2,\"isDead\":true}", records.get(8).getValue());
        Assert.assertEquals("{\"id\":9,\"text\":\"is 9 age\",\"age\":36,\"paycheck\":327.6,\"isDead\":false}", records.get(9).getValue());
        Assert.assertEquals(TO_GENERATE, created);
    }

    @Test
    public void generateRecordsOverLimit() throws ApplicationException {
        final String url = "url3";
        int TO_GENERATE = 10;
        ResourceM resourceM = getResourceM(url);
        WSResourceGeneration recordGenerationObject = getRecordGenerationObject(TO_GENERATE);

        putParameter(Parameters.MAX_RECORDS.name(), String.valueOf(9));
        putParameter(Parameters.RANDOM_TEXT.name(), "");
        ArrayList<ResourceRecordM> records = new ArrayList<>();
        Mockito.when(resourceRecordRepository.countResourceRecordMByResourceIdAndInfoRDIsNull(any())).thenReturn(0);
        Mockito.when(resourceRepository.getByUrl(url)).thenReturn(resourceM);
        Mockito.when(resourceRecordRepository.save(any())).then(invocationOnMock -> {
            records.add(invocationOnMock.getArgument(0));
            return null;
        });

        long created = resourceService.generateRecords(url, recordGenerationObject);
        Assert.assertEquals("{\"id\":0,\"text\":\"is 0 age\",\"age\":18,\"paycheck\":0.0,\"isDead\":true}", records.get(0).getValue());
        Assert.assertEquals("{\"id\":1,\"text\":\"is 1 age\",\"age\":20,\"paycheck\":36.4,\"isDead\":false}", records.get(1).getValue());
        Assert.assertEquals("{\"id\":2,\"text\":\"is 2 age\",\"age\":22,\"paycheck\":72.8,\"isDead\":true}", records.get(2).getValue());
        Assert.assertEquals("{\"id\":3,\"text\":\"is 3 age\",\"age\":24,\"paycheck\":109.2,\"isDead\":false}", records.get(3).getValue());
        Assert.assertEquals("{\"id\":4,\"text\":\"is 4 age\",\"age\":26,\"paycheck\":145.6,\"isDead\":true}", records.get(4).getValue());
        Assert.assertEquals("{\"id\":5,\"text\":\"is 5 age\",\"age\":28,\"paycheck\":182.0,\"isDead\":false}", records.get(5).getValue());
        Assert.assertEquals("{\"id\":6,\"text\":\"is 6 age\",\"age\":30,\"paycheck\":218.4,\"isDead\":true}", records.get(6).getValue());
        Assert.assertEquals("{\"id\":7,\"text\":\"is 7 age\",\"age\":32,\"paycheck\":254.8,\"isDead\":false}", records.get(7).getValue());
        Assert.assertEquals("{\"id\":8,\"text\":\"is 8 age\",\"age\":34,\"paycheck\":291.2,\"isDead\":true}", records.get(8).getValue());
        Assert.assertEquals(TO_GENERATE - 1, created);
    }

    @Test
    public void generateRecordsError() throws ApplicationException {
        final String url = "url1";
        int TO_GENERATE = 10;
        ResourceM resourceM = getResourceM(url);
        WSResourceGeneration recordGenerationObject = getRecordGenerationObject(TO_GENERATE);
        recordGenerationObject.getContent().put("id", "a;+2");

        putParameter(Parameters.MAX_RECORDS.name(), "9");
        putParameter(Parameters.RANDOM_TEXT.name(), "");
        ArrayList<ResourceRecordM> records = new ArrayList<>();
        Mockito.when(resourceRecordRepository.countResourceRecordMByResourceIdAndInfoRDIsNull(any())).thenReturn(0);
        Mockito.when(resourceRepository.getByUrl(url)).thenReturn(resourceM);
        Mockito.when(resourceRecordRepository.save(any())).then(invocationOnMock -> {
            records.add(invocationOnMock.getArgument(0));
            return null;
        });

        try {
            long created = resourceService.generateRecords(url, recordGenerationObject);
            Assert.fail();
        } catch (ApplicationException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(0, records.size());
    }

    @Test
    public void generateRecordsObjects() throws ApplicationException {
        final String url = "url1";
        int TO_GENERATE = 10;
        ResourceM resourceM = new ResourceM();
        resourceM.setUrl(url);
        Map<String, Object> structure = new MapBuilder()
                .put("id", "Long")
                .put("age", "Long")
                .put("text", "text")
                .put("paycheck", "double")
                .put("isDead", "boolean")
                .put("obj", new MapBuilder()
                        .put("value", "Long")
                        .put("obj2", new MapBuilder()
                                .put("value2", "text")
                                .build())
                        .build())
                .build();
        resourceM.setStructure(Utils.convertToJSON(structure));
        WSResourceGeneration recordGenerationObject = getRecordGenerationObject(TO_GENERATE);
        recordGenerationObject.getContent().put("obj", new MapBuilder()
                .put("value", "4+n")
                .put("obj2", new MapBuilder()
                        .put("value2", "test {n}")
                        .build())
                .build());

        putParameter(Parameters.MAX_RECORDS.name(), String.valueOf(TO_GENERATE));
        putParameter(Parameters.RANDOM_TEXT.name(), "");
        ArrayList<ResourceRecordM> records = new ArrayList<>();
        Mockito.when(resourceRecordRepository.countResourceRecordMByResourceIdAndInfoRDIsNull(any())).thenReturn(0);
        Mockito.when(resourceRepository.getByUrl(url)).thenReturn(resourceM);
        Mockito.when(resourceRecordRepository.save(any())).then(invocationOnMock -> {
            records.add(invocationOnMock.getArgument(0));
            return null;
        });

        long created = resourceService.generateRecords(url, recordGenerationObject);
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 0\"},\"value\":4},\"id\":0,\"text\":\"is 0 age\",\"age\":18,\"paycheck\":0.0,\"isDead\":true}", records.get(0).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 1\"},\"value\":5},\"id\":1,\"text\":\"is 1 age\",\"age\":20,\"paycheck\":36.4,\"isDead\":false}", records.get(1).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 2\"},\"value\":6},\"id\":2,\"text\":\"is 2 age\",\"age\":22,\"paycheck\":72.8,\"isDead\":true}", records.get(2).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 3\"},\"value\":7},\"id\":3,\"text\":\"is 3 age\",\"age\":24,\"paycheck\":109.2,\"isDead\":false}", records.get(3).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 4\"},\"value\":8},\"id\":4,\"text\":\"is 4 age\",\"age\":26,\"paycheck\":145.6,\"isDead\":true}", records.get(4).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 5\"},\"value\":9},\"id\":5,\"text\":\"is 5 age\",\"age\":28,\"paycheck\":182.0,\"isDead\":false}", records.get(5).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 6\"},\"value\":10},\"id\":6,\"text\":\"is 6 age\",\"age\":30,\"paycheck\":218.4,\"isDead\":true}", records.get(6).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 7\"},\"value\":11},\"id\":7,\"text\":\"is 7 age\",\"age\":32,\"paycheck\":254.8,\"isDead\":false}", records.get(7).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 8\"},\"value\":12},\"id\":8,\"text\":\"is 8 age\",\"age\":34,\"paycheck\":291.2,\"isDead\":true}", records.get(8).getValue());
        Assert.assertEquals("{\"obj\":{\"obj2\":{\"value2\":\"test 9\"},\"value\":13},\"id\":9,\"text\":\"is 9 age\",\"age\":36,\"paycheck\":327.6,\"isDead\":false}", records.get(9).getValue());
        Assert.assertEquals(TO_GENERATE, created);
    }

    private void putParameter(String name, String valueOf) {
        ParameterM parameterM = new ParameterM();
        parameterM.setCode(name);
        parameterM.setValue(valueOf);
        MyCacheImpl.add(MyCache.Domain.PARAMETER, parameterM);
    }


    @NotNull
    private ResourceM getResourceM(String url) throws ApplicationException {
        ResourceM resourceM = new ResourceM();
        resourceM.setUrl(url);
        Map<String, Object> structure = new MapBuilder()
                .put("id", "Long")
                .put("age", "Long")
                .put("text", "text")
                .put("paycheck", "double")
                .put("isDead", "boolean")
                .build();
        resourceM.setStructure(Utils.convertToJSON(structure));
        return resourceM;
    }

    @NotNull
    private WSResourceGeneration getRecordGenerationObject(int TO_GENERATE) {
        WSResourceGeneration recordGenerationObject = new WSResourceGeneration();
        recordGenerationObject.setGenerate(TO_GENERATE);
        recordGenerationObject.setContent(new MapBuilder()
                .put("id", "n")
                .put("age", "18+(2*n)")
                .put("text", "is {n} age")
                .put("paycheck", "18.2 *(2 *n)")
                .put("isDead", "(n + 1)%2")
                .build());
        return recordGenerationObject;
    }

    @TestConfiguration
    public static class BasicControllerTestConfiguration {

        @Bean
        public MyCache myCache() {
            return new MyCacheImpl();
        }
    }
}