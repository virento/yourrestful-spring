package kacper.barszczewski.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import kacper.barszczewski.filters.CustomAuthenticationFilter;
import kacper.barszczewski.aspect.checkers.ResourceAccessChecker;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.pojo.authentication.LoginForm;
import kacper.barszczewski.pojo.authentication.RegisterForm;
import kacper.barszczewski.resources.ParameterRepository;
import kacper.barszczewski.resources.ResourceRecordRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.resources.ResourceShareRepository;
import kacper.barszczewski.resources.UserRepository;
import kacper.barszczewski.resources.VerificationRepository;
import kacper.barszczewski.services.AuthToken;
import kacper.barszczewski.services.AuthenticationServiceImpl;
import kacper.barszczewski.services.UserAuthenticationImpl;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import kacper.barszczewski.services.iterfaces.MailService;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import kacper.barszczewski.services.iterfaces.VerificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthenticationController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserAuthenticationService myUserDetails;
    @Autowired
    private CustomAuthenticationFilter customAuthenticationFilter;

    @MockBean
    private ResourceRepository resourceRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private ResourceRecordRepository resourceRecordRepository;
    @MockBean
    private MyCache myCache;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private ResourceAccessChecker resourceAccessChecker;
    @MockBean
    private ResourceShareRepository resourceShareRepository;
    @MockBean
    private ParameterRepository parameterRepository;
    @MockBean
    private MailService mailService;
    @MockBean
    private VerificationService verificationService;
    @MockBean
    private VerificationRepository verificationRepository;

    @Test
    public void login() throws Exception {
        Mockito.when(myUserDetails.authenticateByCredential(any())).thenReturn(null);

        mockMvc.perform(post("/login"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void login_application_exception() throws Exception {
        LoginForm loginForm = new LoginForm("Test1", "Test2");

        Mockito.doThrow(new ApplicationException("Some exception")).when(myUserDetails).authenticateByCredential(any());

        mockMvc.perform(post("/login")
                .contentType("application/hal+json")
                .content("{\"login\":\"Test1\",\"password\":\"Test2\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void login_no_logged() throws Exception {
        Mockito.when(myUserDetails.getLoggedUser()).thenReturn(null);

        mockMvc.perform(post("/login")
                .contentType("application/hal+json")
                .content("{\"login\":\"Test1\",\"password\":\"Test2\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void login_logged() throws Exception {
        Mockito.when(myUserDetails.getLoggedUser()).thenReturn(new UserM());
        Mockito.when(myUserDetails.authenticateByCredential(any())).thenReturn(new AuthToken(null, Collections.emptyList(), new Date(), null, null, null));

        mockMvc.perform(post("/login")
                .contentType("application/hal+json")
                .content("{\"login\":\"Test1\",\"password\":\"Test2\"}"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void register_ok() throws Exception {
        RegisterForm registerForm = new RegisterForm("kacper", "admin.123", "kacper@test.pl", "kacper");
        Mockito.when(myUserDetails.saveUserByForm(registerForm)).thenReturn(true);

        mockMvc.perform(post("/register")
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(registerForm)))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void register_application_exception() throws Exception {
        RegisterForm registerForm = new RegisterForm("kacper", "admin", "kacper@test.pl", "kacper");
        Mockito.when(myUserDetails.saveUserByForm(registerForm)).thenReturn(false);

        mockMvc.perform(post("/register")
                .contentType("application/json")
                .content("{\"username\": \"kacper\",\"password\": \"admin\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @TestConfiguration
    public static class AuthenticationControllerTestConfiguration {

        @Bean
        @Qualifier("userAuthenticationImpl")
        public UserAuthenticationService myUserDetails() {
            return Mockito.mock(UserAuthenticationImpl.class);
        }

        @Bean
        public AuthenticationService authenticationService() {
            return new AuthenticationServiceImpl();
        }
    }

}