package kacper.barszczewski.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import kacper.barszczewski.aspect.checkers.ResourceAccessChecker;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.assemblers.ResourceModelAssembler;
import kacper.barszczewski.models.assemblers.ResourceRecordModelAssembler;
import kacper.barszczewski.models.assemblers.UserModelAssembler;
import kacper.barszczewski.models.assemblers.UserResourceRecordModelAssembler;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.pojo.resource.request.WSResourceObject;
import kacper.barszczewski.pojo.resource.request.WSResourceShare;
import kacper.barszczewski.resources.ParameterRepository;
import kacper.barszczewski.resources.ResourceRecordRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.resources.ResourceShareRepository;
import kacper.barszczewski.resources.UserRepository;
import kacper.barszczewski.resources.VerificationRepository;
import kacper.barszczewski.services.AuthenticationServiceImpl;
import kacper.barszczewski.services.UserAuthenticationImpl;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import kacper.barszczewski.services.iterfaces.MailService;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.services.iterfaces.ResourceService;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import kacper.barszczewski.services.iterfaces.VerificationService;
import kacper.barszczewski.utils.InitializationBean;
import kacper.barszczewski.utils.factories.BooleanFactory;
import kacper.barszczewski.utils.factories.DateFactory;
import kacper.barszczewski.utils.factories.DoubleFactory;
import kacper.barszczewski.utils.factories.LongFactory;
import kacper.barszczewski.utils.factories.ObjectFactory;
import kacper.barszczewski.utils.factories.TextFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import utils.MapBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    private MockMvc mockMvc;
    @Autowired
    private UserAuthenticationService myUserDetails;

    @MockBean
    private ResourceRepository resourceRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private MyCache myCache;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ResourceRecordRepository resourceRecordRepository;
    @MockBean
    private ResourceAccessChecker resourceAccessChecker;
    @MockBean
    private ResourceShareRepository resourceShareRepository;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private ParameterRepository parameterRepository;
    @MockBean
    private InitializationBean initializationBean;
    @MockBean
    private MailService mailService;
    @MockBean
    private VerificationService verificationService;
    @MockBean
    private VerificationRepository verificationRepository;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
        ObjectFactory.register(ObjectFactory.Type.BOOLEAN.name(), BooleanFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DOUBLE.name(), DoubleFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.LONG.name(), LongFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.TEXT.name(), TextFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DATE.name(), DateFactory.getInstance());
    }

    @Test
    public void getResource() throws Exception {
        final String resourceUrl = UUID.randomUUID().toString();
        final Long userId = 100L;
        final String username = "Kacper";
        insertSimpleAuthentication(userId, username);

        ResourceM resourceM = new ResourceM();
        resourceM.setName("Name 1");
        resourceM.setId(1231L);
        resourceM.setUrl(resourceUrl);
        resourceM.setType(ResourceM.ResourceType.PRIVATE);
        resourceM.setOwnerId(100L);
        resourceM.setInfoCD(new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").parse("10:10:10 01/01/2020"));

        Mockito.when(resourceService.getResource(resourceUrl)).thenReturn(resourceM);

        mockMvc.perform(get("/user/resource/" + resourceUrl))
                .andDo(print())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"title\":\"Name 1\"")))
                .andExpect(content().string(containsString("\"url\":\"" + resourceUrl + "\"")))
                .andExpect(content().string(containsString("\"_links\"")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/resource/" + resourceUrl + "\"}")))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/resource\"}")))
                .andExpect(status().isOk())
                .andDo(document("user"));
    }

    @Test
    public void getResource_not_found() throws Exception {
        Long userId = 100L;
        String username = "kacper";
        final String resourceUrl = UUID.randomUUID().toString();
        insertSimpleAuthentication(userId, username);

        Mockito.when(resourceService.getResource(resourceUrl)).thenReturn(null);

        mockMvc.perform(get("/user/resource/" + resourceUrl))
                .andExpect(content().contentType("application/hal+json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/resource\"}")))
                .andExpect(content().string(not(containsString("\"title\":null"))))
                .andExpect(content().string(not(containsString("\"url\":null"))))
                .andExpect(content().string(not(containsString("\"createDate\":null"))))
                .andDo(document("user"));
    }

    @Test
    public void getResources() throws Exception {
        final Long userId = 100L;
        final String username = "kacper";
        final List<ResourceM> resourceList = getResourcesTest1();
        insertSimpleAuthentication(userId, username);


        Mockito.when(resourceService.getAllResources(any(), any())).thenReturn(resourceList);

        mockMvc.perform(get("/user/resource"))
                .andExpect(content().contentType("application/hal+json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"page\":{\"size\":" + resourceList.size() + ",\"totalElements\":" + resourceList.size() + ",\"totalPages\":1,\"number\":0}")))
                .andDo(document("user"));
    }

    private void insertSimpleAuthentication(Long userId, String username) throws ParseException {
        UserM userM = new UserM();
        userM.setInfoCD(new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").parse("10:10:10 01/01/2020"));
        userM.setUsername(username);
        userM.setId(userId);
        userM.setStatus(UserM.UserStatus.ACTIVE);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userM, "password"
                , Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void getResources_empty_list() throws Exception {
        final Long userId = 100L;
        final String username = "kacper";
        final List<ResourceM> resourceList = Collections.emptyList();
        insertSimpleAuthentication(userId, username);

        Mockito.when(resourceService.getAllResources(userId, new ListSearchCriteria(100, 0))).thenReturn(resourceList);

        mockMvc.perform(get("/user/resource/"))
                .andExpect(content().contentType("application/hal+json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"page\":{\"size\":" + resourceList.size() + ",\"totalElements\":" + resourceList.size() + ",\"totalPages\":1,\"number\":0}")))
                .andDo(document("user"));
    }

    @Test
    public void createResource() throws Exception {
        final Long userId = 121211L;
        final String username = "Ballando_121";
        final Long resourceId = 2121L;
        final String url = UUID.randomUUID().toString();
        final String title = "Hello im super uber title nr 1 in the world";

        insertSimpleAuthentication(userId, username);

        WSResourceObject resource = new WSResourceObject();
        resource.setTitle(title);
        resource.setStructure(new MapBuilder()
                .put("id", "Long")
                .put("name", "Text")
                .put("price", "Double")
                .put("long_value", "Long")
                .build());
        resource.setRecords(Arrays.asList(
                new MapBuilder()
                        .put("id", 100L)
                        .put("name", "Janek ola bola ula")
                        .put("price", 2221212.22231d)
                        .put("long_value", 1345678231L)
                        .build(),
                new MapBuilder()
                        .put("id", 200L)
                        .put("name", "Uas busa dusa kusa")
                        .put("price", 0.231313214351d)
                        .put("long_value", 34567890918726351L)
                        .build()
        ));
        ResourceM resourceM = resource.toEntity(userId);
        resourceM.setId(resourceId);
        resourceM.setUrl(url);
        Mockito.when(resourceService.createResourceWithRecords(any(ResourceM.class), anyList(), any())).thenReturn(resourceM);

        mockMvc.perform(post("/user/resource")
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(resource)))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"title\":\"" + title + "\"")))
                .andExpect(content().string(containsString("\"url\":\"" + url + "\"")))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/resource\"}")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/resource/" + url + "\"}")))
                .andExpect(content().string(containsString("\"contents\":{\"href\":\"http://localhost:8080/user/" + url + "\"")))
                .andDo(print())
                .andDo(document("user"));
    }

    @Test
    public void getResourceRecords() throws Exception {
        final Long userId = 2321312L;
        final String username = "aaaleesaa-22";
        final String url = UUID.randomUUID().toString();
        final long resourceId = 231321L;
        insertSimpleAuthentication(userId, username);

        ResourceM resourceM = new ResourceM();
        resourceM.setUrl(url);
        resourceM.setId(resourceId);

        List<ResourceRecordM> records = getListResourceRecords1();

        Mockito.when(resourceService.getResource(url)).thenReturn(resourceM);
        Mockito.when(resourceService.getResourceRecords(anyLong(), any(ListSearchCriteria.class))).thenReturn(records);

        mockMvc.perform(get("/user/" + url))
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"page\":{\"size\":5,\"totalElements\":5,\"totalPages\":1,\"number\":0}")))
                .andExpect(content().string(containsString("\"resource\":{\"href\":\"http://localhost:8080/user/resource/" + url + "\"}")))
                .andDo(print())
                .andDo(document("user"));

    }

    private List<ResourceRecordM> getListResourceRecords1() {
        ResultList<ResourceRecordM> records = new ResultList<>();
        for (int i = 0; i < 5; i++) {
            ResourceRecordM record = new ResourceRecordM();
            record.setId(231321L + i);
            record.setInfoCD(new Date());
            record.setParsedValue(new MapBuilder()
                    .put("id", (long) i)
                    .put("name", "Janek" + i)
                    .put("balance", 231.22d * i)
                    .put("adult", i % 3 == 0)
                    .build());
            records.add(record);
        }
        records.setTotalPage(1L);
        records.setPage(0L);
        records.setTotalSize(5L);
        return records;
    }

    @Test
    public void getResourceRecord() throws Exception {
        final Long userId = 1231321L;
        final String username = "dsa23212cdsa";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 23121L;
        final Long resourceId = 231321L;
        insertSimpleAuthentication(userId, username);

        ResourceM resourceM = new ResourceM();
        resourceM.setId(resourceId);
        resourceM.setUrl(url);

        ResourceRecordM record = new ResourceRecordM();
        record.setId(recordId);
        record.setParsedValue(new MapBuilder()
                .put("id", recordId)
                .put("name", "Janek bo lubie")
                .put("balance", 231321.02d)
                .put("adult", true)
                .build());
        record.setResource(resourceM);

        Mockito.when(resourceService.getResource(url)).thenReturn(resourceM);
        Mockito.when(resourceService.getResourceRecord(resourceId, recordId)).thenReturn(record);

        mockMvc.perform(get("/user/" + url + "/" + recordId))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/" + url + "\"}")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/" + url + "/" + recordId + "\"")))
                .andExpect(content().string(containsString("\"balance\":231321.02")))
                .andExpect(content().string(containsString("\"name\":\"Janek bo lubie\"")))
                .andExpect(content().string(containsString("\"id\":23121")))
                .andExpect(content().string(containsString("\"adult\":true")))
                .andDo(print())
                .andDo(document("user"));

    }

    @Test
    public void postResourceRecord() throws Exception {
        final Long userId = 54325432L;
        final String username = "Janosikkkaaa";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 453243L;
        final Long resourceId = 45321L;
        insertSimpleAuthentication(userId, username);

        Map<String, Object> map = new MapBuilder()
                .put("id", recordId)
                .put("name", "Testowanietest")
                .put("balance", 344321.021d)
                .put("adult", false)
                .build();

        ResourceM resource = new ResourceM();
        resource.setId(resourceId);
        resource.setUrl(url);
        resource.setEditable(true);
        resource.setActive(true);

        ResourceRecordM record = new ResourceRecordM();
        record.setId(recordId);
        record.setParsedValue(new HashMap<>(map));
        record.getParsedValue().remove("id");

        Mockito.when(resourceService.getResource(url)).thenReturn(resource);
        //noinspection unchecked
        Mockito.when(resourceService.createResourceRecord(anyLong(), any(Map.class), anyBoolean())).thenReturn(record);

        mockMvc.perform(post("/user/" + url)
                .content(new ObjectMapper().writeValueAsString(map))
                .contentType("application/json"))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/" + url + "\"}")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/" + url + "/" + recordId + "\"}")))
                .andExpect(content().string(containsString("\"name\":\"Testowanietest\"")))
                .andExpect(content().string(containsString("\"name\":\"Testowanietest\"")))
                .andExpect(content().string(containsString("\"balance\":344321.021")))
                .andExpect(content().string(containsString("\"adult\":false")))
                .andDo(print())
                .andDo(document("user"));
    }

    @Test
    public void putResourceRecord() throws Exception {
        final Long userId = 54325432L;
        final String username = "Borukassa";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 65463L;
        final Long resourceId = 431221L;
        insertSimpleAuthentication(userId, username);

        ResourceM resource = new ResourceM();
        resource.setId(resourceId);
        resource.setUrl(url);
        resource.setEditable(true);
        resource.setActive(true);

        ResourceRecordM record = new ResourceRecordM();
        record.setId(recordId);
        final Map<String, Object> map = new MapBuilder()
                .put("id", recordId)
                .put("name", "Boloonow www")
                .put("balance", 432111.0111d)
                .put("adult", true)
                .build();
        record.setParsedValue(map);

        Mockito.when(resourceService.getResource(url)).thenReturn(resource);
        //noinspection unchecked
        Mockito.when(resourceService.modifyResourceRecord(anyLong(), any(Map.class), anyBoolean())).thenReturn(record);
        Mockito.when(resourceService.existsResourceRecord(anyLong(), anyLong())).thenReturn(true);

        mockMvc.perform(put("/user/" + url)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/" + url + "\"}")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/" + url + "/" + recordId + "\"}")))
                .andExpect(content().string(containsString("\"balance\":432111.0111")))
                .andExpect(content().string(containsString("\"name\":\"Boloonow www\"")))
                .andExpect(content().string(containsString("\"id\":" + recordId.toString())))
                .andExpect(content().string(containsString("\"adult\":true")))
                .andDo(print())
                .andDo(document("user"));
    }

    @Test
    public void putResourceRecord_creating() throws Exception {
        final Long userId = 54325432L;
        final String username = "Borukassa";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 65463L;
        final Long resourceId = 431221L;
        insertSimpleAuthentication(userId, username);

        ResourceM resource = new ResourceM();
        resource.setId(resourceId);
        resource.setUrl(url);
        resource.setEditable(true);
        resource.setActive(true);

        ResourceRecordM record = new ResourceRecordM();
        final Map<String, Object> map = new MapBuilder()
                .put("id", recordId)
                .put("name", "Boloonow www")
                .put("balance", 432111.0111d)
                .put("adult", true)
                .build();
        record.setParsedValue(map);

        Mockito.when(resourceService.getResource(url)).thenReturn(resource);
        //noinspection unchecked
        Mockito.when(resourceService.createResourceRecord(anyLong(), any(Map.class), anyBoolean())).thenReturn(record);
        Mockito.when(resourceService.existsResourceRecord(anyLong(), anyLong())).thenReturn(false);

        mockMvc.perform(put("/user/" + url)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/" + url + "\"}")))
                .andExpect(content().string(containsString("\"balance\":432111.0111")))
                .andExpect(content().string(containsString("\"name\":\"Boloonow www\"")))
                .andExpect(content().string(containsString("\"id\":" + recordId.toString())))
                .andExpect(content().string(containsString("\"adult\":true")))
                .andDo(print())
                .andDo(document("user"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void patchResourceRecord() throws Exception {
        final Long userId = 5432678L;
        final String username = "Barakuda Zeusa";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 1231L;
        final Long resourceId = 543112L;
        insertSimpleAuthentication(userId, username);

        ResourceM resource = new ResourceM();
        resource.setId(resourceId);
        resource.setUrl(url);
        resource.setEditable(true);
        resource.setActive(true);

        ResourceRecordM record = new ResourceRecordM();
        record.setId(recordId);
        final Map<String, Object> map = new MapBuilder()
                .put("name", "Borkurrr")
                .put("balance", 3111.0d)
                .put("adult", false)
                .build();

        final Map<String, Object> beforeRecord = new MapBuilder()
                .put("id", recordId)
                .put("name", "Borkurrr")
                .put("balance", 3111.0d)
                .put("adult", false)
                .build();
        record.setParsedValue(beforeRecord);

        Mockito.when(resourceService.getResource(url)).thenReturn(resource);
        Mockito.when(resourceService.getResourceRecord(resourceId, recordId)).thenReturn(record);
        Mockito.when(resourceService.modifyResourceRecord(anyLong(), any(Map.class), anyBoolean())).thenReturn(record);
        Mockito.when(resourceService.existsResourceRecord(anyLong(), anyLong())).thenReturn(true);

        mockMvc.perform(patch("/user/" + url + "/" + recordId)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost:8080/user/" + url + "\"}")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/" + url + "/" + recordId + "\"}")))
                .andExpect(content().string(containsString("\"balance\":3111.0")))
                .andExpect(content().string(containsString("\"name\":\"Borkurrr\"")))
                .andExpect(content().string(containsString("\"id\":" + recordId.toString())))
                .andExpect(content().string(containsString("\"adult\":false")))
                .andDo(print())
                .andDo(document("user"));
    }

    @Test
    public void patchResourceRecordTest2ExceptionIdEditing() throws Exception {
        final Long userId = 5432678L;
        final String username = "Barakuda Zeusa";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 1231L;
        final Long resourceId = 543112L;
        insertSimpleAuthentication(userId, username);

        ResourceM resource = new ResourceM();
        resource.setId(resourceId);
        resource.setUrl(url);
        resource.setEditable(true);
        resource.setActive(true);

        ResourceRecordM record = new ResourceRecordM();
        record.setId(recordId);
        final Map<String, Object> map = new MapBuilder()
                .put("id", recordId)
                .put("name", "Borkurrr")
                .put("balance", 3111.0d)
                .put("adult", false)
                .build();
        record.setParsedValue(map);

        Mockito.when(resourceService.getResource(url)).thenReturn(resource);
        Mockito.when(resourceService.getResourceRecord(resourceId, recordId)).thenReturn(record);
        Mockito.when(resourceService.modifyResourceRecord(anyLong(), any(Map.class), anyBoolean())).thenReturn(record);
        Mockito.when(resourceService.existsResourceRecord(anyLong(), anyLong())).thenReturn(true);

        mockMvc.perform(patch("/user/" + url + "/" + recordId)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andDo(document("user"));
    }


    @Test
    public void deleteResourceRecord() throws Exception {
        final Long userId = 5435432L;
        final String username = "Telewizor";
        final String url = UUID.randomUUID().toString();
        final Long recordId = 4321L;
        final Long resourceId = 5432532L;
        insertSimpleAuthentication(userId, username);

        ResourceM resource = new ResourceM();
        resource.setId(resourceId);
        resource.setUrl(url);
        resource.setEditable(true);
        resource.setActive(true);

        Mockito.when(resourceService.getResource(url)).thenReturn(resource);
        Mockito.when(resourceService.getResourceRecord(resourceId, recordId)).thenReturn(new ResourceRecordM());

        mockMvc.perform(delete("/user/" + url + "/" + recordId))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void patchResource() throws Exception {
        final Long userId = 5435432L;
        final String username = "Telewizor";
        final String resourceUrl = UUID.randomUUID().toString();
        final WSResourceObject resourceObject = new WSResourceObject();
        final String title = "New title";
        final Boolean active = false;
        final Boolean editable = true;
        insertSimpleAuthentication(userId, username);
        resourceObject.setTitle(title);
        resourceObject.setActive(active);
        resourceObject.setEditable(editable);

        ResourceM resourceTest = getResourceTest1();
        resourceTest.setId(100L);
        resourceTest.setUrl(resourceUrl);

        Mockito.when(resourceService.getResource(resourceUrl)).thenReturn(resourceTest);
        Mockito.when(resourceService.modifyResource(any(), any())).thenReturn(resourceTest);

        mockMvc.perform(patch("/user/resource/" + resourceUrl)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(resourceObject)))
                .andExpect(content().string(containsString("\"title\":\"" + title + "\"")))
                .andExpect(content().string(containsString("\"url\":\"" + resourceUrl + "\"")))
                .andExpect(content().string(containsString("\"active\":" + active.toString() + "")))
                .andExpect(content().string(containsString("\"editable\":" + editable.toString() + "")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost:8080/user/resource/" + resourceUrl + "\"}")))
                .andExpect(content().string(containsString("\"contents\":{\"href\":\"http://localhost:8080/user/" + resourceUrl + "\"}")))
                .andExpect(status().isOk())
                .andDo(print())
                .andDo(document("user"));
    }

    @Test
    public void addUserToResource() throws Exception {
        final Long userId = 5435432L;
        final String username = "Janosik";
        final String newShareUser = "Bartek";
        final String resourceUrl = UUID.randomUUID().toString();
        insertSimpleAuthentication(userId, username);

        WSResourceShare resourceShare = new WSResourceShare();
        resourceShare.setNick(newShareUser);

        Mockito.when(myUserDetails.existsUser(any(), any())).thenReturn(true);

        mockMvc.perform(post("/user/" + resourceUrl + "/share")
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(resourceShare)))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void getResourceShares() throws Exception {
        final Long userId = 5435432L;
        final String username = "Janosik";
        final String resourceUrl = UUID.randomUUID().toString();

        final Long user2Id = 231231L;
        final String user2FirstName = "Bartosik";
        final String user2Surname = "Bartoszewski";
        final String user2Email = "Bartek222@test.pl";
        final String user2UserName = "Barte222";
        insertSimpleAuthentication(userId, username);

        UserM user = new UserM();
        user.setId(user2Id);
        user.setUsername(user2UserName);
        user.setLastName(user2Surname);
        user.setFirstName(user2FirstName);
        user.setEmail(user2Email);
        ResultList<UserM> usersList = new ResultList<>();
        usersList.add(user);
        usersList.setTotalSize(1L);
        usersList.setPage(0L);
        usersList.setTotalPage(1L);

        Mockito.when(resourceService.getResourceShares(any(), any())).thenReturn(usersList);

        mockMvc.perform(get("/user/" + resourceUrl + "/shares"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[{\"username\":\"Barte222\",\"email\":\"Bartek222@test.pl\"}]")))
                .andDo(print())
                .andDo(document("user"));
    }

    private List<ResourceM> getResourcesTest1() {
        final List<ResourceM> resources = new ArrayList<>();
        for (int n = 0; n < 10; n++) {
            ResourceM resourceM = new ResourceM();
            resourceM.setType(ResourceM.ResourceType.PRIVATE);
            resourceM.setId((long) n);
            resourceM.setName("Name" + n);
            resourceM.setInfoCD(new Date());
            resourceM.setUrl(UUID.randomUUID().toString());
            resources.add(resourceM);
        }
        return resources;
    }

    private ResourceM getResourceTest1() {
        ResourceM resource = new ResourceM();
        resource.setName("Resource name");
        resource.setUrl(UUID.randomUUID().toString());
        resource.setActive(true);
        resource.setEditable(true);
        resource.setInfoCD(new Date());
        resource.setStructure("{Rok: Text, Nazwisko: Text, Wiek: Long, id: Long}");
        return resource;
    }

    @TestConfiguration
    public static class BasicControllerTestConfiguration {


        @Bean
        public ResourceRecordModelAssembler resourceRecordModelAssembler() {
            return new ResourceRecordModelAssembler();
        }

        @Bean
        public ResourceModelAssembler resourceModelAssembler() {
            return new ResourceModelAssembler();
        }

        @Bean
        public UserResourceRecordModelAssembler userResourceRecordModelAssembler() {
            return new UserResourceRecordModelAssembler();
        }

        @Bean
        @Qualifier("userAuthenticationImpl")
        public UserAuthenticationService myUserDetails() {
            return Mockito.mock(UserAuthenticationImpl.class);
        }

        @Bean
        public InitializationBean initializingBean() {
            return new InitializationBean();
        }

        @Bean
        public AuthenticationService authenticationService() {
            return new AuthenticationServiceImpl();
        }

        @Bean
        public UserModelAssembler userModelAssembler() {
            return new UserModelAssembler();
        }
    }
}