package kacper.barszczewski.controllers;

import kacper.barszczewski.aspect.checkers.ResourceAccessChecker;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.assemblers.ResourceModelAssembler;
import kacper.barszczewski.models.assemblers.ResourceRecordModelAssembler;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.resources.ParameterRepository;
import kacper.barszczewski.resources.ResourceRecordRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.resources.ResourceShareRepository;
import kacper.barszczewski.resources.UserRepository;
import kacper.barszczewski.resources.VerificationRepository;
import kacper.barszczewski.services.AuthenticationServiceImpl;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import kacper.barszczewski.services.iterfaces.MailService;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.services.iterfaces.ResourceService;
import kacper.barszczewski.services.iterfaces.VerificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("unused")
@WebMvcTest(BasicController.class)
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {ResourceRecordModelAssembler.class, WebSecurityConfigInitializer.class, BasicController.class})
public class BasicControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ResourceService resourceService;
    @MockBean
    private ResourceRecordRepository resourceRecordRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private ResourceRepository resourceRepository;
    @MockBean
    private ResourceAccessChecker resourceAccessChecker;
    @MockBean
    private ResourceShareRepository resourceShareRepository;
    @MockBean
    private ParameterRepository parameterRepository;
    @MockBean
    private MailService mailService;
    @MockBean
    private VerificationService verificationService;
    @MockBean
    private VerificationRepository verificationRepository;

    @Test
    public void getData() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;
        final ResourceRecordM resourceRecordM = new ResourceRecordM();
        resourceRecordM.setId(recordId);
        resourceRecordM.setInfoCD(new Date());
        resourceRecordM.setResourceId(universalResourceId);
        resourceRecordM.setValue("{id: 100, name: Janek, surname: Janowski, adult: False}");
        Map<String, Object> values = new HashMap<>();
        values.put("id", 100L);
        values.put("name", "Janek");
        values.put("surname", "Janowski");
        values.put("adult", false);
        resourceRecordM.setParsedValue(values);


        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(resourceRecordM);

        mockMvc.perform(get("/api/200"))
                .andDo(print())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"surname\":\"Janowski\"")))
                .andExpect(content().string(containsString("\"name\":\"Janek\"")))
                .andExpect(content().string(containsString("\"id\":100")))
                .andExpect(content().string(containsString("\"adult\":false")))
                .andExpect(content().string(containsString("_links")))
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost/api/200\"}}")))
                .andExpect(status().isOk());
    }

    @Test
    public void getData_Id_not_found() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(null);

        mockMvc.perform(get("/api/200"))
                .andExpect(content().string("{\"_links\":{\"all\":{\"href\":\"http://localhost/api/\"}}}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andDo(print());
    }

    @Test
    public void getDataAll() throws Exception {
        final Long universalResourceId = 100L;
        final List<ResourceRecordM> records = getResourceRecords1(universalResourceId);

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecords(universalResourceId, new ListSearchCriteria(100, 0))).thenReturn(records);

        mockMvc.perform(get("/api"))
                .andDo(print())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"page\":{\"size\":" + records.size() + ",\"totalElements\":" + records.size() + ",\"totalPages\":1,\"number\":0}")))
                .andExpect(content().string(containsString("\"price\":40.2,\"name\":\"Kacper2\",\"id\":2,\"good\":true}")))
                .andExpect(status().isOk());
    }

    @Test
    public void postData() throws Exception {
        final Long universalResourceId = 100L;
        final Map<String, Object> map = new HashMap<>();
        map.put("id", 100);
        map.put("name", "Test");
        map.put("price", 102.22d);
        map.put("adult", true);
        final ResourceRecordM resourceRecordM = new ResourceRecordM();
        resourceRecordM.setId(200L);
        resourceRecordM.setParsedValue(map);
        resourceRecordM.setResourceId(universalResourceId);

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.createResourceRecord(universalResourceId, map, true)).thenReturn(resourceRecordM);
        Mockito.when(resourceService.existsResourceRecord(universalResourceId, 100L)).thenReturn(false);

        mockMvc.perform(post("/api")
                .contentType("application/json")
                .content("{\"id\": 100, \"name\":\"Test\", \"price\":102.22, \"adult\":true}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost/api/200\"}")))
                .andExpect(content().string(containsString("\"price\":102.22,\"name\":\"Test\",\"id\":100,\"adult\":true")))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost/api/\"}")))
                .andExpect(content().contentType("application/hal+json"));
    }

    @Test
    public void postData_error_throned() throws Exception {
        final Long universalResourceId = 100L;
        final Map<String, Object> map = new HashMap<>();
        map.put("id", 100);
        map.put("name", "Test");
        map.put("price", 102.22d);
        map.put("adult", true);
        final ResourceRecordM resourceRecordM = new ResourceRecordM();
        resourceRecordM.setId(200L);
        resourceRecordM.setParsedValue(map);
        resourceRecordM.setResourceId(universalResourceId);

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.createResourceRecord(universalResourceId, map, true)).thenThrow(new ApplicationException("Error x122"));
        Mockito.when(resourceService.existsResourceRecord(universalResourceId, 100L)).thenReturn(false);

        mockMvc.perform(post("/api")
                .contentType("application/json")
                .content("{\"id\": 100, \"name\":\"Test\", \"price\":102.22, \"adult\":true}"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("\"errorCode\":\"ERROR\"")))
                .andExpect(content().string(containsString("\"description\":\"Error x122\"")))
                .andExpect(content().contentType("application/json"));
    }

    @Test
    public void putData_modification() throws Exception {
        final Long universalResourceId = 100L;
        final Map<String, Object> map = new HashMap<>();
        map.put("id", 100);
        map.put("name", "Test");
        map.put("price", 102.22d);
        map.put("adult", true);
        final ResourceRecordM resourceRecordM = new ResourceRecordM();
        resourceRecordM.setId(200L);
        resourceRecordM.setParsedValue(map);
        resourceRecordM.setResourceId(universalResourceId);
        resourceRecordM.setInfoMD(new Date());

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.modifyResourceRecord(universalResourceId, map, true)).thenReturn(resourceRecordM);
        Mockito.when(resourceService.existsResourceRecord(universalResourceId, 100L)).thenReturn(true);

        mockMvc.perform(put("/api")
                .contentType("application/json")
                .content("{\"id\": 100, \"name\":\"Test\", \"price\":102.22, \"adult\":true}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost/api/200\"}")))
                .andExpect(content().string(containsString("\"price\":102.22,\"name\":\"Test\",\"id\":100,\"adult\":true")))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost/api/\"}")))
                .andExpect(content().contentType("application/hal+json"));
    }

    @Test
    public void putData_creation() throws Exception {
        final Long universalResourceId = 100L;
        final Map<String, Object> map = new HashMap<>();
        map.put("id", 100);
        map.put("name", "Test");
        map.put("price", 102.22d);
        map.put("adult", true);
        final ResourceRecordM resourceRecordM = new ResourceRecordM();
        resourceRecordM.setId(100L);
        resourceRecordM.setInfoCD(new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").parse("10:10:10 01/01/2020"));
        resourceRecordM.setParsedValue(map);
        resourceRecordM.setResourceId(universalResourceId);

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.createResourceRecord(universalResourceId, map, true)).thenReturn(resourceRecordM);
        Mockito.when(resourceService.existsResourceRecord(universalResourceId, 100L)).thenReturn(false);

        mockMvc.perform(put("/api")
                .contentType("application/json")
                .content("{\"id\": 100, \"name\":\"Test\", \"price\":102.22, \"adult\":true}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost/api/100\"}")))
                .andExpect(content().string(containsString("\"price\":102.22,\"name\":\"Test\",\"id\":100,\"adult\":true")))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost/api/\"}")))
                .andExpect(content().contentType("application/hal+json"));
    }

    @Test
    public void patchData() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;
        final Map<String, Object> map = new HashMap<>();
        map.put("id", recordId);
        map.put("name", "Test");
        map.put("price", 102.22d);
        map.put("adult", true);
        final ResourceRecordM resourceRecordM = new ResourceRecordM();
        resourceRecordM.setId(200L);
        resourceRecordM.setInfoCD(new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").parse("10:10:10 01/01/2020"));
        resourceRecordM.setParsedValue(map);
        resourceRecordM.setResourceId(universalResourceId);

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(resourceRecordM);
        Mockito.when(resourceService.modifyResourceRecord(anyLong(), anyMap(), anyBoolean())).thenReturn(resourceRecordM);
        Mockito.when(resourceService.existsResourceRecord(universalResourceId, recordId)).thenReturn(true);

        mockMvc.perform(patch("/api/200")
                .contentType("application/json")
                .content("{\"id\": 200, \"name\":\"Test\", \"price\":102.22, \"adult\":true}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"self\":{\"href\":\"http://localhost/api/200\"}")))
                .andExpect(content().string(containsString("\"price\":102.22,\"name\":\"Test\",\"id\":200,\"adult\":true")))
                .andExpect(content().string(containsString("\"all\":{\"href\":\"http://localhost/api/\"}")))
                .andExpect(content().contentType("application/hal+json"));
    }

    @Test
    public void patchData_not_found() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(null);

        mockMvc.perform(patch("/api/200")
                .contentType("application/json")
                .content("{\"id\": 200, \"name\":\"Test\", \"price\":102.22, \"adult\":true}"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("\"description\":\"Record with id 200 not found\"")))
                .andExpect(content().string(containsString("\"errorCode\":\"ERROR\"")))
                .andExpect(content().contentType("application/json"));
    }

    @Test
    public void deleteData() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(new ResourceRecordM());

        mockMvc.perform(delete("/api/200"))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteData_id_not_found() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(null);

        mockMvc.perform(delete("/api/200"))
                .andDo(print())
                .andExpect(content().string(containsString("\"description\":\"Record with id 200 not found\"")))
                .andExpect(content().string(containsString("\"errorCode\":\"ERROR\"")))
                .andExpect(content().contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    private void addUniversalResource(Long universalResourceId) {
        ResourceM resourceM = new ResourceM();
        resourceM.setId(universalResourceId);
        resourceM.setUrl("api");
        resourceM.setType(ResourceM.ResourceType.UNIVERSAL);
        MyCacheImpl.add(MyCache.Domain.RESOURCE, resourceM);
    }

    @Test
    public void deleteData_without_id() throws Exception {
        final Long universalResourceId = 100L;
        final Long recordId = 200L;

        addUniversalResource(universalResourceId);
        Mockito.when(resourceService.getResourceRecord(universalResourceId, recordId)).thenReturn(null);

        mockMvc.perform(delete("/api"))
                .andDo(print())
                .andExpect(content().string(containsString("{\"content\":\"Request method 'DELETE' not supported\"}")))
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(status().isMethodNotAllowed());
    }

    @TestConfiguration
    public static class BasicControllerTestConfiguration {

        @Bean
        public ResourceRecordModelAssembler resourceRecordModelAssembler() {
            return new ResourceRecordModelAssembler();
        }

        @Bean
        public ResourceModelAssembler resourceModelAssembler() {
            return new ResourceModelAssembler();
        }

        @Bean
        public AuthenticationService authenticationService() {
            return new AuthenticationServiceImpl();
        }
    }

    public List<ResourceRecordM> getResourceRecords1(Long resourceId) {
        List<ResourceRecordM> records = new ArrayList<>();

        for (int n = 0; n < 10; n++) {
            ResourceRecordM record = new ResourceRecordM();
            record.setId(100L);
            record.setResourceId(resourceId);
            Map<String, Object> map = new HashMap<>();
            map.put("id", n);
            map.put("name", "Kacper" + n);
            map.put("price", 20.1d * n);
            map.put("good", n % 2 == 0);
            record.setParsedValue(map);
            records.add(record);
        }
        ResultList<ResourceRecordM> resultList = new ResultList<>(records);
        resultList.setTotalPage(1L);
        resultList.setPage(0L);
        resultList.setTotalSize((long) resultList.size());
        return resultList;
    }
}