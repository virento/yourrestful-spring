package kacper.barszczewski.models;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class ResourceMTest {

    private void testGetStructureAsJson(String test, String expected) {
        ResourceM resourceM = new ResourceM();
        resourceM.setStructure(test);
        String structureAsJson = resourceM.getStructureAsJson();
        Assert.assertEquals(expected, structureAsJson);
    }
}