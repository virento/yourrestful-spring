package kacper.barszczewski.structure;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.utils.factories.AbstractFactory;
import kacper.barszczewski.utils.factories.BooleanFactory;
import kacper.barszczewski.utils.factories.DoubleFactory;
import kacper.barszczewski.utils.factories.LongFactory;
import kacper.barszczewski.utils.factories.ObjectFactory;
import kacper.barszczewski.utils.factories.TextFactory;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.MapBuilder;

import java.util.Map;

class StructureParserTest {


    @Test
    void structureTest1() throws ApplicationException {
        String structure = "{text1: \"text\", obj1: {obj11: {text111: \"text\", text112: \"text\"}}, text2: \"text\", id: \"Long\"}";
        StructureParser structureParser = new StructureParser(structure);
        Map<String, Object> map = new MapBuilder()
                .put("text1", "test1")
                .put("text2", "test2")
                .put("id", 123)
                .put("obj1", new MapBuilder()
                        .put("obj11", new MapBuilder()
                                .put("text111", "test1")
                                .put("text112", "test22")
                                .build())
                        .build()
                ).build();
        structureParser.validateRecord(map);
    }

    @Test
    void structureTest2() throws ApplicationException {
        String structure = "{test:{obj2:{tt:\"text\"},aa:\"text\"},obj1:{test:\"text\",obj2:{tt:\"text\"},ds:\"text\"},id:\"Long\"}";
        StructureParser structureParser = new StructureParser(structure);
        Map<String, AbstractFactory> map = structureParser.getMap();

        Assert.assertTrue(map.containsKey("test.obj2.tt"));
        Assert.assertTrue(map.containsKey("test.aa"));
        Assert.assertTrue(map.containsKey("obj1.test"));
        Assert.assertTrue(map.containsKey("obj1.obj2.tt"));
        Assert.assertTrue(map.containsKey("obj1.ds"));
        Assert.assertTrue(map.containsKey("id"));
    }

    @Test
    void structureTest3() throws ApplicationException {
        String structure = "{array:[{number:\"long\",text:\"text\"}],imie:\"text\",praca:{nazwa:\"text\",nienazwa:\"text\"}}";
        StructureParser structureParser = new StructureParser(structure);
        Map<String, AbstractFactory> map = structureParser.getMap();
        Assert.assertTrue(map.containsKey("array.number"));
        Assert.assertTrue(map.containsKey("array.text"));
        Assert.assertTrue(map.containsKey("imie"));
        Assert.assertTrue(map.containsKey("praca.nazwa"));
        Assert.assertTrue(map.containsKey("praca.nienazwa"));
    }

    @BeforeEach
    public void init() {
        ObjectFactory.register(ObjectFactory.Type.BOOLEAN.name(), BooleanFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DOUBLE.name(), DoubleFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.LONG.name(), LongFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.TEXT.name(), TextFactory.getInstance());
    }

}