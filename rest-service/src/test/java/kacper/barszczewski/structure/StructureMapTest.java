package kacper.barszczewski.structure;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.utils.Utils;
import kacper.barszczewski.utils.factories.BooleanFactory;
import kacper.barszczewski.utils.factories.DoubleFactory;
import kacper.barszczewski.utils.factories.LongFactory;
import kacper.barszczewski.utils.factories.ObjectFactory;
import kacper.barszczewski.utils.factories.TextFactory;
import org.assertj.core.util.Arrays;
import org.junit.Assert;
import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.MapBuilder;

import java.util.HashMap;
import java.util.Map;

class StructureMapTest {


    @Test
    void structureNestExceed() throws ApplicationException {
        putParameter(Parameters.STRUCTURE_MAX_NESTING.name(), "2");

        String wrongMap = Utils.convertToJSON(
                new MapBuilder()
                        .put("n1", new MapBuilder()
                                .put("n2", new MapBuilder()
                                        .put("n3", new MapBuilder()
                                                .put("n4", "text")
                                                .build()
                                        ).build()
                                ).build()
                        ).build()
        );

        String wrongMap2 = Utils.convertToJSON(
                new MapBuilder()
                        .put("test", "text")
                        .put("n1", new MapBuilder()
                                .put("test", "text")
                                .put("n2", new MapBuilder()
                                        .put("test", "text")
                                        .put("n3", new MapBuilder()
                                                .put("test", "text")
                                                .build())
                                        .build())
                                .build())
                        .build()
        );

        String wrongMap3 = Utils.convertToJSON(
                new MapBuilder()
                        .put("test", "text")
                        .put("n1", new MapBuilder()
                                .put("test", "text")
                                .put("n2", new MapBuilder()
                                        .put("test", "text")
                                        .build())
                                .build())
                        .build()
        );

        String goodMap = Utils.convertToJSON(
                new MapBuilder().put("n1", "text").build()
        );
        String goodMap2 = Utils.convertToJSON(
                new MapBuilder().put("test", "text")
                        .put("n1", new MapBuilder().put("test", "text").build())
                        .build()
        );
        String goodMap3 = Utils.convertToJSON(
                new MapBuilder().put("test", "text")
                        .put("n1", new MapBuilder().put("test", "text")
                                .put("n2", new MapBuilder().put("test", "text").build())
                                .build())
                        .build()
        );

        assertThrows(ApplicationException.class, () -> new StructureParser(wrongMap));
        assertThrows(ApplicationException.class, () -> new StructureParser(wrongMap2));
        assertThrows(ApplicationException.class, () -> new StructureParser(wrongMap3));
        try {
            new ApplicationException(goodMap);
            new ApplicationException(goodMap2);
            new ApplicationException(goodMap3);
        } catch (ApplicationException e) {
            Assert.fail();
        }
    }

    @Test
    void structureFieldExceed() {
        putParameter(Parameters.STRUCTURE_MAX_FIELDS.name(), "3");

        String good1 = Utils.convertToJSON(
                new MapBuilder()
                        .put("f1", "text")
                        .put("f2", "text")
                        .put("f3", new MapBuilder()
                                .put("f1", "text")
                                .put("f2", "text")
                                .put("f3", "text")
                                .build())
                        .build()
        );
        String good2 = Utils.convertToJSON(
                new MapBuilder()
                        .put("f1", "text")
                        .put("f2", "text")
                        .put("f3", "text")
                        .build()
        );

        String wrong1 = Utils.convertToJSON(
                new MapBuilder()
                        .put("f1", "text")
                        .put("f2", "text")
                        .put("f3", "text")
                        .put("f4", "text")
                        .build()
        );

        String wrong2 = Utils.convertToJSON(
                new MapBuilder()
                        .put("f1", "text")
                        .put("f2", "text")
                        .put("f3", new MapBuilder()
                                .put("f1", "text")
                                .put("f2", "text")
                                .put("f3", "text")
                                .put("f4", "text")
                                .build())
                        .build()
        );
        String wrong3 = Utils.convertToJSON(
                new MapBuilder()
                        .put("f1", "text")
                        .put("f2", "text")
                        .put("f3", new MapBuilder()
                                .put("f1", "text")
                                .put("f2", "text")
                                .put("f3", "text")
                                .build())
                        .put("f4", "text")
                        .build()
        );
        assertThrows(ApplicationException.class, () -> new StructureParser(wrong1));
        assertThrows(ApplicationException.class, () -> new StructureParser(wrong2));
        assertThrows(ApplicationException.class, () -> new StructureParser(wrong3));
        try {
            new ApplicationException(good1);
            new ApplicationException(good2);
        } catch (ApplicationException e) {
            Assert.fail();
        }
    }

    public static void assertThrows(Class<ApplicationException> applicationExceptionClass, ThrowingRunnable o) {
        try {
            o.run();
        } catch (Exception e) {
            Assert.assertEquals(applicationExceptionClass, e.getClass());
            System.out.println(e.getMessage());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    // {"title":"resource1","active":true,"editable":"","structure":{"obj1":{"obj1Text":"text","obj2":{"obj2text":"text"}},"text1":"text"}}
    @Test
    void convertToString() throws ApplicationException {
        Map<String, Object> map = new HashMap<>();
        map.put("obj1", new MapBuilder().put("obj1Text", "text").put("obj2", new MapBuilder().put("obj2text", "text").build()).build());
        map.put("text1", "text");
        String result = Utils.convertToJSON(map);
        Assert.assertEquals("{\"text1\":\"text\",\"obj1\":{\"obj1Text\":\"text\",\"obj2\":{\"obj2text\":\"text\"}}}", result);
    }

    @Test
    void convertValueMapToString() throws ApplicationException {
        Map<String, Object> map = new HashMap<>();
        map.put("obj1", new MapBuilder().put("obj1Text", "text").put("obj2", new MapBuilder().put("obj2text", "text").build()).build());
        map.put("text1", "text");
        String result = Utils.convertToJSON(map);
        Assert.assertEquals("{\"text1\":\"text\",\"obj1\":{\"obj1Text\":\"text\",\"obj2\":{\"obj2text\":\"text\"}}}", result);
    }

    @BeforeEach
    public void init() {
        ObjectFactory.register(ObjectFactory.Type.BOOLEAN.name(), BooleanFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DOUBLE.name(), DoubleFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.LONG.name(), LongFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.TEXT.name(), TextFactory.getInstance());
    }

    @Test
    void getStructureMap() {
    }

    @Test
    void getObjectValue() {
    }

    @Test
    void getMap() {
    }

    @Test
    void testConvertToString() throws ApplicationException {
        Map<String, Object> arrayContent = new MapBuilder()
                .put("test", "text")
                .build();
        Map<String, Object> map = new MapBuilder()
                .put("obj", new MapBuilder()
                        .put("adsa", "text")
                        .build())
                .put("fds", Arrays.asList(new Map[]{arrayContent}))
                .build();
        String result = Utils.convertToJSON(map);
        Assert.assertEquals("{\"fds\":[{\"test\":\"text\"}],\"obj\":{\"adsa\":\"text\"}}", result);
    }

    private void putParameter(String name, String valueOf) {
        ParameterM parameterM = new ParameterM();
        parameterM.setCode(name);
        parameterM.setValue(valueOf);
        MyCacheImpl.add(MyCache.Domain.PARAMETER, parameterM);
    }

}