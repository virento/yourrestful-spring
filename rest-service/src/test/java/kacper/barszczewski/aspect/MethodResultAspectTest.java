package kacper.barszczewski.aspect;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.resources.ResourceRecordRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.ResourceServiceImpl;
import kacper.barszczewski.services.iterfaces.ResourceService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class MethodResultAspectTest {

    private ResourceService resourceService;

    @MockBean
    private ResourceRepository resourceRepository;
    @MockBean
    private ResourceRecordRepository resourceRecordRepository;


    @Before
    public void beforeEach() {
        ResourceServiceImpl resourceService = new ResourceServiceImpl();
        resourceService.setResourceRepository(resourceRepository);
        resourceService.setResourceRecordRepository(resourceRecordRepository);

        AspectJProxyFactory factory = new AspectJProxyFactory(resourceService);
        MethodResultAspect aspect = new MethodResultAspect();
        factory.addAspect(aspect);
        this.resourceService = factory.getProxy();
    }

    @Test
    public void afterReturningNullValue() {
        final Long resourceId = 200L;

        Mockito.when(resourceRepository.findById(resourceId)).thenReturn(Optional.of(new ResourceM()));
        final ResourceM resource = resourceService.getResource(resourceId);
        Assert.assertNotNull(resource);
    }

    @Test
    public void afterReturningNullValue_return_null() {
        final Long resourceId = 200L;
        try {
            resourceService.getResource(resourceId);
            Assert.fail();
        } catch (ApplicationException exception) {
            Assert.assertEquals(new ApplicationException(ErrorCode.RESOURCE_NOT_FOUND, resourceId).getMessage(), exception.getMessage());
        }
    }

    @Test
    public void afterReturningNullValue_return_null_resource_url() {
        final String resourceUrl = UUID.randomUUID().toString();
        ApplicationException applicationException = null;
        try {
            resourceService.getResource(resourceUrl);
            Assert.fail();
        } catch (ApplicationException e) {
            applicationException = e;
            Assert.assertEquals(new ApplicationException(ErrorCode.RESOURCE_URL_NOT_FOUND, resourceUrl).getMessage(), e.getMessage());
        }
        Assert.assertEquals(new ApplicationException(ErrorCode.RESOURCE_URL_NOT_FOUND, resourceUrl).getMessage(), applicationException.getMessage());
    }

    @Test
    public void afterReturningNullValue_return_null_resource_record() {
        final Long resourceId = 100L;
        final Long recordId = 200L;
        Mockito.when(resourceRecordRepository.findById(any())).thenReturn(Optional.empty());
        ApplicationException applicationException = null;
        try {
            resourceService.getResourceRecord(resourceId, recordId);
            Assert.fail();
        } catch (UndeclaredThrowableException e) {
            Throwable exception = e.getUndeclaredThrowable();
            if (!(exception instanceof ApplicationException)) {
                Assert.fail("Exception not instance of ApplicationException");
            }
            applicationException = (ApplicationException) exception;
        } catch (ApplicationException e) {
            applicationException = e;
        }
        Assert.assertEquals(new ApplicationException(ErrorCode.RESOURCE_RECORD_NOT_FOUND, resourceId, recordId).getMessage(), applicationException.getMessage());
    }
}