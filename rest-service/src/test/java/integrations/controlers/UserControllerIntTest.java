package integrations.controlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import integrations.IntegrationTestConfiguration;
import kacper.barszczewski.WebSecurityConfigInitializer;
import kacper.barszczewski.filters.CustomAuthenticationFilter;
import kacper.barszczewski.filters.ExceptionHandlerFilter;
import kacper.barszczewski.filters.ResourceRedirectFilter;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.pojo.resource.request.WSResourceObject;
import kacper.barszczewski.resources.UserRepository;
import kacper.barszczewski.services.AuthToken;
import kacper.barszczewski.services.iterfaces.ResourceService;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import org.apache.tomcat.util.json.JSONParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import utils.MapBuilder;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.LinkedHashMap;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {IntegrationTestConfiguration.class, WebSecurityConfigInitializer.class, WebSecurityConfigInitializer.ApiWebSecurityConfigurationAdapter.class})
@TestPropertySource(locations = "classpath:application.properties")
public class UserControllerIntTest {

    private MockMvc mockMvc;

    private static final String USER_USERNAME = "kacper";
    private static final String USER_EMAIL = "kacper@test.pl";
    private static final String USER_PASSWORD = "kacper123";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private WebApplicationContext context;

    @Qualifier("userAuthenticationImpl")
    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(springSecurity())
                .addFilters(
                        new CustomAuthenticationFilter(),
                        new ExceptionHandlerFilter(),
                        new ResourceRedirectFilter())
                .build();
    }

    private UserM createUser() {
        UserM userM = new UserM();
        userM.setUsername(USER_USERNAME);
        userM.setPassword(passwordEncoder.encode(USER_PASSWORD));
        userM.setEmail(USER_EMAIL);
        userM.setStatus(UserM.UserStatus.ACTIVE);
        userM.setType(UserM.UserType.USER);
        return userRepository.save(userM);
    }

    private UserM createUser(String username, String email) {
        UserM userM = new UserM();
        userM.setUsername(username);
        userM.setPassword(passwordEncoder.encode(USER_PASSWORD));
        userM.setEmail(email);
        userM.setStatus(UserM.UserStatus.ACTIVE);
        userM.setType(UserM.UserType.USER);
        return userRepository.save(userM);
    }

    @Test
    public void resource_create_test() throws Exception {
        createUser();
        AuthToken authToken = loginUser(USER_USERNAME, USER_PASSWORD);
        WSResourceObject resourceObject = createResourceRequestObject("New title");
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        MockHttpServletResponse resultActions = mockMvc.perform(prepareCall("/user/resource", authToken)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(resourceObject)))
                .andDo(print()).andReturn().getResponse();

        LinkedHashMap<String, Object> object = new JSONParser(resultActions.getContentAsString()).object();
        Assert.assertEquals(201, resultActions.getStatus());
        System.out.println("test");
    }

    @Test
    public void resource_create_share() throws Exception {
        UserM user1 = createUser("user3", "user3@test.pl");
        UserM user2 = createUser("user2", "user2@test.pl");
        AuthToken authToken1 = loginUser("user3", USER_PASSWORD);
        AuthToken authToken2 = loginUser("user2", USER_PASSWORD);
        WSResourceObject resourceObject = createResourceRequestObject("New title");
        MockHttpServletResponse resultActions = mockMvc.perform(prepareCall("/user/resource", authToken1)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(resourceObject)))
                .andDo(print()).andReturn().getResponse();
        Assert.assertEquals(201, resultActions.getStatus());
        LinkedHashMap<String, Object> object = new JSONParser(resultActions.getContentAsString()).object();

        resultActions = mockMvc.perform(prepareCall("/user/" + object.get("url") + "/share", authToken1)
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(new MapBuilder().put("nick", "user2").build()))).andDo(print())
                .andReturn().getResponse();
        Assert.assertEquals(204, resultActions.getStatus());

        resultActions = mockMvc.perform(get("/user/" + object.get("url")).header("Authorization", authToken2.getToken()))
                .andDo(print())
                .andReturn().getResponse();
        Assert.assertEquals(200, resultActions.getStatus());
    }

    public MockHttpServletRequestBuilder prepareCall(String url, AuthToken authToken) throws Exception {
        return post(url)
                .header("Authorization", authToken.getToken());
    }

    public WSResourceObject createResourceRequestObject(String title) {
        WSResourceObject resource = new WSResourceObject();
        resource.setTitle(title);
        resource.setStructure(new MapBuilder()
                .put("id", "Long")
                .put("name", "Text")
                .put("price", "Double")
                .put("long_value", "Long")
                .build());
        resource.setRecords(Arrays.asList(
                new MapBuilder()
                        .put("id", 100L)
                        .put("name", "Janek ola bola ula")
                        .put("price", 2221212.22231d)
                        .put("long_value", 1345678231L)
                        .build(),
                new MapBuilder()
                        .put("id", 200L)
                        .put("name", "Uas busa dusa kusa")
                        .put("price", 0.231313214351d)
                        .put("long_value", 34567890918726351L)
                        .build()
        ));
        return resource;
    }

    private AuthToken loginUser(String userName, String password) throws Exception {
        byte[] response = mockMvc.perform(post("/login")
                .contentType("application/json")
                .content("{\"login\":\"" + userName + "\",\"password\":\"" + password + "\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsByteArray();
        return new ObjectMapper().readValue(response, AuthToken.class);
    }

}
