package integrations;

import kacper.barszczewski.SpringRestServiceApplication;
import kacper.barszczewski.WebSecurityConfigInitializer;
import kacper.barszczewski.config.CustomProperties;
import kacper.barszczewski.controllers.AuthenticationControllerTest;
import kacper.barszczewski.controllers.BasicControllerTest;
import kacper.barszczewski.controllers.UserControllerTest;
import kacper.barszczewski.services.ResourceServiceImplTest;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@TestConfiguration
@FlywayDataSource
@EnableJpaRepositories
@EnableAspectJAutoProxy
@EnableConfigurationProperties(CustomProperties.class)
@ComponentScan(basePackageClasses = {SpringRestServiceApplication.class}, excludeFilters =
        {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {AuthenticationControllerTest.AuthenticationControllerTestConfiguration.class, BasicControllerTest.BasicControllerTestConfiguration.class, UserControllerTest.BasicControllerTestConfiguration.class, ResourceServiceImplTest.BasicControllerTestConfiguration.class})
        })
@Import({WebSecurityConfigInitializer.class, WebSecurityConfigInitializer.ApiWebSecurityConfigurationAdapter.class})
public class IntegrationTestConfiguration {
}
