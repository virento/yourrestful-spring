package utils;

import kacper.barszczewski.models.UserM;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;

@Getter
@Setter
@EqualsAndHashCode
public class AuthenticationTest implements Authentication {

    private boolean authenticated = false;
    private String username;
    private UserM principal;

    @SneakyThrows
    public AuthenticationTest(Long id, String username) {
        this.username = username;
        UserM userM = new UserM();
        userM.setInfoCD(new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").parse("10:10:10 01/01/2020"));
        userM.setUsername(username);
        userM.setId(id);
        setPrincipal(userM);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        this.authenticated = b;
    }

    @Override
    public String getName() {
        return username;
    }
}
