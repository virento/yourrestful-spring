package utils;

import java.util.HashMap;
import java.util.Map;

public class MapBuilder {

    private Map<String, Object> obj = new HashMap<>();

    public MapBuilder() {

    }

    public MapBuilder put(String key, Object object) {
        obj.put(key, object);
        return this;
    }

    public Map<String, Object> build() {
        return obj;
    }


}
