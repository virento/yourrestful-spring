package security;

import kacper.barszczewski.WebSecurityConfigInitializer;
import kacper.barszczewski.controllers.BasicController;
import kacper.barszczewski.controllers.UserController;
import kacper.barszczewski.models.assemblers.ResourceRecordModelAssembler;
import kacper.barszczewski.services.AuthenticationServiceImpl;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebSecurityConfigInitializer.class, WithoutLoggedUserTest.WithoutLoggedUserTestConfiguration.class})
@WebAppConfiguration
public class WithoutLoggedUserTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void apiTest1() throws Exception {
        mvc.perform(get("/api/50"))
                .andExpect(unauthenticated());
    }

    @Test
    public void apiTest2() throws Exception {
        mvc.perform(get("/api"))
                .andExpect(unauthenticated());
    }

    @Test
    public void apiTest3() throws Exception {
        mvc.perform(post("/api"))
                .andExpect(unauthenticated());
    }

    @Test
    public void apiTest4() throws Exception {
        mvc.perform(put("/api/123"))
                .andExpect(status().isNotFound())
                .andExpect(unauthenticated());
    }

    @Test
    public void apiTest5() throws Exception {
        mvc.perform(delete("/api"))
                .andExpect(status().isNotFound())
                .andExpect(unauthenticated());
    }

    @TestConfiguration
    static class WithoutLoggedUserTestConfiguration {
        @Bean
        public AuthenticationService authenticationService() {
            return new AuthenticationServiceImpl();
        }
    }

}
