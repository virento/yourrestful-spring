alter table sys_parameters
    add spr_public boolean not null default false;
update sys_parameters
set
    spr_public = true
where spr_code in (
                   'MAX_RECORDS', 'MAX_RESOURCES'
    );