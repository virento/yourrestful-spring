create sequence seq_sur_id start with 50 increment by 50;
create table sys_users (
                           sur_id number(10) default next value for seq_sur_id,
                           sur_info_cd timestamp not null default current_timestamp,
                           sur_info_md timestamp,
                           sur_info_rd timestamp,
                           sur_username varchar(50) not null,
                           sur_firstname varchar(100),
                           sur_lastname varchar(100),
                           sur_password clob not null,
                           sur_email varchar(100) not null,
                           sur_status varchar(1) not null,
                           sur_type varchar(1) not null,
                           primary key (sur_id)
);
create unique index uniq_info_rd_username on sys_users(
                                                       sur_info_rd,
                                                       sur_username
    );
create unique index uniq_info_rd_email on sys_users(
                                                    sur_info_rd,
                                                    sur_email
    );
create unique index uniq_username on sys_users(sur_username);
create unique index uniq_email on sys_users(sur_email);
create sequence seq_ure_id start with 50 increment by 50;
create table usr_resources (
                               ure_id number(10) default next value for seq_ure_id,
                               ure_info_cd timestamp not null default current_timestamp,
                               ure_info_md timestamp,
                               ure_info_rd timestamp,
                               ure_sur_id number(10),
                               ure_name varchar(50),
                               ure_type varchar(1) not null,
                               ure_structure clob not null,
                               ure_active boolean not null default false,
                               ure_url varchar(50),
                               primary key (ure_id)
);
alter table usr_resources
    add constraint fk_sur_id
        foreign key (ure_sur_id)
            references sys_users (sur_id);
create unique index uniq_ure_info_rd_ure_sur_id_ure_name on usr_resources(
                                                                          ure_info_rd,
                                                                          ure_sur_id,
                                                                          ure_name
    );
create unique index uniq_ure_sur_id_ure_name on usr_resources(
                                                              ure_sur_id,
                                                              ure_name
    );
create sequence seq_urr_id start with 50 increment by 50;
create table usr_resources_records (
                                       urr_id number(10) default next value for seq_urr_id,
                                       urr_info_cd timestamp not null default current_timestamp,
                                       urr_info_cu number(10),
                                       urr_info_md timestamp,
                                       urr_info_mu number(10),
                                       urr_info_rd timestamp,
                                       urr_info_ru number(10),
                                       urr_ure_id number(10) not null,
                                       urr_value clob not null,
                                       constraint usr_resources_records_pkey
                                       primary key (urr_id)
);
alter table usr_resources_records
    add constraint fk_ure_id
        foreign key (urr_ure_id)
            references usr_resources (ure_id);
alter table usr_resources_records
    add constraint fk_urr_info_cu
        foreign key (urr_info_cu)
            references sys_users (sur_id);
alter table usr_resources_records
    add constraint fk_urr_info_mu
        foreign key (urr_info_mu)
            references sys_users (sur_id);
alter table usr_resources_records
    add constraint fk_urr_info_ru
        foreign key (urr_info_ru)
            references sys_users (sur_id);
create sequence seq_urs_id start with 50 increment by 50;
create table usr_resources_share (
                                     urs_id number(10) default next value for seq_urs_id,
                                     urs_audit_cd timestamp not null default current_timestamp,
                                     urs_sur_id number(10) not null,
                                     urs_ure_id number(10) not null,
                                     primary key (urs_id)
);
alter table usr_resources_share
    add constraint fk_urs_sur_id
        foreign key (urs_sur_id)
            references sys_users (sur_id);
alter table usr_resources_share
    add constraint fk_urs_ure_id
        foreign key (urs_ure_id)
            references usr_resources (ure_id);
create sequence seq_svr_id start with 50 increment by 50;
create table sys_verification (
                                  svr_id number(10) default next value for seq_svr_id,
                                  svr_info_cd timestamp not null default current_timestamp,
                                  svr_info_md timestamp,
                                  svr_sur_id number(10) not null,
                                  svr_status varchar(1) not null,
                                  svr_value clob not null,
                                  svr_type varchar(1) not null,
                                  primary key (svr_id)
);
alter table sys_verification
    add constraint fk_svr_sur_id
        foreign key (svr_sur_id)
            references sys_users (sur_id);