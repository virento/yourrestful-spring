insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'PASSWORD_VERIFICATION_MESSAGE',
           'Reset password request!<br/>Please click on the following link if you want to reset the password:<br/>'
       );
insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'PASSWORD_VERIFICATION_TITLE',
           'Reset password'
       );
insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'MESSAGE_FROM_NAME',
           'YourRestful Administration'
       );