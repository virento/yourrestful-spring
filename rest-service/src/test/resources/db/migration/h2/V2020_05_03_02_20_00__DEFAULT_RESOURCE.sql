insert into usr_resources (
    ure_name,
    ure_type,
    ure_structure,
    ure_active,
    ure_url
)
values (
           'UNIVERSAL',
           'UNIVERSAL',
           '{id: Long, name: Text, surname: Text, age: Long, paycheck: Double}',
           'true',
           'api'
       );
insert into usr_resources_records (
    urr_ure_id,
    urr_value
)
values (
           (
               select ure_id
               from usr_resources
               where ure_type = 'UNIVERSAL'
           ),
           '{id: 1, name: ''Janek'', surname: ''Jankowski'', age: 20, paycheck: 20.22}'
       );
insert into usr_resources_records (
    urr_ure_id,
    urr_value
)
values (
           (
               select ure_id
               from usr_resources
               where ure_type = 'UNIVERSAL'
           ),
           '{id: 2, name: ''Bartek'', surname: ''Barkowski'', age: 10, paycheck: 20.00}'
       );
insert into usr_resources_records (
    urr_ure_id,
    urr_value
)
values (
           (
               select ure_id
               from usr_resources
               where ure_type = 'UNIVERSAL'
           ),
           '{id: 3, name: ''Marek'', surname: ''Markowski'', age: 9, paycheck: 20}'
       );
insert into usr_resources_records (
    urr_ure_id,
    urr_value
)
values (
           (
               select ure_id
               from usr_resources
               where ure_type = 'UNIVERSAL'
           ),
           '{id: 4, name: ''Wartek'', surname: ''Wartkowski'', age: 24, paycheck: 20.12}'
       );
insert into usr_resources_records (
    urr_ure_id,
    urr_value
)
values (
           (
               select ure_id
               from usr_resources
               where ure_type = 'UNIVERSAL'
           ),
           '{id: 5, name: ''Kamil'', surname: ''Kamilowski'', age: 50, paycheck: 200.22}'
       );
insert into usr_resources_records (
    urr_ure_id,
    urr_value
)
values (
           (
               select ure_id
               from usr_resources
               where ure_type = 'UNIVERSAL'
           ),
           '{id: 6, name: ''Ola'', surname: ''Olowska'', age: 60, paycheck: 0.22}'
       );