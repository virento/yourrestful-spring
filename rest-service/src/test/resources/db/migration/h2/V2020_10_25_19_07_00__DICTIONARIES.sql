create sequence seq_spr_id start with 50 increment by 50;
create table sys_parameters (
                                spr_id number(10) default next value for seq_spr_id,
                                spr_info_cd timestamp not null default current_timestamp,
                                spr_info_md timestamp,
                                spr_code varchar(100),
                                spr_value text,
                                primary key (spr_id)
);
insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'MAX_RECORDS',
           '20'
       );
insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'MAX_RESOURCES',
           '5'
       );