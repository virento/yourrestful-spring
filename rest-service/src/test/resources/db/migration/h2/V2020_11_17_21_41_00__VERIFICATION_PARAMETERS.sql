-- alter table sys_parameters
--     alter COLUMN spr_value clob;
insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'VERIFICATION_MESSAGE',
           'Welcome in Cinema Club!<br/>Please activate your account by clicking the following link:<br/>'
       );
insert into sys_parameters (
    spr_code,
    spr_value
)
values (
           'VERIFICATION_TITLE',
           'Confirm your email'
       );