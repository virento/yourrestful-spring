alter table usr_resources_records
    drop constraint usr_resources_records_pkey;
alter table usr_resources_records
    add constraint usr_resources_records_pkey
        primary key (
                     urr_ure_id,
                     urr_id
            );