package kacper.barszczewski.validators;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.method.annotation.InitBinderDataBinderFactory;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import java.util.List;

/**
 * Override {@link RequestMappingHandlerAdapter} and implement custom {@link #createDataBinderFactory(List)}
 */
public class CustomRequestMappingHandlerAdapter extends RequestMappingHandlerAdapter {

    /**
     * @see CustomServletDataBinderFactory
     */
    @NotNull
    @Override
    protected InitBinderDataBinderFactory createDataBinderFactory(@NotNull List<InvocableHandlerMethod> binderMethods) {
        return new CustomServletDataBinderFactory(binderMethods, getWebBindingInitializer());
    }
}
