package kacper.barszczewski.validators;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;

/**
 * Simply override {@link #getPropertyValue(PropertyTokenHolder)}
 *
 */
public class CustomBeanWrapperImpl extends BeanWrapperImpl {
    public CustomBeanWrapperImpl(Object target) {
        super(target);
    }


    /**
     * Because new validation was added {@link kacper.barszczewski.adnotation.validators.MultiFieldNotNull}
     * this method was unable to find value of multi field name
     *
     * Simple replace actualName (name of the filed in class {@link #getRootClass()}) so its value is equal
     * to first property name from list of properties
     *
     * @see BeanWrapperImpl
     */
    @Override
    protected Object getPropertyValue(PropertyTokenHolder tokens) throws BeansException {
        if (tokens.canonicalName.contains("[") && tokens.canonicalName.contains("]")) {
            tokens.actualName = tokens.canonicalName.substring(1, tokens.canonicalName.indexOf(","));
            tokens.keys = new String[0];
        }
        return super.getPropertyValue(tokens);
    }
}
