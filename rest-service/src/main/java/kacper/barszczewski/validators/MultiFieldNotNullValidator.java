package kacper.barszczewski.validators;

import kacper.barszczewski.adnotation.validators.MultiFieldNotNull;
import kacper.barszczewski.utils.ClassUtil;
import lombok.Getter;
import lombok.Setter;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

@Getter
@Setter
public class MultiFieldNotNullValidator implements ConstraintValidator<MultiFieldNotNull, Object> {

    private String[] fields;
    private String message;

    @Override
    public void initialize(MultiFieldNotNull constraintAnnotation) {
        setFields(constraintAnnotation.fields());
        setMessage(constraintAnnotation.message());
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        for (String field : getFields()) {
            if (ClassUtil.getFieldValue(value, field) != null) {
                return true;
            }
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message)
                .addPropertyNode(Arrays.toString(getFields()))
                .addConstraintViolation();
        return false;
    }

}
