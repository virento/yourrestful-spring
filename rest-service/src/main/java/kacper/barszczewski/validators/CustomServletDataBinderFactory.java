package kacper.barszczewski.validators;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;

import java.util.List;

/**
 * Override {@link ServletRequestDataBinderFactory} and implement custom {@link #createBinderInstance(Object, String, NativeWebRequest)}
 */
public class CustomServletDataBinderFactory extends ServletRequestDataBinderFactory {
    public CustomServletDataBinderFactory(List<InvocableHandlerMethod> binderMethods, WebBindingInitializer initializer) {
        super(binderMethods, initializer);
    }


    /**
     * @see kacper.barszczewski.validators.CustomBeanWrapperImpl
     */
    @NotNull
    @Override
    protected ServletRequestDataBinder createBinderInstance(Object target, @NotNull String objectName, @NotNull NativeWebRequest request) {
        return new CustomExtendedServletRequestDataBinder(target, objectName);
    }
}
