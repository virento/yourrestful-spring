package kacper.barszczewski.validators;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanWrapper;
import org.springframework.validation.BeanPropertyBindingResult;

/**
 * Override {@link BeanPropertyBindingResult} and implement custom {@link #createBeanWrapper()}
 */
public class CustomBeanPropertyBindingResult extends BeanPropertyBindingResult {

    public CustomBeanPropertyBindingResult(Object target, String objectName, boolean autoGrowNestedPaths, int autoGrowCollectionLimit) {
        super(target, objectName, autoGrowNestedPaths, autoGrowCollectionLimit);
    }

    /**
     * @see CustomBeanWrapperImpl
     */
    @NotNull
    protected BeanWrapper createBeanWrapper() {
        if (getTarget() == null) {
            throw new IllegalStateException("Cannot access properties on null bean instance '" + getObjectName() + "'");
        }
        return new CustomBeanWrapperImpl(getTarget());
    }
}
