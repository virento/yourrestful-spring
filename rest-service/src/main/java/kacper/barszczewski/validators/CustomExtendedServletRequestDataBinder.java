package kacper.barszczewski.validators;

import org.jetbrains.annotations.NotNull;
import org.springframework.validation.AbstractPropertyBindingResult;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.web.bind.ServletRequestDataBinder;

/**
 * Override {@link ServletRequestDataBinder} and implement custom {@link #createBeanPropertyBindingResult()}
 */
public class CustomExtendedServletRequestDataBinder extends ServletRequestDataBinder {

    private MessageCodesResolver messageCodesResolver;

    public CustomExtendedServletRequestDataBinder(Object target, String objectName) {
        super(target, objectName);
    }


    /**
     * @see CustomBeanPropertyBindingResult
     */
    @NotNull
    @Override
    protected AbstractPropertyBindingResult createBeanPropertyBindingResult() {
        BeanPropertyBindingResult result = new CustomBeanPropertyBindingResult(getTarget(),
                getObjectName(), isAutoGrowNestedPaths(), getAutoGrowCollectionLimit());

        if (getConversionService() != null) {
            result.initConversion(getConversionService());
        }
        if (this.messageCodesResolver != null) {
            result.setMessageCodesResolver(this.messageCodesResolver);
        }

        return result;
    }

    /**
     * Because {@link #messageCodesResolver} is private instance of superclass it cannot be access
     * <p>
     * This provide simple workaround so when #MessageCodesResolver is set, we keep reference to it fo farther use.
     *
     * @see #createBeanPropertyBindingResult()
     */
    @Override
    public void setMessageCodesResolver(MessageCodesResolver messageCodesResolver) {
        super.setMessageCodesResolver(messageCodesResolver);
        this.messageCodesResolver = messageCodesResolver;
    }
}
