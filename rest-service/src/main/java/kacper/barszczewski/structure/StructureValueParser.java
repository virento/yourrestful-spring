package kacper.barszczewski.structure;

import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;

import java.util.Map;

public class StructureValueParser {

    private Map<String, Object> map;

    public StructureValueParser(String structure) {
        try {
            map = new JSONParser(structure).object();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Object> getMap() {
        return map;
    }
}
