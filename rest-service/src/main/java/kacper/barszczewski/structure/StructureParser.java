package kacper.barszczewski.structure;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.controllers.exceptions.ValidationException;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.pojo.ValidationError;
import kacper.barszczewski.utils.EntityFactory;
import kacper.barszczewski.utils.Utils;
import kacper.barszczewski.utils.factories.AbstractFactory;
import kacper.barszczewski.utils.factories.ObjectFactory;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StructureParser extends StructureMap<AbstractFactory> {

    private Set<String> usedValues = new HashSet<>();

    /**
     * Constructor
     * As a result, {@link StructureParser} will hold functionality to parse raw string value
     * to map values.
     *
     * @param structure String representation of structure
     */
    public StructureParser(String structure) throws ApplicationException {
        super(structure);
    }

    @Override
    protected Map<String, AbstractFactory> getStructureMap() {
        return EntityFactory.createStructureMap();
    }

    @Override
    protected AbstractFactory getObjectValue(String value) throws ApplicationException {
        return ObjectFactory.getFactory(value);
    }

    /**
     * Parse raw value of all records in list
     *
     * @param records List of records to parse
     */
    public void pars(List<ResourceRecordM> records) {
        records.forEach(this::pars);
    }

    /**
     * Method parse raw record value to Map. Result is set to ParsedValue in {@link ResourceRecordM}
     *
     * @param record Record which value will be parsed
     */
    public void pars(ResourceRecordM record) {
        Map<String, Object> valuesMap = new StructureValueParser(record.getValue()).getMap();
        record.setParsedValue(valuesMap);
    }

    /**
     * Validate record according to structure
     *
     * @param record Record which object map value will be validated
     */
//    TODO: Look at https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api#hateoas for validation info, "Errors"
    public void validateRecord(Map<String, Object> record) throws ApplicationException {
        usedValues = new HashSet<>();
        List<ValidationError> validationErrors = validateRecord(record, null);
        addArrayFields(usedValues);
        if (usedValues.size() != getMap().size()) {
            getMap().forEach((key, abstractFactory) -> {
                if (!usedValues.contains(key)) {
                    validationErrors.add(new ValidationError(key, ErrorCode.MISSING_FIELD));
                }
            });
        }
        if (!validationErrors.isEmpty()) {
            throw new ValidationException(validationErrors);
        }
    }

    private void addArrayFields(Set<String> usedValues) {
        getParsedObject().forEach((key, value) -> {
            if (value instanceof ArrayList) {
                addToList(key, usedValues, (Map<String, Object>) ((ArrayList<Object>) value).get(0));
            }
        });
    }

    private void addToList(String masterKey, final Set<String> usedValues, Map<String, Object> map) {
        if (masterKey == null) {
            masterKey = "";
        } else {
            masterKey = masterKey + ".";
        }
        final String newKey = masterKey;
        if (map == null || map.isEmpty()) {
            return;
        }
        map.forEach((key, value) -> {
            if (value instanceof ArrayList) {
                addToList(newKey + key, usedValues, (Map<String, Object>) ((ArrayList<?>) value).get(0));
            } else if (value instanceof Map) {
                addToList(newKey + value, usedValues, (Map<String, Object>) value);
            } else {
                usedValues.add(newKey + value);
            }
        });
    }

    private List<ValidationError> validateRecord(Map<String, Object> record, String key) throws ApplicationException {
        List<ValidationError> validationErrors = new ArrayList<>();
        for (Map.Entry<String, Object> entry : record.entrySet()) {
            String fullKey = key == null ? entry.getKey() : key + "." + entry.getKey();
            if (entry.getValue() instanceof Map) {
                //noinspection unchecked
                validationErrors.addAll(validateRecord((Map<String, Object>) entry.getValue(), fullKey));
                continue;
            } else if (entry.getValue() instanceof List) {
                List<?> values = (List<?>) entry.getValue();
                for (Object value : values) {
                    validationErrors.addAll(validateRecord((Map<String, Object>) value, fullKey));
                }
                continue;
            }
            String entryKey = fullKey;
            if (!getMap().containsKey(entryKey)) {
                throw new ApplicationException("Wrong structure format");
            }
            usedValues.add(entryKey);
            ErrorCode errorCode;
            if (entry.getValue() == null) {
                errorCode = ErrorCode.NULL_VALUE;
            } else {
                errorCode = getMap().get(entryKey).validateValue(entry.getValue());
            }
            if (errorCode != null) {
                validationErrors.add(new ValidationError(entryKey, errorCode));
            }
        }
        return validationErrors;
    }

    public void validateRecords(List<Map<String, Object>> records) throws ApplicationException {
        List<Long> recordIds = new ArrayList<>();
        List<ValidationError> validationErrors = new ArrayList<>();
        for (Map<String, Object> record : records) {
            List<ValidationError> errors = validateRecord(record, null);
            if (!errors.isEmpty()) {
                validationErrors.addAll(errors);
            }
            Long recordId = NumberUtils.toLong(String.valueOf(record.get("id")));
            if (recordIds.contains(recordId)) {
                throw new ApplicationException("Duplicated records id: {}", recordId);
            }
            recordIds.add(recordId);
        }
        if (!validationErrors.isEmpty()) {
            throw new ValidationException(validationErrors);
        }
    }

    public void validateKeys(Map<String, Object> map) throws ApplicationException {
        List<ValidationError> errors = validateKeys(map, null);
        if (!errors.isEmpty()) {
            throw new ApplicationException(ErrorCode.VALIDATION_ERROR, errors);
        }
    }

    public List<ValidationError> validateKeys(Map<String, Object> map, String key) {
        List<ValidationError> validationErrors = new ArrayList<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String fullKey = key == null ? entry.getKey() : key + "." + entry.getKey();
            if (entry.getValue() instanceof Map) {
                //noinspection unchecked
                validationErrors.addAll(validateKeys((Map<String, Object>) entry.getValue(), fullKey));
                continue;
            } else if (entry.getValue() instanceof List) {
                List<?> values = (List<?>) entry.getValue();
                for (Object value : values) {
                    validationErrors.addAll(validateKeys((Map<String, Object>) value, fullKey));
                }
                continue;
            }
            String entryKey = fullKey;
            if (!getMap().containsKey(entryKey)) {
                validationErrors.add(new ValidationError(entry.getKey(), ErrorCode.FIELD_NOT_PRESENT));
            }
        }
        return validationErrors;
    }

    public List<ResourceRecordM> generateRecords(Map<String, Object> recordObject, int toGenerate) throws ApplicationException {
        List<ResourceRecordM> records = new ArrayList<>();
        for (int i = 0; i < toGenerate; i++) {
            ResourceRecordM record = new ResourceRecordM();
            Map<String, Object> value = new HashMap<>();

            for (Map.Entry<String, AbstractFactory> entry : getMap().entrySet()) {
                List<String> generators = Utils.getMapValue(recordObject, entry.getKey());
                for (String generator : generators) {
                    if (generator == null) {
                        throw new ApplicationException(ErrorCode.MISSING_GENERATOR, entry.getKey());
                    }
                    Object generatedValue = entry.getValue().generate(i, generator);
                    if (entry.getKey().equals("id")) {
                        record.setId((Long) generatedValue);
                    }
                    Utils.putMapValue(recordObject, value, entry.getKey(), generatedValue);
                }
            }
            record.setValue(Utils.convertToJSON(value));
            records.add(record);
        }
        return records;
    }
}
