package kacper.barszczewski.structure;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.Parameters;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter(value = AccessLevel.PACKAGE)
public abstract class StructureMap<T> {

    private final Map<String, T> map;

    {
        map = getStructureMap();
    }

    private Map<String, Object> parsedObject;

    protected abstract Map<String, T> getStructureMap();

    public StructureMap(String structure) throws ApplicationException {
        JSONParser parser = new JSONParser(structure);
        try {
            parsedObject = parser.parseObject();
        } catch (ParseException e) {
            throw new ApplicationException(ErrorCode.WRONG_RESOURCE_STRUCTURE);
        }
        startProcessing(parsedObject, "");
        Optional<String[]> nestLevel = map.keySet().stream().map(s -> s.split("\\.")).max(Comparator.comparingInt(strings -> strings.length));
        if (nestLevel.isPresent()) {
            checkMaxNestLevel(nestLevel.get().length);
        }
    }

    private void checkMaxNestLevel(int nestLevel) {
        int maxLevel = Parameters.STRUCTURE_MAX_NESTING.getValue();
        if (maxLevel < nestLevel) {
            throw new ApplicationException(ErrorCode.NEST_LEVEL_EXCEED, nestLevel, maxLevel);
        }

    }

    protected void startProcessing(Map<String, Object> structure, String prefix) throws ApplicationException {
        Set<Map.Entry<String, Object>> entries = structure.entrySet();

        // check number of fields
        int maxFields = Parameters.STRUCTURE_MAX_FIELDS.getValue();
        if (entries.size() > maxFields) {
            throw new ApplicationException(ErrorCode.STRUCTURE_FIELD_NUMBER_EXCEED, entries.size(), maxFields);
        }
        for (Map.Entry<String, Object> entry : entries) {
            String key = prefix + (prefix.length() == 0 ? "" : ".") + entry.getKey();
            Object value = entry.getValue();
            String textValue;
            if (value instanceof Map) {
                startProcessing((Map<String, Object>) value, key);
                continue;
            } else if (value instanceof List) {
                startProcessing((Map<String, Object>) ((List<?>) value).get(0), key);
                continue;
            }
            textValue = (String) value;
            getMap().put(key, getObjectValue(textValue));
        }
    }

    protected abstract T getObjectValue(String value) throws ApplicationException;
}
