package kacper.barszczewski.filters;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResourceRedirectFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String resourceId = request.getHeader("X-RESOURCE-ID");
        if (resourceId != null) {
            String recordId = "/";
            if (request.getRequestURI().split("/").length > 2) {
                recordId += request.getRequestURI().split("/")[2];
            }
            request.getRequestDispatcher("/user/" + resourceId + recordId).forward(request, response);
//            filterChain.doFilter(request, response);
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
