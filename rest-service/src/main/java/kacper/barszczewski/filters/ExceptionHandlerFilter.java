package kacper.barszczewski.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.helpers.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExceptionHandlerFilter extends OncePerRequestFilter {

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            if (!(e instanceof ApplicationException)) {
                e.printStackTrace();
            }
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.getWriter().write(new ObjectMapper().writeValueAsString(new ErrorResponse(e)));
        }
    }
}
