package kacper.barszczewski.filters;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.services.AuthToken;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@Component
public class CustomAuthenticationFilter extends OncePerRequestFilter {

    private final String HEADER = "Authorization";
    private final String HEADER_PREFIX = "Bearer ";

    private AuthenticationService authenticationService;

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        injectBeans(request.getServletContext());

        String token = request.getHeader(HEADER);
        if (token != null && token.startsWith(HEADER_PREFIX)) {
            token = token.substring(HEADER_PREFIX.length());
        }
        AuthToken authenticated = authenticationService.isAuthenticated(token);
        if (authenticated == null) {
            if (SecurityContextHolder.getContext().getAuthentication() == null || !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
                SecurityContextHolder.clearContext();
            }
        } else {
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(authenticated.getUser(), null,
                    authenticated.getAuthorities().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        filterChain.doFilter(request, response);
    }

    private void injectBeans(ServletContext context) throws ApplicationException {
        WebApplicationContext webContext = WebApplicationContextUtils.getWebApplicationContext(context);
        if (webContext == null) {
            throw new ApplicationException(ErrorCode.ERROR);
        }
        if (authenticationService == null) {
            setAuthenticationService(webContext.getBean(AuthenticationService.class));
        }
    }

    @Autowired
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
