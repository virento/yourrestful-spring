package kacper.barszczewski.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "custom")
public class CustomProperties {

    @Deprecated
    private MailOptions mailOptions;
    private String verificationTokenLink;
    private String passwordTokenLink;

    @Getter
    @Setter
    public static class MailOptions {
        private Boolean enable;
        private String host;
        private String hostPort;
        private String socketFactoryPort;
        private String socketFactoryClass;
        private String email;
        private String password;
    }

}
