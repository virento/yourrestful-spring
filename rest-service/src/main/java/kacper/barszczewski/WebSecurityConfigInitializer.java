package kacper.barszczewski;

import kacper.barszczewski.filters.CustomAuthenticationFilter;
import kacper.barszczewski.filters.ExceptionHandlerFilter;
import kacper.barszczewski.filters.ResourceRedirectFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.DispatcherType;
import java.util.Arrays;

@Configuration
@EnableWebMvc
public class WebSecurityConfigInitializer {

    @Configuration
    @Order(1)
    @EnableWebSecurity
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        private AutowireCapableBeanFactory beanFactory;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .csrf().disable()
                    .cors().and()
                    .antMatcher("/api/**").addFilterBefore(new ResourceRedirectFilter(), BasicAuthenticationFilter.class)
                    .antMatcher("**")
                    .authorizeRequests()
                    .antMatchers("/api/**", "/login").permitAll()
                    .antMatchers("/user/api/structure").permitAll()
                    .antMatchers("/swagger-ui*").permitAll()
                    .antMatchers("/user/**").authenticated()
                    .antMatchers("/system/**").authenticated()
                    .and()
                    .addFilterAfter(new CustomAuthenticationFilter(), ResourceRedirectFilter.class)
                    .addFilterBefore(new ExceptionHandlerFilter(), CustomAuthenticationFilter.class);
        }

        @Bean
        public FilterRegistrationBean<ExceptionHandlerFilter> exceptionHandlerFilterFilterRegistrationBean() {
            FilterRegistrationBean<ExceptionHandlerFilter> registration = new FilterRegistrationBean<>();
            ExceptionHandlerFilter myFilter = new ExceptionHandlerFilter();
            beanFactory.autowireBean(myFilter);
            registration.setFilter(myFilter);
            registration.setOrder(0);
            registration.setDispatcherTypes(DispatcherType.FORWARD, DispatcherType.REQUEST);
            return registration;
        }

        @Bean
        public FilterRegistrationBean<CustomAuthenticationFilter> customAuthenticationFilterFilterRegistrationBean() {
            FilterRegistrationBean<CustomAuthenticationFilter> registration = new FilterRegistrationBean<>();
            CustomAuthenticationFilter myFilter = new CustomAuthenticationFilter();
            beanFactory.autowireBean(myFilter);
            registration.setFilter(myFilter);
            registration.setOrder(1);
            registration.setDispatcherTypes(DispatcherType.FORWARD, DispatcherType.REQUEST);
            return registration;
        }

    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.setAllowedMethods(Arrays.asList("POST", "OPTIONS", "GET", "DELETE", "PUT", "PATCH"));
        config.setAllowedHeaders(Arrays.asList("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization", "X-RESOURCE-ID"));
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

}
