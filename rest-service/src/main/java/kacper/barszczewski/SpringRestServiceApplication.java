package kacper.barszczewski;

import kacper.barszczewski.config.CustomProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@FlywayDataSource
@EnableJpaRepositories
@EnableAspectJAutoProxy
@EnableConfigurationProperties(CustomProperties.class)
public class SpringRestServiceApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return super.configure(builder).sources(SpringRestServiceApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringRestServiceApplication.class, args);
    }
}
