package kacper.barszczewski.aspect;

import kacper.barszczewski.controllers.exceptions.ApplicationException;

public abstract class AccessChecker {

    protected abstract Boolean haveAccess(String level, Object[] params, Long userId) throws ApplicationException;

}
