package kacper.barszczewski.aspect;

import kacper.barszczewski.adnotation.HaveAccess;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.UserM;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class RESTMethodInvokeAspect {

    private ApplicationContext applicationContext;

    @Before(value = "@annotation(haveAccess)")
    public void checkAccess(JoinPoint jp, HaveAccess haveAccess) throws ApplicationException {
        AccessChecker bean = applicationContext.getBean(haveAccess.checker());
        int paramsLength = haveAccess.params().length;
        if (paramsLength == 0) {
            paramsLength = jp.getArgs().length;
        }
        Object[] args = new Object[paramsLength];
        if (paramsLength == jp.getArgs().length) {
            System.arraycopy(jp.getArgs(), 0, args, 0, paramsLength);
        }
        Object[] jpArgs = jp.getArgs();
        int[] params = haveAccess.params();
        for (int i = 0; i < params.length; i++) {
            args[i] = jpArgs[params[i]];
        }
        if (!bean.haveAccess(haveAccess.level(), args, getCurrentUserId())) {
            throw new AccessDeniedException("");
        }
    }

    private Long getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object user = authentication.getPrincipal();
        if (user instanceof UserM) {
            return ((UserM) user).getId();
        }
        return null;
    }

    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
