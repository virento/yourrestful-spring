package kacper.barszczewski.aspect;

import kacper.barszczewski.models.CodeModel;
import kacper.barszczewski.models.IdModel;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.MyCache;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@Aspect
@Slf4j
public class CacheAspect {

    @Around(value = "(execution(kacper.barszczewski.models.CodeModel+ *(..)) ) && " +
            "(execution(* kacper.barszczewski.resources.*.*find*(..)) || execution(* kacper.barszczewski.resources.*.*get*(..))) && " +
            "(args(String) || args(Long))")
    public IdModel getCacheObjectByCode(ProceedingJoinPoint pjp) throws Throwable {
        Class<?> returnType = ((MethodSignature) pjp.getSignature()).getReturnType();
        MyCache.Domain domain = MyCache.Domain.getDomain(returnType);
        Object argument = pjp.getArgs()[0];
        if (domain == null) {
            return (IdModel) pjp.proceed();
        }
        Optional<IdModel> cachedObject = MyCacheImpl.getCacheObject(domain, argument);
        if (cachedObject.isPresent()) {
            log.debug("Fetching from cache: domain=" + domain.name() + ", by: " + argument.toString());
            return cachedObject.get();
        }
        IdModel proceed = (IdModel) pjp.proceed();
        log.debug("Adding to cache: domain=" + domain.name() + ", by: " + argument.toString() + ", " + proceed);
        if (proceed != null) {
            MyCacheImpl.add(domain, proceed);
        }
        return proceed;
    }

    @Around(value = "" +
            "(execution(* kacper.barszczewski.resources.*.*save*(..))) " +
            " && args(codeModel)")
    public Object setCacheObjectByCode(ProceedingJoinPoint pjp, IdModel codeModel) throws Throwable {
        MyCache.Domain domain = MyCache.Domain.getDomain(codeModel.getClass());
        if (domain == null) {
            return pjp.proceed();
        }
        CodeModel returnValue = (CodeModel) pjp.proceed();
        MyCacheImpl.add(domain, returnValue);
        log.info("Update code cache item: id=" + codeModel.getId() + ": " + returnValue);
        return returnValue;
    }
}
