package kacper.barszczewski.aspect.checkers;

import kacper.barszczewski.aspect.AccessChecker;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.UserM;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AdminAccessChecker extends AccessChecker {

    @Override
    protected Boolean haveAccess(String level, Object[] params, Long userId) throws ApplicationException {
        UserM currentUser = getCurrentUser();
        return currentUser != null && UserM.UserType.ADMIN.equals(currentUser.getType());
    }

    private UserM getCurrentUser() {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user instanceof UserM) {
            return (UserM) user;
        }
        return null;
    }
}
