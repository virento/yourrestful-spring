package kacper.barszczewski.aspect.checkers;

import kacper.barszczewski.adnotation.HaveAccess;
import kacper.barszczewski.aspect.AccessChecker;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.resources.ResourceShareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ResourceAccessChecker extends AccessChecker {

    private ResourceShareRepository resourceShareRepository;

    private ResourceRepository resourceRepository;

    @Override
    protected Boolean haveAccess(String level, Object[] params, Long userId) throws ApplicationException {
        String resourceUrl = (String) params[0];
        if (Objects.equals(userId, resourceRepository.getResourceOwner(resourceUrl))) {
            return true;
        } else if (level.equals(HaveAccess.Level.PRIVATE)) {
            accessDenied(resourceUrl);
        }
        if (resourceShareRepository.haveAccess(userId, resourceUrl)) {
            return true;
        }
        throw new ApplicationException(ErrorCode.RESOURCE_NOT_ACCESSIBLE, resourceUrl);
    }

    private void accessDenied(String resourceUrl) throws ApplicationException {
        throw new ApplicationException(ErrorCode.RESOURCE_NOT_ACCESSIBLE, resourceUrl);
    }

    @Autowired
    public void setResourceShareRepository(ResourceShareRepository resourceShareRepository) {
        this.resourceShareRepository = resourceShareRepository;
    }

    @Autowired
    public void setResourceRepository(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }
}
