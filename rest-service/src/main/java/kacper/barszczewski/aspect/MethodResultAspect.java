package kacper.barszczewski.aspect;

import kacper.barszczewski.adnotation.ExceptionOnResultNull;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class MethodResultAspect {

    @AfterReturning(value = "@annotation(exceptionOnResultNull)", returning = "ret")
    public void afterReturningNullValue(JoinPoint jp, ExceptionOnResultNull exceptionOnResultNull, Object ret) throws ApplicationException {
        if (ret == null) {
            throw new ApplicationException(exceptionOnResultNull.errorCode(), jp.getArgs());
        }
    }

}
