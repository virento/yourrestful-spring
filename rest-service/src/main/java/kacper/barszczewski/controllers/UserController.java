package kacper.barszczewski.controllers;

import kacper.barszczewski.adnotation.HaveAccess;
import kacper.barszczewski.aspect.checkers.ResourceAccessChecker;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.assemblers.ResourceModelAssembler;
import kacper.barszczewski.models.assemblers.UserModelAssembler;
import kacper.barszczewski.models.assemblers.UserResourceRecordModelAssembler;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.pojo.UserObject;
import kacper.barszczewski.pojo.resource.RecordObject;
import kacper.barszczewski.pojo.resource.ResourceObject;
import kacper.barszczewski.pojo.resource.request.WSResourceGeneration;
import kacper.barszczewski.pojo.resource.request.WSResourceObject;
import kacper.barszczewski.pojo.resource.request.WSResourceShare;
import kacper.barszczewski.services.iterfaces.ResourceService;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController {

    private ResourceService resourceService;

    private ResourceModelAssembler resourceModelAssembler;

    private UserResourceRecordModelAssembler userResourceRecordModelAssembler;

    private UserAuthenticationService userDetailsService;

    private UserModelAssembler userModelAssembler;

    @GetMapping("/resource/{resourceUrl}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class)
    public ResponseEntity<ResourceObject> getResource(@PathVariable String resourceUrl) throws ApplicationException {
        ResourceM resource = resourceService.getResource(resourceUrl);
        return resourceModelAssembler.toOkModel(resource);
    }

    @GetMapping("/resource")
    public CollectionModel<ResourceObject> getResources(@ModelAttribute ListSearchCriteria searchCriteria) {
        List<ResourceM> resources = resourceService.getAllResources(getUserId(), searchCriteria);
        return resourceModelAssembler.toPageCollection(resources);
    }

    @PostMapping("/resource")
    public ResponseEntity<ResourceObject> createResource(@Valid @RequestBody WSResourceObject wsResource) throws ApplicationException {
        ResourceM resource = resourceService.createResourceWithRecords(wsResource.toEntity(getUserId()), wsResource.getRecords(), wsResource.getGeneration());
        return resourceModelAssembler.toCreateModel(resource);
    }

    @PatchMapping("/resource/{resourceUrl}")
    @HaveAccess(level = HaveAccess.Level.PRIVATE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<ResourceObject> patchResource(@PathVariable String resourceUrl, @RequestBody WSResourceObject wsResource) throws ApplicationException {
        ResourceM resource = resourceService.getResource(resourceUrl);
        ResourceM newResource = wsResource.toEntity(getUserId());
        resource.setName(newResource.getName() != null ? newResource.getName() : resource.getName());
        resource.setEditable(newResource.getEditable() != null ? newResource.getEditable() : resource.getEditable());
        resource.setActive(newResource.getActive() != null ? newResource.getActive() : resource.getActive());
        if (newResource.getStructure() != null) {
            resource.setStructure(newResource.getStructure());
        }
        newResource = resourceService.modifyResource(resource, wsResource.getRecords());
        return resourceModelAssembler.toOkModel(newResource);
    }

    @GetMapping("/{resourceUrl}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class, params = {0})
    public CollectionModel<RecordObject> getResourceRecords(@PathVariable String resourceUrl, @ModelAttribute ListSearchCriteria searchCriteria) throws ApplicationException {
        ResourceM resource = resourceService.getResource(resourceUrl);
        List<ResourceRecordM> records = resourceService.getResourceRecords(resource.getId(), searchCriteria);
        return userResourceRecordModelAssembler.toPageCollection(resource, records);
    }

    @GetMapping("/{resourceUrl}/{recordId}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<RecordObject> getResourceRecord(@PathVariable String resourceUrl, @PathVariable Long recordId) throws ApplicationException {
        ResourceM resource = resourceService.getResource(resourceUrl);
        ResourceRecordM record = resourceService.getResourceRecord(resource.getId(), recordId);
        return userResourceRecordModelAssembler.toOkModel(resource, record);
    }

    @PostMapping("/{resourceUrl}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<RecordObject> postResourceRecord(@PathVariable String resourceUrl, @RequestBody Map<String, Object> body) throws ApplicationException {
        ResourceM resource = resourceService.getResource(resourceUrl);
        ResourceRecordM resourceRecordM = resourceService.createResourceRecord(resource.getId(), body, !resource.getEditable() || !resource.getActive());
        return userResourceRecordModelAssembler.toCreateModel(resource, resourceRecordM);
    }

    @PutMapping("/{resourceUrl}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<RecordObject> putResourceRecord(@PathVariable String resourceUrl, @RequestBody Map<String, Object> body) throws ApplicationException {
        ResourceM resource = resourceService.getResource(resourceUrl);
        ResourceRecordM record;
        boolean simulate = resource.getEditable() && resource.getActive();
        Long recordId = NumberUtils.toLong(String.valueOf(body.get("id")));
        boolean newRecord = !resourceService.existsResourceRecord(resource.getId(), recordId);
        if (!newRecord) {
            record = resourceService.modifyResourceRecord(resource.getId(), body, simulate);
            return userResourceRecordModelAssembler.toOkModel(resource, record);
        } else {
            record = resourceService.createResourceRecord(resource.getId(), body, simulate);
            return userResourceRecordModelAssembler.toCreateModel(resource, record);
        }
    }

    @PatchMapping("/{resourceUrl}/{recordId}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<RecordObject> patchResourceRecord(@PathVariable String resourceUrl, @PathVariable Long recordId, @RequestBody Map<String, Object> body) throws ApplicationException {
        ResourceM resourceM = resourceService.getResource(resourceUrl);
        ResourceRecordM record = resourceService.getResourceRecord(resourceM.getId(), recordId);
        if (record == null) {
            throw new ApplicationException("Record with id {} not found", recordId);
        }
        if (body.containsKey("id")) {
            throw new ApplicationException(ErrorCode.RESOURCE_RECORD_ID_MODIFYING);
        }
        body = record.merge(body);
        record = resourceService.modifyResourceRecord(resourceM.getId(), body, !resourceM.getEditable() || !resourceM.getActive());
        return userResourceRecordModelAssembler.toOkModel(resourceM, record);
    }

    @DeleteMapping("/{resourceUrl}/{recordId}")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<Object> deleteResourceRecord(@PathVariable String resourceUrl, @PathVariable Long recordId) throws ApplicationException {
        ResourceM resourceM = resourceService.getResource(resourceUrl);
        ResourceRecordM record = resourceService.getResourceRecord(resourceM.getId(), recordId);
        if (record == null) {
            throw new ApplicationException("Record with id {} not found", recordId);
        }
        resourceService.removeResourceRecord(resourceUrl, record.getId());
        return ResponseEntity.noContent().build();
    }


    @PostMapping("/{resourceUrl}/share")
    @HaveAccess(level = HaveAccess.Level.PRIVATE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<Object> addUserToResource(@PathVariable String resourceUrl, @Valid @RequestBody WSResourceShare shareRequest) throws ApplicationException {
        checkIfUserExists(shareRequest.getEmail(), shareRequest.getNick());
        if (userDetailsService.isSameUser(getUserId(), shareRequest.getNick(), shareRequest.getEmail())) {
            throw new ApplicationException(ErrorCode.SHARE_SAME_USER);
        }
        resourceService.shareResourceToUser(resourceUrl, shareRequest.getNick(), shareRequest.getEmail());
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{resourceUrl}/shares")
    @HaveAccess(level = HaveAccess.Level.PRIVATE, checker = ResourceAccessChecker.class, params = {0})
    public CollectionModel<UserObject> getShares(@PathVariable String resourceUrl, @ModelAttribute ListSearchCriteria searchCriteria) throws ApplicationException {
        List<UserM> resourceShares = resourceService.getResourceShares(resourceUrl, searchCriteria);
        return userModelAssembler.toPageCollection(resourceShares);
    }

    // userInfo can be email or nick
    @DeleteMapping("/{resourceUrl}/share/{userInfo}")
    @HaveAccess(level = HaveAccess.Level.PRIVATE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<Object> removeUserFromResource(@PathVariable String resourceUrl, @PathVariable String userInfo) throws ApplicationException {
        checkIfUserExists(userInfo, userInfo);
        if (userDetailsService.isSameUser(getUserId(), userInfo, userInfo)) {
            throw new ApplicationException(ErrorCode.SHARE_SAME_USER);
        }
        resourceService.removeShareUserFromResource(resourceUrl, userInfo, userInfo);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{resourceUrl}/structure")
    @HaveAccess(level = HaveAccess.Level.SHARE, checker = ResourceAccessChecker.class)
    public ResponseEntity<ResourceObject> getResourceStructure(@PathVariable String resourceUrl) throws ApplicationException {
        ResourceM resourceM = resourceService.getResource(resourceUrl);
        return resourceModelAssembler.toOkModel(resourceM, ResourceModelAssembler.Variant.RESOURCE_ONLY);
    }

    @DeleteMapping("/{resourceUrl}")
    @HaveAccess(level = HaveAccess.Level.PRIVATE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<Object> removeResource(@PathVariable String resourceUrl) throws ApplicationException {
        resourceService.removeResource(resourceUrl);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{resourceUrl}/generate")
    @HaveAccess(level = HaveAccess.Level.PRIVATE, checker = ResourceAccessChecker.class, params = {0})
    public ResponseEntity<Object> generateRecords(@PathVariable String resourceUrl, @Valid @RequestBody WSResourceGeneration recordObject) throws ApplicationException {
        int generated = resourceService.generateRecords(resourceUrl, recordObject);
        Map<String, Object> data = new HashMap<>();
        data.put("generated", generated);
        return ResponseEntity.ok(data);
    }

    private void checkIfUserExists(String email, String nick) throws ApplicationException {
        if (!userDetailsService.existsUser(email, nick)) {
            String msg = "";
            if (nick != null) msg += "nick=" + nick;
            if (email != null)
                msg += (msg.length() == 0 ? "" : ", ") + "email=" + email;
            throw new ApplicationException(ErrorCode.USER_NOT_FOUND, msg);
        }
    }

    /**
     * #######################################################
     * # Injecting services into fields                      #
     * #######################################################
     */

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @Autowired
    public void setResourceModelAssembler(ResourceModelAssembler resourceModelAssembler) {
        this.resourceModelAssembler = resourceModelAssembler;
    }

    @Autowired
    public void setUserResourceRecordModelAssembler(UserResourceRecordModelAssembler userResourceRecordModelAssembler) {
        this.userResourceRecordModelAssembler = userResourceRecordModelAssembler;
    }

    @Autowired
    public void setUserDetailsService(@Qualifier("userAuthenticationImpl") UserAuthenticationService userDetails) {
        this.userDetailsService = userDetails;
    }

    @Autowired
    public void setUserModelAssembler(UserModelAssembler userModelAssembler) {
        this.userModelAssembler = userModelAssembler;
    }
}
