package kacper.barszczewski.controllers.exceptions;

public enum ErrorCode {

    ERROR("Error"),
    RESOURCE_NOT_FOUND("Resource with id {} not found"),
    RESOURCE_URL_NOT_FOUND("Resource with url {} not found"),
    UNIVERSAL_RESOURCE_NOT_FOUND("Universal resource not found"),
    RESOURCE_RECORD_NOT_FOUND("Record with id {} not found"),
    RESOURCE_NOT_UNIQUE_NAME("Resource with name {} already exists"),
    VALIDATION_ERROR("Validation error"),
    RESOURCE_NOT_ACCESSIBLE("You can not access resource with url {}"),
    USER_NOT_FOUND("User with {} not found"),
    SHARE_SAME_USER("User performing action and user related to action are the same users"),
    SHARE_ALREADY_ACCESSIBLE("User can already access resource {}"),
    SHARE_NOT_ACCESSIBLE("User can't access resource {}"),
    BAD_CREDENTIALS("Wrong username/password"),
    REFRESH_TOKEN_EXPIRED("Refresh token expired"),
    TOKEN_EXPIRED("Token has expired"),
    REGISTER_EMAIL_USED("You can't use this email"),
    REGISTER_USERNAME_USED("Username {} already taken"),
    RESOURCE_RECORD_EXISTS("Record with id {} already exists"),
    RESOURCE_RECORD_ID_MODIFYING("Can't modify id of record"),
    RESOURCE_NOT_EMPTY("Can't remove resource if its contains records"),
    LOGIN_FAILED("Bad credentials"),
    WRONG_RESOURCE_STRUCTURE("Wrong resource structure form"),
    WRONG_RECORD_STRUCTURE("Wrong record structure form"),
    RECORDS_LIMIT_EXCEEDED("Maximum number of records will be reached. Can't create new records, maximum is {}"),
    RESOURCES_LIMIT_EXCEEDED("Maximum number of resources will be reached. Can't create new resource, maximum is {}"),
    FIELD_NOT_PRESENT("field is not present"),
    UNPARSABLE_GENERATOR("Can't generate value from {}"),
    GENERATOR_MAX_TEXT("Wrong value {}, maximum text length is {}"),
    MISSING_GENERATOR("Missing expresion for field {}"),
    WRONG_PARAMETER("Parameter {} not found"),
    MISSING_FIELD("Missing field value"),
    WRONG_FIELD_TYPE("Field type {} does not exists"),
    USER_NOT_ACTIVE("User not active"),
    USER_WRONG_STATUS("Wrong user status"),
    WRONG_PARAMETER_TYPE("Wrong parameter {} type. Value must be type of {}"),
    NEST_LEVEL_EXCEED("Nest level exceed. Structure level: {}, max level: {}"),
    STRUCTURE_FIELD_NUMBER_EXCEED("Exceeded maximum number of field in nest level. Found {} fields, max is {}"),

    // Validations
    BOOLEAN_VALUE("must be boolean (true/false)"),
    DATE_VALUE("must be a date with the following format yyyy/MM/dd HH:mm:ss"),
    DOUBLE_VALUE("must be a double"),
    LONG_VALUE("must be a long"),
    STRING_VALUE("must be a string"),
    NULL_VALUE("must not be null"),

    //Verification
    NO_VERIFICATION("There is no verification associated with token {}"),
    VERIFICATION_NOT_MATCH("Provided token not match actual token"),
    VERIFICATION_RESENT("Link has expire, new link has been sent to your email"),

    EXPRESSION_EVALUATION("Expression result error. Probably returned value is too big or dividing by 0. {}")
    ;


    private final String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
