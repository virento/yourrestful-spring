package kacper.barszczewski.controllers.exceptions;

import kacper.barszczewski.pojo.ValidationError;

import java.util.List;

public class ValidationException extends ApplicationException {

    private List<ValidationError> validationErrorList;

    public ValidationException(List<ValidationError> validationErrorList) {
        super(ErrorCode.VALIDATION_ERROR);
        this.validationErrorList = validationErrorList;
    }

    public List<ValidationError> getValidationErrorList() {
        return validationErrorList;
    }
}
