package kacper.barszczewski.controllers.exceptions;

import org.slf4j.helpers.MessageFormatter;

public class ProgramException extends RuntimeException {
    public ProgramException(String message) {
        super(message);
    }

    public ProgramException(String message, String... args) {
        super(MessageFormatter.format(message, args).getMessage());
    }
}
