package kacper.barszczewski.controllers.exceptions;

import org.slf4j.helpers.MessageFormatter;

public class ApplicationException extends RuntimeException {

    private ErrorCode errorCode = ErrorCode.ERROR;

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String messageFormat, Object... args) {
        super(MessageFormatter.arrayFormat(messageFormat, args).getMessage());
    }

    public ApplicationException(ErrorCode error, Object... args) {
        super(MessageFormatter.arrayFormat(error.getMessage(), args).getMessage());
        setErrorCode(error);
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
