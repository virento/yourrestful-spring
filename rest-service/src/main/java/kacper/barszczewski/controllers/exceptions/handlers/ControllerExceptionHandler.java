package kacper.barszczewski.controllers.exceptions.handlers;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.helpers.SimpleModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.UndeclaredThrowableException;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler({ApplicationException.class, UndeclaredThrowableException.class})
    @ResponseBody
    public ResponseEntity<?> handleControllerException(HttpServletRequest request, Throwable ex) {
        ex.printStackTrace();
        return ResponseEntity.ok(ex.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<?> handleProgramException(HttpServletRequest request, Throwable ex) {
//        TODO: When exception is thrown, this method is called
        if (ex instanceof HttpRequestMethodNotSupportedException) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(new SimpleModel(ex.getMessage()));
        }
        ex.printStackTrace();
        return ResponseEntity.ok("Processing error");
    }

}
