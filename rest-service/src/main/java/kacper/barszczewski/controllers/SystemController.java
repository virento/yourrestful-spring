package kacper.barszczewski.controllers;

import kacper.barszczewski.adnotation.HaveAccess;
import kacper.barszczewski.aspect.checkers.AdminAccessChecker;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.assemblers.ParameterModelAssembler;
import kacper.barszczewski.models.assemblers.UserModelAssembler;
import kacper.barszczewski.models.criteriars.ParametersListCriteria;
import kacper.barszczewski.models.criteriars.UserListCriteria;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.pojo.ParameterForm;
import kacper.barszczewski.pojo.ParameterObject;
import kacper.barszczewski.pojo.UserForm;
import kacper.barszczewski.pojo.UserObject;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.services.iterfaces.ParameterService;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/system")
public class SystemController extends AbstractController {

    private ParameterModelAssembler parameterAssembler;

    private ParameterService parameterService;

    private UserModelAssembler userAssembler;

    private UserAuthenticationService userAuthenticationService;

    @GetMapping("/parameter/{parameterCode}")
    public ResponseEntity<ParameterObject> getSystemParameter(@PathVariable String parameterCode) throws ApplicationException {
        Parameters parameters = Parameters.get(parameterCode);
        if (parameters == null || !canAccessParameter(getUser(), parameters.getParameter())) {
            throw new ApplicationException(ErrorCode.WRONG_PARAMETER, parameterCode);
        }
        return parameterAssembler.toOkModel(parameters.getParameter());
    }

    @GetMapping("/parameter")
    public CollectionModel<ParameterObject> getSystemParameters(@ModelAttribute ParametersListCriteria criteria) {
        List<ParameterM> accessibleParameters = getParameters(criteria);
        long parametersCount = accessibleParameters.size();

        List<ParameterM> collect = accessibleParameters.stream()
                .skip(criteria.getPage() * criteria.getMaxResult())
                .limit(criteria.getMaxResult())
                .collect(Collectors.toList());
        ResultList<ParameterM> list = new ResultList<>(collect);
        list.setPage((long) criteria.getPage());
        list.setTotalPage(((long) Math.ceil((float) parametersCount / criteria.getMaxResult())));
        list.setTotalSize(parametersCount);

        return parameterAssembler.toPageCollection(list);
    }

    @GetMapping("/users")
    @HaveAccess(checker = AdminAccessChecker.class)
    public CollectionModel<UserObject> getUsers(@ModelAttribute UserListCriteria criteria) {
        return userAssembler.toPageCollection(userAuthenticationService.findUsers(criteria), UserModelAssembler.Variant.LIST);
    }

    @PatchMapping("/parameter/{paramCode}")
    @HaveAccess(checker = AdminAccessChecker.class)
    public ResponseEntity<Object> modifyParameter(@PathVariable String paramCode, @Valid @RequestBody ParameterForm parameterForm) {
        parameterService.modifyParameter(paramCode, parameterForm.getValue());
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/users/{username}")
    @HaveAccess(checker = AdminAccessChecker.class)
    public ResponseEntity<Object> modifyUser(@PathVariable String username, @Valid @RequestBody UserForm userForm) {
        userAuthenticationService.modifyUser(username, userForm);
        return ResponseEntity.noContent().build();
    }

    private boolean canAccessParameter(UserM user, ParameterM parameter) {
        if (parameter == null || parameter.getIsPublic()) {
            return true;
        }
        return user != null && UserM.UserType.ADMIN.equals(user.getType());
    }

    public UserM getUser() {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(user instanceof UserM)) {
            return null;
        }
        return (UserM) user;
    }

    @NotNull
    private List<ParameterM> getParameters(ParametersListCriteria criteria) {
        List<ParameterM> parameters = MyCacheImpl.getAll(MyCache.Domain.PARAMETER);
        final UserM currentUser = getUser();
        Stream<ParameterM> stream = parameters.stream()
                .sorted(Comparator.comparing(ParameterM::getCode));
        if (criteria.getCode() != null) {
            final String code = criteria.getCode().toLowerCase();
            stream = stream.filter(parameterM -> parameterM.getCode().toLowerCase().contains(code));
        }
        return stream
                .filter(parameterM -> canAccessParameter(currentUser, parameterM))
                .collect(Collectors.toList());
    }

    @Autowired
    public void setUserAuthenticationService(UserAuthenticationService userAuthenticationService) {
        this.userAuthenticationService = userAuthenticationService;
    }

    @Autowired
    public void setUserAssembler(UserModelAssembler userAssembler) {
        this.userAssembler = userAssembler;
    }

    @Autowired
    public void setParameterAssembler(ParameterModelAssembler parameterAssembler) {
        this.parameterAssembler = parameterAssembler;
    }

    @Autowired
    public void setParameterService(ParameterService parameterService) {
        this.parameterService = parameterService;
    }
}
