package kacper.barszczewski.controllers;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.assemblers.ResourceModelAssembler;
import kacper.barszczewski.models.assemblers.ResourceRecordModelAssembler;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.pojo.resource.RecordObject;
import kacper.barszczewski.pojo.resource.ResourceObject;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.ResourceService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BasicController extends AbstractController {

    private ResourceRecordModelAssembler resourceRecordModelAssembler;

    private ResourceModelAssembler resourceModelAssembler;

    private ResourceService resourceService;

    @GetMapping("/structure")
    public ResponseEntity<ResourceObject> getStructure() throws ApplicationException {
        ResourceM resourceM = resourceService.getUniversalResource();
        return resourceModelAssembler.toOkModel(resourceM, ResourceModelAssembler.Variant.RESOURCE_ONLY);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RecordObject> getData(@PathVariable Long id) throws ApplicationException {
        ResourceRecordM resource = resourceService.getResourceRecord(getUniversalResourceId(), id);
        return resourceRecordModelAssembler.toOkModel(resource);
    }

    @GetMapping("")
    public CollectionModel<RecordObject> getDataAll(@ModelAttribute ListSearchCriteria searchCriteria) throws ApplicationException {
        List<ResourceRecordM> records = resourceService.getResourceRecords(getUniversalResourceId(), searchCriteria);
        return resourceRecordModelAssembler.toPageCollection(records);
    }

    @PostMapping("")
    public ResponseEntity<RecordObject> postData(@RequestBody Map<String, Object> body) throws ApplicationException {
        ResourceRecordM record = resourceService.createResourceRecord(getUniversalResourceId(), body, true);
        return resourceRecordModelAssembler.toCreateModel(record);
    }

    @PutMapping("")
    public ResponseEntity<RecordObject> putData(@RequestBody Map<String, Object> body) throws ApplicationException {
        Long recordId = NumberUtils.toLong(String.valueOf(body.get("id")));
        boolean newRecord = !resourceService.existsResourceRecord(getUniversalResourceId(), recordId);
        ResourceRecordM record;
        if (!newRecord) {
            record = resourceService.modifyResourceRecord(getUniversalResourceId(), body, true);
            return ResponseEntity.ok(resourceRecordModelAssembler.toModel(record));
        } else {
            record = resourceService.createResourceRecord(getUniversalResourceId(), body, true);
            return resourceRecordModelAssembler.toCreateModel(record);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<RecordObject> patchData(@PathVariable Long id, @RequestBody Map<String, Object> body) throws ApplicationException {
        ResourceRecordM resourceRecord = resourceService.getResourceRecord(getUniversalResourceId(), id);
        if (resourceRecord == null) {
            throw new ApplicationException("Record with id {} not found", id);
        }
        body = resourceRecord.merge(body);
        body.put("id", id);
        ResourceRecordM record = resourceService.modifyResourceRecord(getUniversalResourceId(), body, true);
        return resourceRecordModelAssembler.toOkModel(record);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResourceRecordM> deleteData(@PathVariable Long id) throws ApplicationException {
        if (resourceService.getResourceRecord(getUniversalResourceId(), id) == null) {
            throw new ApplicationException("Record with id {} not found", id);
        }
        return ResponseEntity.noContent().build();
    }

    private Long getUniversalResourceId() {
        return MyCacheImpl.getUniversalResourceId();
    }

    /**
     * #######################################################
     * # Injecting services into fields                      #
     * #######################################################
     */

    @Autowired
    public void setResourceRecordModelAssembler(ResourceRecordModelAssembler resourceRecordModelAssembler) {
        this.resourceRecordModelAssembler = resourceRecordModelAssembler;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @Autowired
    public void setResourceModelAssembler(ResourceModelAssembler resourceModelAssembler) {
        this.resourceModelAssembler = resourceModelAssembler;
    }
}
