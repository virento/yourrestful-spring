package kacper.barszczewski.controllers;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.helpers.ErrorResponse;
import kacper.barszczewski.models.UserM;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.lang.reflect.UndeclaredThrowableException;

@Getter
@Setter
public abstract class AbstractController {

    @ExceptionHandler
    private ResponseEntity<Object> handleException(Throwable ex) throws Throwable {
        if (ex instanceof AccessDeniedException) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (ex instanceof UndeclaredThrowableException) {
            final Throwable undeclaredThrowable = ((UndeclaredThrowableException) ex).getUndeclaredThrowable();
            if (undeclaredThrowable instanceof ApplicationException) {
                ex = undeclaredThrowable;
            }
        }
        if (ex instanceof HttpMessageNotReadableException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        if (!(ex instanceof ApplicationException) && !(ex instanceof MethodArgumentNotValidException)) {
            ex.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new ErrorResponse((Exception) ex));
    }

    public Long getUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object user = authentication.getPrincipal();
        if (user instanceof UserM) {
            return ((UserM) user).getId();
        }
        return null;
    }
}
