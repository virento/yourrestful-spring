package kacper.barszczewski.controllers;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.pojo.UserObject;
import kacper.barszczewski.pojo.authentication.LoginForm;
import kacper.barszczewski.pojo.authentication.NewPasswordForm;
import kacper.barszczewski.pojo.authentication.RegisterForm;
import kacper.barszczewski.pojo.authentication.ResetPasswordForm;
import kacper.barszczewski.services.AuthToken;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping
public class AuthenticationController extends AbstractController {

    private static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
    private static final String GRANT_TYPE_CREDENTIAL = "credential";

    private UserAuthenticationService userDetails;

    @PostMapping(value = "/login")
    public ResponseEntity<Object> login(@RequestParam(name = "Authorization", required = false) String authorization,
                                        @RequestHeader(name = "Authorization", required = false) String header,
                                        @RequestParam(name = "grant_type", defaultValue = GRANT_TYPE_CREDENTIAL) String grandType,
                                        @RequestParam(name = "refresh_token", required = false) String refreshToken) throws ApplicationException {
        AuthToken authToken;
        if (GRANT_TYPE_REFRESH_TOKEN.equals(grandType)) {
            authToken = userDetails.authenticateByRefreshToken(refreshToken);
        } else {
            List<String> authorizations = new ArrayList<>();
            authorizations.add(authorization);
            authorizations.add(header);
            authToken = userDetails.authenticateByCredential(authorizations);
        }
        if (authToken != null) {
            return ResponseEntity.ok(authToken);
        }
        throw new ApplicationException(ErrorCode.LOGIN_FAILED);
    }

    @PostMapping(value = "/login", consumes = "application/json")
    public ResponseEntity<Object> login(@Valid @RequestBody LoginForm loginForm) throws ApplicationException {
        AuthToken authToken = userDetails.authenticateByCredential(Collections.singletonList(loginForm.getAsString()));
        if (authToken != null) {
            return ResponseEntity.ok(authToken);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PostMapping("/register")
    public ResponseEntity<Object> register(@Valid @RequestBody RegisterForm registerForm) throws ApplicationException {
        if (userDetails.saveUserByForm(registerForm)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/profile")
    public ResponseEntity<Object> getProfile() { //TODO: Change to user Assembler?
        UserM loggedUser = userDetails.getLoggedUser();
        return ResponseEntity.ok(UserObject.wrapDetailsUser(loggedUser));
    }

    @GetMapping("/verifyuser")
    public ResponseEntity<Object> verifyUser(@RequestParam(name = "token") String token, @RequestParam(name = "username") String username) throws ApplicationException {
        userDetails.verifyUser(token, username);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/resetPassword")
    public ResponseEntity<Object> resetPassword(@Valid @RequestBody ResetPasswordForm form) throws ApplicationException {
        userDetails.resetUserPassword(form.getEmail());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/newPassword")
    public ResponseEntity<Object> newPassword(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "username") String username,
            @Valid @RequestBody NewPasswordForm form) {
        userDetails.newPassword(token, username, form.getPassword());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Autowired
    public void setUserDetails(@Qualifier("userAuthenticationImpl") UserAuthenticationService userDetails) {
        this.userDetails = userDetails;
    }
}
