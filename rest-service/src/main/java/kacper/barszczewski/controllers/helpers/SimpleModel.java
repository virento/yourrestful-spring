package kacper.barszczewski.controllers.helpers;

import org.springframework.hateoas.EntityModel;

public class SimpleModel extends EntityModel<String> {

    public SimpleModel(String text) {
        super(text);
    }

}
