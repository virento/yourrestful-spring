package kacper.barszczewski.controllers.helpers;

import com.fasterxml.jackson.annotation.JsonInclude;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.controllers.exceptions.ValidationException;
import kacper.barszczewski.pojo.ValidationError;
import kacper.barszczewski.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Links;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ErrorResponse extends EntityModel<Object> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String data = Utils.getDateFormat().format(new Date());

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String description;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ValidationError> errors;

    public ErrorResponse(ApplicationException exception) {
        fillProperties(exception);
    }

    public ErrorResponse(Exception exception) {
        if (exception instanceof ValidationException) {
            fillValidationProperties((ValidationException) exception);
        } else if (exception instanceof ApplicationException) {
            fillProperties((ApplicationException) exception);
        } else if (exception instanceof MethodArgumentNotValidException) {
            fillProperties((MethodArgumentNotValidException) exception);
        } else {
            fillDefaultProperties();
        }
    }

    private void fillValidationProperties(ValidationException exception) {
        setErrorCode(exception.getErrorCode().name());
        setDescription(exception.getErrorCode().getMessage());
        setErrors(exception.getValidationErrorList());
    }

    private void fillDefaultProperties() {
        setErrorCode(ErrorCode.ERROR.getMessage());
        setDescription("Application error");
    }

    private void fillProperties(MethodArgumentNotValidException exception) {
        setErrorCode(ErrorCode.VALIDATION_ERROR.name());
        setDescription(ErrorCode.VALIDATION_ERROR.getMessage());
        List<ValidationError> validationErrors = new ArrayList<>();
        for (FieldError fieldError : exception.getBindingResult().getFieldErrors()) {
            validationErrors.add(new ValidationError(fieldError.getField(), fieldError.getDefaultMessage()));
        }
        setErrors(validationErrors);
    }

    private void fillProperties(ApplicationException exception) {
        setDescription(exception.getMessage());
        setErrorCode(exception.getErrorCode().name());
    }

    /**
     * Remove empty object Link[] from response
     * If at least one link is provided, response will contains object "link"
     */
    @Override
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Links getLinks() {
        Links links = super.getLinks();
        return links.isEmpty() ? null : links;
    }
}
