package kacper.barszczewski.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kacper.barszczewski.models.UserM;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@NoArgsConstructor
@Setter
public class AuthToken {

    @JsonIgnore
    private UserM user;

    @JsonIgnore
    private List<String> authorities = new ArrayList<>();

    @JsonIgnore
    private Date expirationRefreshTokenDate;

    @JsonIgnore
    private Date expirationDate;

    @JsonProperty("access_token")
    private String token;

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("expire_in")
    private Long expireIn;


    public AuthToken(UserM user, List<String> authorities, Date expirationDate, Date expirationRefreshTokenDate, String token, String refreshToken) {
        this.user = user;
        Collections.copy(authorities, this.authorities);
        this.expirationDate = expirationDate;
        this.token = token;
        this.refreshToken = refreshToken;
        this.expirationRefreshTokenDate = expirationRefreshTokenDate;
        this.expireIn = expirationDate.getTime() - new Date().getTime();
    }

}
