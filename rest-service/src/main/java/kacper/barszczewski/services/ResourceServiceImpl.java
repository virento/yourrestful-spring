package kacper.barszczewski.services;

import kacper.barszczewski.adnotation.ExceptionOnResultNull;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.ResourceShareM;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.models.persistence.ResourceRecordId;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.pojo.resource.request.WSResourceGeneration;
import kacper.barszczewski.resources.ResourceRecordRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.resources.ResourceShareRepository;
import kacper.barszczewski.resources.UserRepository;
import kacper.barszczewski.services.iterfaces.ResourceService;
import kacper.barszczewski.structure.StructureParser;
import kacper.barszczewski.utils.Utils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ResourceServiceImpl implements ResourceService {

    private ResourceRepository resourceRepository;

    private ResourceRecordRepository resourceRecordRepository;

    private UserRepository userRepository;

    private ResourceShareRepository resourceShareRepository;

    private EntityManager entityManager;

    @Override
    @ExceptionOnResultNull(errorCode = ErrorCode.RESOURCE_NOT_FOUND)
    public ResourceM getResource(Long id) {
        Optional<ResourceM> resource = resourceRepository.findById(id);
        if (!resource.isPresent()) {
            throw new ApplicationException(ErrorCode.RESOURCE_NOT_FOUND, id);
        }
        return resource.get();
    }

    @Override
    @ExceptionOnResultNull(errorCode = ErrorCode.RESOURCE_RECORD_NOT_FOUND)
    @Transactional
    public ResourceRecordM getResourceRecord(Long resourceId, Long recordId) throws ApplicationException {
        Optional<ResourceRecordM> optional = resourceRecordRepository.findById(new ResourceRecordId(resourceId, recordId));
        if (!optional.isPresent()) {
            return null;
        }
        ResourceRecordM record = optional.get();
        parseRecordValue(Collections.singletonList(record));
        return record;
    }

    @Override
    @Transactional
    public List<ResourceRecordM> getResourceRecords(Long resourceId, ListSearchCriteria searchCriteria) throws ApplicationException {
        Pageable pageable = searchCriteria.getPageable();
        List<ResourceRecordM> records = ResultList.wrap(resourceRecordRepository.findAllByResourceIdOrderById(resourceId, pageable));
        parseRecordValue(records);
        return records;
    }

    @Override
    @ExceptionOnResultNull(errorCode = ErrorCode.UNIVERSAL_RESOURCE_NOT_FOUND)
    public ResourceM getUniversalResource() {
        return MyCacheImpl.getUniversalResource();
    }

    @Override
    @Transactional
    public ResourceM createResource(ResourceM resource) throws ApplicationException {
        if (resourceRepository.existsByNameAndOwnerIdAndInfoRDIsNull(resource.getName(), resource.getOwnerId())) {
            throw new ApplicationException(ErrorCode.RESOURCE_NOT_UNIQUE_NAME, resource.getName());
        }

        int currentResources = resourceRepository.countResourceMByOwnerIdAndInfoRDIsNull(resource.getOwnerId());
        int maxResources = Parameters.MAX_RESOURCES.getValue();
        if (currentResources + 1 > maxResources) {
            throw new ApplicationException(ErrorCode.RESOURCES_LIMIT_EXCEEDED, maxResources);
        }
        resource = modifyResource(resource);
        return resource;
    }

    @Override
    @ExceptionOnResultNull(errorCode = ErrorCode.RESOURCE_URL_NOT_FOUND)
    public ResourceM getResource(String resourceUrl) throws ApplicationException {
        ResourceM resource = resourceRepository.getByUrl(resourceUrl);
        if (resource == null) {
            throw new ApplicationException(ErrorCode.RESOURCE_URL_NOT_FOUND, resourceUrl);
        }
        return resource;
    }

    @Override
    @Transactional
    public ResourceM createResourceWithRecords(ResourceM resource, List<Map<String, Object>> records, WSResourceGeneration generation) throws ApplicationException {
        if (records == null) {
            records = Collections.emptyList();
        }
        // Validate structure form
        new StructureParser(resource.getStructure());
        resource = createResource(resource);
        crateResourceRecords(resource, records, generation);
        return resource;
    }

    @Override
    @Transactional
    public void crateResourceRecords(ResourceM resource, List<Map<String, Object>> records, WSResourceGeneration generation) throws ApplicationException {
        StructureParser structureParser = new StructureParser(resource.getStructure());
        if (records != null && records.isEmpty()) {
            structureParser.validateRecords(records);
            for (Map<String, Object> recordValues : records) {
                ResourceRecordM record = new ResourceRecordM();
                record.setId(Long.valueOf((Integer) recordValues.get("id")));
                record.setResourceId(resource.getId());
                record.setValue(Utils.convertToJSON(recordValues));
                createResourceRecord(record);
            }
        }
        if (generation != null && generation.getContent() != null) {
            int i = generateRecords(resource.getUrl(), generation);
            resource.setGeneratedRecords(i);
        }
    }

    @Override
    @Transactional
    public ResourceM modifyResource(ResourceM resource, List<Map<String, Object>> records) throws ApplicationException {
        ResourceM existingResource = resourceRepository.findByOwnerIdAndName(resource.getOwnerId(), resource.getName());
        if (existingResource != null) {
            if (existingResource.getOwnerId().equals(resource.getOwnerId()) && !existingResource.getId().equals(resource.getId())) {
                throw new ApplicationException(ErrorCode.RESOURCE_NOT_UNIQUE_NAME, resource.getName());
            }
            if (!Utils.stringEqualIgnoreCaseRemoveSpace(existingResource.getStructure(), resource.getStructure())) {
                crateResourceRecords(resource, records, null);
            }
        }
        resource = modifyResource(resource);
        return resource;
    }

    @Override
    @Transactional
    public ResourceM modifyResource(ResourceM resource) throws ApplicationException {
        return resourceRepository.save(resource);
    }

    @Override
    public void shareResourceToUser(String resourceUrl, String nick, String email) throws ApplicationException {
        UserM user = userRepository.getUserMByEmailOrUsername(email, nick);
        if (resourceShareRepository.haveAccess(user.getId(), resourceUrl)) {
            throw new ApplicationException(ErrorCode.SHARE_ALREADY_ACCESSIBLE, resourceUrl);
        }
        ResourceShareM share = new ResourceShareM();
        ResourceM resource = getResource(resourceUrl);
        if (resource == null) {
            throw new ApplicationException(ErrorCode.RESOURCE_URL_NOT_FOUND, resourceUrl);
        }
        share.setResourceId(resource.getId());
        share.setUserId(user.getId());
        resourceShareRepository.save(share);
    }

    @Override
    public boolean existsResourceRecord(Long resourceId, Long recordId) {
        return resourceRecordRepository.existsById(new ResourceRecordId(resourceId, recordId));
    }

    @Override
    public List<UserM> getResourceShares(String resourceUrl, ListSearchCriteria searchCriteria) throws ApplicationException {
        Long resourceId = getResource(resourceUrl).getId();
        return ResultList.wrap(userRepository.findAllByShareResourceUrl(resourceId, searchCriteria.getPageable()));
    }

    @Override
    public void removeShareUserFromResource(String resourceUrl, String email, String nick) throws ApplicationException {
        Long resourceId = getResource(resourceUrl).getId();
        ResourceShareM share = resourceShareRepository.getByUserNickOrEmail(resourceId, email, nick);
        if (share == null) {
            throw new ApplicationException(ErrorCode.SHARE_NOT_ACCESSIBLE, resourceUrl);
        }
        resourceShareRepository.delete(share);
    }

    @Override
    public void removeResourceRecord(String resourceUrl, Long id) throws ApplicationException {
        Long resourceId = getResource(resourceUrl).getId();
        resourceRecordRepository.deleteById(new ResourceRecordId(resourceId, id));
    }

    @Override
    public void removeResource(String resourceUrl) throws ApplicationException {
        ResourceM resource = getResource(resourceUrl);
        boolean resourceRecordsExists = resourceRecordRepository.isResourceRecordsExists(resource.getId());
        if (resourceRecordsExists) {
            throw new ApplicationException(ErrorCode.RESOURCE_NOT_EMPTY);
        }

        resource.setInfoRD(new Date());
        modifyResource(resource);
    }

    @Override
    @Transactional
    public int generateRecords(String resourceUrl, WSResourceGeneration recordObject) throws ApplicationException {
        ResourceM resource = getResource(resourceUrl);
        int toGenerate = recordObject.getGenerate();
        StructureParser validator = new StructureParser(resource.getStructure());
        validator.validateKeys(recordObject.getContent());

        int maxGenerate = Parameters.MAX_RECORDS.getValue();
        int currentRecords = resourceRecordRepository.countResourceRecordMByResourceIdAndInfoRDIsNull(resource.getId());
        if (currentRecords + toGenerate > maxGenerate) {
            toGenerate = maxGenerate - currentRecords;
        }
        if (toGenerate == 0) {
            return toGenerate;
        }
        List<ResourceRecordM> records = validator.generateRecords(recordObject.getContent(), toGenerate);
        for (ResourceRecordM record : records) {
            record.setResourceId(resource.getId());
            createResourceRecord(record);
        }
        return records.size();
    }

    @Override
    public ResourceRecordM createResourceRecord(Long resourceId, Map<String, Object> body, boolean simulate) throws ApplicationException {
        if (!body.containsKey("id")) {
            throw new ApplicationException("Can't create resource without id");
        }
        long recordId = NumberUtils.toLong(String.valueOf(body.get("id")));
        if (resourceRecordRepository.existsById(new ResourceRecordId(resourceId, recordId))) {
            throw new ApplicationException(ErrorCode.RESOURCE_RECORD_EXISTS, recordId);
        }
        ResourceM resource = getResource(resourceId);
        StructureParser parser = new StructureParser(resource.getStructure());
        parser.validateRecord(body);
        ResourceRecordM resourceRecord = new ResourceRecordM();
        resourceRecord.setId(recordId);
        resourceRecord.setResourceId(resource.getId());
        resourceRecord.setParsedValue(body);
        resourceRecord.setValue(Utils.convertToJSON(body));
        if (!simulate) {
            return createResourceRecord(resourceRecord);
        }
        return resourceRecord;
    }

    @Override
    @Transactional
    public ResourceRecordM modifyResourceRecord(Long resourceId, Map<String, Object> body, boolean simulate) throws ApplicationException {
        if (!body.containsKey("id")) {
            throw new ApplicationException("Can't modify resource without id");
        }
        ResourceM resource = getResource(resourceId);
        StructureParser parser = new StructureParser(resource.getStructure());
        long recordId = NumberUtils.toLong(String.valueOf(body.get("id")));
        ResourceRecordM record = getResourceRecord(resource.getId(), recordId);
        record.setValue(Utils.convertToJSON(body));
        record.getParsedValue().putAll(body);
        parser.validateRecord(body);
        if (!simulate) {
            return modifyResourceRecord(record);
        }
        entityManager.detach(record);
        return record;
    }

    @Override
    @Transactional
    public ResourceRecordM modifyResourceRecord(ResourceRecordM resourceRecord) {
        return resourceRecordRepository.save(resourceRecord);
    }

    @Override
    @Transactional
    public ResourceRecordM createResourceRecord(ResourceRecordM resourceRecord) throws ApplicationException {
        int currentRecords = resourceRecordRepository.countResourceRecordMByResourceIdAndInfoRDIsNull(resourceRecord.getResourceId());
        int maxValue = Parameters.MAX_RECORDS.getValue();
        if (maxValue < currentRecords + 1) {
            throw new ApplicationException(ErrorCode.RECORDS_LIMIT_EXCEEDED, maxValue);
        }
        return resourceRecordRepository.save(resourceRecord);
    }

    @Override
    public List<ResourceM> getAllResources(Long userId, ListSearchCriteria searchCriteria) {
        Pageable pageable = PageRequest.of(searchCriteria.getPage(), searchCriteria.getMaxResult());
        return ResultList.wrap(resourceRepository.findAllByOwnerId(userId, pageable));
    }

    private void parseRecordValue(List<ResourceRecordM> records) throws ApplicationException {
        if (records.isEmpty()) {
            return;
        }
        ResourceM resource = records.get(0).getResource();
        String structure = resource.getStructure();
        StructureParser parser = new StructureParser(structure);
        parser.pars(records);
    }

    @Autowired
    public void setResourceRepository(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    @Autowired
    public void setResourceRecordRepository(ResourceRecordRepository resourceRecordRepository) {
        this.resourceRecordRepository = resourceRecordRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setResourceShareRepository(ResourceShareRepository resourceShareRepository) {
        this.resourceShareRepository = resourceShareRepository;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
