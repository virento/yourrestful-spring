package kacper.barszczewski.services;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.VerificationM;
import kacper.barszczewski.models.criteriars.UserListCriteria;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.pojo.UserForm;
import kacper.barszczewski.pojo.authentication.RegisterForm;
import kacper.barszczewski.resources.UserRepository;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import kacper.barszczewski.services.iterfaces.MailService;
import kacper.barszczewski.services.iterfaces.UserAuthenticationService;
import kacper.barszczewski.services.iterfaces.VerificationService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Base64;
import java.util.List;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
@Slf4j
public class UserAuthenticationImpl implements UserAuthenticationService {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private AuthenticationService authenticationService;

    private MailService mailService;

    private VerificationService verificationService;

    public UserM loadUserByUsername(String login) throws UsernameNotFoundException {
        return userRepository.getUserMByEmailOrUsername(login, login);
    }

    @Override
    @Transactional
    public void saveUser(UserM user) {
        userRepository.save(user);
    }

    @Override
    public AuthToken authenticateByCredential(String username, String rawPassword) throws ApplicationException {
        UserM userDetails = loadUserByUsername(username);
        if (userDetails == null) {
            throw new ApplicationException(ErrorCode.USER_NOT_FOUND, "nick=" + username);
        }
        if (!UserM.UserStatus.ACTIVE.equals(userDetails.getStatus())) {
            throw new ApplicationException(ErrorCode.USER_NOT_ACTIVE);
        }

        if (!passwordEncoder.matches(rawPassword, userDetails.getPassword())) {
            throw new ApplicationException(ErrorCode.BAD_CREDENTIALS);
        }
        AuthToken authToken = authenticationService.generateUserToken(userDetails);
        log.debug("User authenticated: {}", username);
        return authToken;
    }

    @Override
    public AuthToken authenticateByCredential(List<String> authorizations) throws ApplicationException {
        for (String authorization : authorizations) {
            if (authorization == null) {
                continue;
            }
            if (authorization.startsWith("Basic ")) {
                authorization = decodeCredential(authorization);
            }
            String[] args = authorization.split(":");
            if (args.length != 2) {
                throw new ApplicationException(ErrorCode.BAD_CREDENTIALS);
            }
            return authenticateByCredential(args[0], args[1]);
        }
        throw new ApplicationException(ErrorCode.BAD_CREDENTIALS);
    }

    @Override
    public AuthToken authenticateByRefreshToken(String refreshToken) throws ApplicationException {
        if (refreshToken == null) {
            throw new ApplicationException(ErrorCode.BAD_CREDENTIALS);
        }
        return authenticationService.generateUserToken(refreshToken);
    }

    @Override
    @Transactional
    public void verifyUser(String token, String username) throws ApplicationException {
        UserM user = userRepository.getUserMByEmailOrUsername(null, username);
        if (user == null) {
            throw new ApplicationException(ErrorCode.USER_NOT_FOUND, "nick=" + username);
        }
        if (!UserM.UserStatus.NEW.equals(user.getStatus())) {
            throw new ApplicationException(ErrorCode.NO_VERIFICATION, token);
        }
        verificationService.verifyToken(user, token, VerificationM.Type.EMAIL);
        user.setStatus(UserM.UserStatus.ACTIVE);
    }

    @Override
    public void resetUserPassword(String email) throws ApplicationException {
        UserM user = userRepository.getUserMByEmailOrUsername(email, null);
        if (user == null) {
            throw new ApplicationException(ErrorCode.USER_NOT_FOUND, "email=" + email);
        }
        if (!UserM.UserStatus.ACTIVE.equals(user.getStatus())) {
            throw new ApplicationException(ErrorCode.USER_WRONG_STATUS);
        }

        mailService.sendEmail(user, VerificationM.Type.PASSWORD);
    }

    @Override
    @Transactional
    public void newPassword(String token, String username, String password) throws ApplicationException {
        UserM user = userRepository.getUserMByEmailOrUsername(null, username);
        if (user == null) {
            throw new ApplicationException(ErrorCode.USER_NOT_FOUND, "nick=" + username);
        }
        if (!UserM.UserStatus.ACTIVE.equals(user.getStatus())) {
            throw new ApplicationException(ErrorCode.USER_WRONG_STATUS);
        }
        verificationService.verifyToken(user, token, VerificationM.Type.PASSWORD);
        user.setPassword(passwordEncoder.encode(password));
    }

    @Override
    public List<UserM> findUsers(UserListCriteria criteria) {
        Specification<UserM> spec = criteria.buildSpecification();
        return ResultList.wrap(userRepository.findAll(where(spec), criteria.getPageable()));
    }

    @Override
    @Transactional
    public void modifyUser(String username, UserForm userForm) {
        if (!userRepository.existsByUsername(username)) {
            throw new ApplicationException(ErrorCode.USER_NOT_FOUND);
        }
        UserM user = userRepository.getUserMByEmailOrUsername(null, username);
        userForm.modifyModel(user);
        userRepository.saveAndFlush(user);
        log.info("User with nick " + username + " has been modified");
    }

    private String decodeCredential(String encrypted) {
        String decrypted = null;
        if (encrypted == null) {
            return null;
        }
        if (encrypted.startsWith("Basic ")) {
            encrypted = encrypted.substring("Basic ".length());
            decrypted = new String(Base64.getDecoder().decode(encrypted));
        }
        return decrypted;
    }

    @Override
    public UserM getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserM) {
            return (UserM) principal;
        }
        return null;
    }

    @Override
    @Transactional
    public boolean saveUserByForm(RegisterForm registerForm) throws ApplicationException {
        if (userRepository.existsByEmail(registerForm.getEmail())) {
            throw new ApplicationException(ErrorCode.REGISTER_EMAIL_USED);
        }
        if (userRepository.existsByUsername(registerForm.getUsername())) {
            throw new ApplicationException(ErrorCode.REGISTER_USERNAME_USED, registerForm.getUsername());
        }
        UserM user = new UserM();
        user.setEmail(registerForm.getEmail());
        user.setFirstName(registerForm.getName());
        user.setLastName(registerForm.getSurname());
        user.setPassword(passwordEncoder.encode(registerForm.getPassword()));
        user.setUsername(registerForm.getUsername());
        user.setType(UserM.UserType.USER);
        user.setStatus(UserM.UserStatus.NEW);
        saveUser(user);
        mailService.sendEmail(user, VerificationM.Type.EMAIL);

        return true;
    }

    @Override
    public boolean existsUser(@Nullable String email, @Nullable String nick) {
        return userRepository.existsByEmailOrUsername(email, nick);
    }

    /**
     * Check whether user with id #userId is the same user as user with nick #nick or email #email
     * If user with #nick or #email not exists => return false
     */
    @Override
    public boolean isSameUser(Long userId, String nick, String email) {
        UserM user = userRepository.getUserMByEmailOrUsername(email, nick);
        if (user == null) {
            return false;
        }
        return user.getId().equals(userId);
    }

    @Autowired
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Autowired
    public void setVerificationService(VerificationService verificationService) {
        this.verificationService = verificationService;
    }
}
