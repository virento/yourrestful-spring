package kacper.barszczewski.services;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.VerificationM;
import kacper.barszczewski.resources.VerificationRepository;
import kacper.barszczewski.services.iterfaces.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
public class VerificationServiceImpl implements VerificationService {

    private VerificationRepository verificationRepository;

    @Override
    @Transactional
    public VerificationM generateToken(UserM user, VerificationM.Type type) {
        VerificationM verification = verificationRepository.getVerification(user.getId(), type);
        if (verification != null) {
            verification.setExpirationDate(null);
            verification.setValue(UUID.randomUUID().toString());
            return verificationRepository.save(verification);
        } else {
            String token = UUID.randomUUID().toString();
            VerificationM verfToken = new VerificationM();
            verfToken.setType(type);
            verfToken.setValue(token);
            verfToken.setUserId(user.getId());
            verfToken.setStatus(VerificationM.Status.NEW);
            return verificationRepository.save(verfToken);
        }
    }

    @Override
    @Transactional
    public void verifyToken(UserM user, String token, VerificationM.Type type) throws ApplicationException {
        VerificationM verification = verificationRepository.getVerification(user.getId(), type);
        if (verification == null) {
            throw new ApplicationException(ErrorCode.NO_VERIFICATION, token);
        }

        if (!verification.getValue().equals(token)) {
            throw new ApplicationException(ErrorCode.VERIFICATION_NOT_MATCH);
        }

        if (verification.getExpirationDate() != null && verification.getExpirationDate().before(new Date())) {
            throw new ApplicationException(ErrorCode.VERIFICATION_RESENT);
        }

        verification.setStatus(VerificationM.Status.USED);
        verificationRepository.save(verification);
    }

    @Autowired
    public void setVerificationRepository(VerificationRepository verificationRepository) {
        this.verificationRepository = verificationRepository;
    }
}
