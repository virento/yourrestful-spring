package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.pojo.resource.request.WSResourceGeneration;

import java.util.List;
import java.util.Map;

public interface ResourceService {

    ResourceM getResource(Long id);

    ResourceRecordM createResourceRecord(Long resourceId, Map<String, Object> body, boolean simulate) throws ApplicationException;

    ResourceRecordM modifyResourceRecord(Long resourceId, Map<String, Object> body, boolean simulate) throws ApplicationException;

    ResourceRecordM modifyResourceRecord(ResourceRecordM resourceRecord);

    ResourceRecordM createResourceRecord(ResourceRecordM resourceRecord) throws ApplicationException;

    List<ResourceM> getAllResources(Long userId, ListSearchCriteria searchCriteria);

    ResourceRecordM getResourceRecord(Long resourceId, Long recordId) throws ApplicationException;

    List<ResourceRecordM> getResourceRecords(Long resourceId, ListSearchCriteria searchCriteria) throws ApplicationException;

    ResourceM getUniversalResource();

    ResourceM createResource(ResourceM resource) throws ApplicationException;

    ResourceM getResource(String resourceUrl) throws ApplicationException;

    ResourceM createResourceWithRecords(ResourceM resource, List<Map<String, Object>> records, WSResourceGeneration generation) throws ApplicationException;

    void crateResourceRecords(ResourceM resource, List<Map<String, Object>> records, WSResourceGeneration generation) throws ApplicationException;

    ResourceM modifyResource(ResourceM resource, List<Map<String, Object>> records) throws ApplicationException;

    ResourceM modifyResource(ResourceM resource) throws ApplicationException;

    void shareResourceToUser(String resourceUrl, String nick, String email) throws ApplicationException;

    boolean existsResourceRecord(Long resourceId, Long recordId);

    List<UserM> getResourceShares(String resourceUrl, ListSearchCriteria searchCriteria) throws ApplicationException;

    void removeShareUserFromResource(String resourceUrl, String email, String nick) throws ApplicationException;

    void removeResourceRecord(String resourceUrl, Long id) throws ApplicationException;

    void removeResource(String resourceUrl) throws ApplicationException;

    int generateRecords(String resourceUrl, WSResourceGeneration recordObject) throws ApplicationException;
}
