package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.controllers.exceptions.ApplicationException;

public interface ParameterService {

    void modifyParameter(String code, String newValue) throws ApplicationException;

}
