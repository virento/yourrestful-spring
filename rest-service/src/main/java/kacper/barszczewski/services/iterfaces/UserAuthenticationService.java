package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.criteriars.ListSearchCriteria;
import kacper.barszczewski.models.criteriars.UserListCriteria;
import kacper.barszczewski.pojo.UserForm;
import kacper.barszczewski.pojo.authentication.LoginForm;
import kacper.barszczewski.pojo.authentication.RegisterForm;
import kacper.barszczewski.services.AuthToken;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserAuthenticationService {

    void saveUser(UserM user);

    AuthToken authenticateByCredential(String username, String codedPassword) throws ApplicationException;

    UserM getLoggedUser();

    boolean saveUserByForm(RegisterForm registerForm) throws ApplicationException;

    boolean existsUser(@Nullable String email, @Nullable String nick);

    boolean isSameUser(Long userId, String nick, String email);

    AuthToken authenticateByCredential(List<String> authorizations) throws ApplicationException;

    AuthToken authenticateByRefreshToken(String refreshToken) throws ApplicationException;

    void verifyUser(String token, String username) throws ApplicationException;

    void resetUserPassword(String email) throws ApplicationException;

    void newPassword(String token, String username, String password);

    List<UserM> findUsers(UserListCriteria criteria);

    void modifyUser(String username, UserForm userForm);
}
