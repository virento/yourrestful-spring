package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.services.AuthToken;

public interface AuthenticationService {

    AuthToken generateUserToken(UserM user);

    AuthToken generateUserToken(String refreshToken) throws ApplicationException;

    AuthToken isAuthenticated(String token) throws ApplicationException;
}
