package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.ResourceM;

import java.util.Objects;
import java.util.logging.Logger;

public interface MyCache {
    enum Domain {

        RESOURCE(ResourceM.class),
        PARAMETER(ParameterM.class),
        UNIVERSAL_RESOURCE;

        private Class<?> clazz;

        Domain() {
            clazz = null;
        }

        Domain(Class<?> clazz) {
            this.clazz = clazz;
        }

        public static Domain getDomain(Class<?> clazz) {
            for (Domain value : Domain.values()) {
                if (Objects.equals(value.clazz, clazz)) {
                    return value;
                }
            }
            Logger.getLogger(Domain.class.getName()).warning("Can't find domain for class: " + clazz.getName());
            return null;
        }
    }

}
