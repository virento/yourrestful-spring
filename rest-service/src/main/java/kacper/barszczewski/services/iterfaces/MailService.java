package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.VerificationM;

public interface MailService  {

    void sendEmail(UserM user, VerificationM.Type type);

}
