package kacper.barszczewski.services.iterfaces;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.VerificationM;

public interface VerificationService {

    VerificationM generateToken(UserM user, VerificationM.Type type);

    void verifyToken(UserM user, String token, VerificationM.Type type) throws ApplicationException;

}
