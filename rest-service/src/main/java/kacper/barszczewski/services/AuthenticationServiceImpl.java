package kacper.barszczewski.services;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.services.iterfaces.AuthenticationService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class AuthenticationServiceImpl implements AuthenticationService {

    private final int KEY_LENGTH = 1024;

    public final int EXPIRATION_TIME = 30 * 60 * 1000;

    public final int EXPIRATION_REFRESH_TOKEN = 45 * 60 * 1000;

    private final Map<String, AuthToken> authMap = new HashMap<>();

    private final Map<String, String> refreshMap = new HashMap<>();

    private final Map<String, String> tokenMap = new HashMap<>();

    private final SecureRandom secureRandom = new SecureRandom();

    private final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

    @Override
    public AuthToken generateUserToken(UserM user) {
        List<String> authorities = user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        //Remove existing user tokens
        if (authMap.containsKey(user.getUsername())) {
            AuthToken authToken = authMap.get(user.getUsername());
            refreshMap.remove(authToken.getRefreshToken());
            tokenMap.remove(authToken.getToken());
        }

        String token = generateTokenString();
        String refreshToken = generateTokenString();
        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
        Date expirationRefreshTokenDate = new Date(System.currentTimeMillis() + EXPIRATION_REFRESH_TOKEN);
        AuthToken authToken = new AuthToken(user, authorities, expirationDate, expirationRefreshTokenDate, token, refreshToken);
        authMap.put(user.getUsername(), authToken);
        refreshMap.put(refreshToken, user.getUsername());
        tokenMap.put(token, user.getUsername());
        return authToken;
    }

    @Override
    public AuthToken generateUserToken(String refreshToken) throws ApplicationException {
        if (!refreshMap.containsKey(refreshToken)) {
            throw new ApplicationException(ErrorCode.BAD_CREDENTIALS);
        }
        String username = popRefreshToken(refreshToken);
        if (!authMap.containsKey(username)) {
            throw new ApplicationException(ErrorCode.BAD_CREDENTIALS);
        }
        AuthToken authToken = authMap.get(username);
        if (authToken.getExpirationRefreshTokenDate().before(new Date())) {
            throw new ApplicationException(ErrorCode.REFRESH_TOKEN_EXPIRED);
        }
        UserM user = authToken.getUser();
        return generateUserToken(user);
    }

    private String popRefreshToken(String refreshToken) {
        String username = refreshMap.get(refreshToken);
        refreshMap.remove(refreshToken);
        return username;
    }

    @Override
    public AuthToken isAuthenticated(String token) throws ApplicationException {
        String username = tokenMap.get(token);
        if (username == null) {
            return null;
        }
        AuthToken authToken = authMap.get(username);
        if (authToken == null) {
            return null;
        }
        if (authToken.getExpirationDate().before(new Date())) {
            throw new ApplicationException(ErrorCode.TOKEN_EXPIRED);
        }
        return authToken;
    }

    private String generateTokenString() {
        byte[] bytes = new byte[KEY_LENGTH];
        secureRandom.nextBytes(bytes);
        return base64Encoder.encodeToString(bytes);
    }
}
