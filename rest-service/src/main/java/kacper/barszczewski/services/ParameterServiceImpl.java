package kacper.barszczewski.services;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.resources.ParameterRepository;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.services.iterfaces.ParameterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class ParameterServiceImpl implements ParameterService {

    private ParameterRepository parameterRepository;

    private MyCache myCache;

    @Override
    @Transactional
    public void modifyParameter(String code, String newValue) throws ApplicationException {
        Parameters parameters = Parameters.get(code);
        if (parameters == null) {
            throw new ApplicationException(ErrorCode.WRONG_PARAMETER, code);
        }
        if (!parameters.validateValue(newValue)) {
            throw new ApplicationException(ErrorCode.WRONG_PARAMETER_TYPE, code, parameters.getType());
        }
        ParameterM parameterMByCode = parameterRepository.getParameterMByCode(code);
        parameterMByCode.setValue(newValue);

        parameterRepository.saveAndFlush(parameterMByCode);


        log.info("Parameter {} has changed value to {}", code, newValue);
    }

    @Autowired
    public void setParameterRepository(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    @Autowired
    public void setMyCache(MyCache myCache) {
        this.myCache = myCache;
    }
}
