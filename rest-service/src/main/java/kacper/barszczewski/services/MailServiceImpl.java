package kacper.barszczewski.services;

import kacper.barszczewski.config.CustomProperties;
import kacper.barszczewski.controllers.exceptions.ProgramException;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.models.VerificationM;
import kacper.barszczewski.services.iterfaces.MailService;
import kacper.barszczewski.services.iterfaces.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@Service
public class MailServiceImpl implements MailService {

    private CustomProperties customProperties;

    private Session session;

    private VerificationService verificationService;

    private void initMailSession(CustomProperties customProperties) {
        final CustomProperties.MailOptions mailOptions = customProperties.getMailOptions();
        if (!mailOptions.getEnable()) {
            return;
        }

        Properties props = System.getProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.debug", "true");
        props.put("mail.smtp.host", Parameters.MAIL_HOST.getValue());
        props.put("mail.smtp.port", Parameters.MAIL_HOST_PORT.getValue());
        props.put("mail.smtp.socketFactory.port", Parameters.MAIL_SOCKET_FACTORY_PORT.getValue());
        props.put("mail.smtp.socketFactory.class", Parameters.MAIL_SOCKET_FACTORY_CLASS.getValue());

        this.session = Session.getInstance(props,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                Parameters.MAIL_EMAIL.getValue(),
                                Parameters.MAIL_PASSWORD.getValue()
                        );
                    }
                });
    }

    private void sendEmail(String email, String body, String title) {
        if (!customProperties.getMailOptions().getEnable()) {
            return;
        }
        if (session == null) {
            initMailSession(customProperties);
        }

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Parameters.MAIL_EMAIL.getValue(), Parameters.MESSAGE_FROM_NAME.getValue()));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(title);
            message.setContent(body, "text/html; charset=utf-8");
            Transport.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new ProgramException(e.getMessage());
        }
    }

    @Override
    public void sendEmail(UserM user, VerificationM.Type type) {
        if (!customProperties.getMailOptions().getEnable()) {
            return;
        }

        String link;
        Parameters messageParameter, titleParameter;
        switch (type) {
            case EMAIL:
                messageParameter = Parameters.VERIFICATION_MESSAGE;
                titleParameter = Parameters.VERIFICATION_TITLE;
                link = customProperties.getVerificationTokenLink();
                break;
            case PASSWORD:
                messageParameter = Parameters.PASSWORD_VERIFICATION_MESSAGE;
                titleParameter = Parameters.PASSWORD_VERIFICATION_TITLE;
                link = customProperties.getPasswordTokenLink();
                break;
            default:
                throw new RuntimeException("Email type not found: " + type.name());
        }
        VerificationM verification = verificationService.generateToken(user, type);
        String verificationLink = link + "?token=" + verification.getValue() + "&username=" + user.getUsername();

        String bodyText = messageParameter.getValue() + verificationLink;
        String title = titleParameter.getValue();
        sendEmail(user.getEmail(), bodyText, title);
    }

    @Autowired
    public void setVerificationService(VerificationService verificationService) {
        this.verificationService = verificationService;
    }

    @Autowired
    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}
