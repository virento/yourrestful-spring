package kacper.barszczewski.services;

import kacper.barszczewski.models.CodeModel;
import kacper.barszczewski.models.IdModel;
import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.services.iterfaces.MyCache;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
@Slf4j
public class MyCacheImpl implements MyCache {

    private static final Map<Domain, CacheRegion<?>> values = new HashMap<>();

    private static Long universalResourceId;

    static {
        values.put(Domain.RESOURCE, new CodeCacheRegion<ResourceM>(Parameters.CACHE_SIZE.name()));
        values.put(Domain.PARAMETER, new CodeCacheRegion<ParameterM>());
        values.put(Domain.UNIVERSAL_RESOURCE, new CodeCacheRegion<ResourceM>());
    }

    public static Optional<ResourceM> getResource(String resourceUrl) {
        CacheRegion<?> cacheRegion = values.get(Domain.RESOURCE);
        if (cacheRegion instanceof CodeCacheRegion) {
            return ((CodeCacheRegion<ResourceM>) cacheRegion).getByCode(resourceUrl);
        }
        return Optional.empty();
    }

    public static <T> Optional<T> get(Domain domain, Long id) {
        return (Optional<T>) getRegion(domain).get(id);
    }

    public static Long getUniversalResourceId() {
        return universalResourceId;
    }

    public static ResourceM getUniversalResource() {
        return (ResourceM) values.get(Domain.UNIVERSAL_RESOURCE).get(universalResourceId).orElse(null);
    }

    public static void addResource(ResourceM resource) {
        if (resource == null) {
            return;
        }
        if (ResourceM.ResourceType.UNIVERSAL.equals(resource.getType())) {
            universalResourceId = resource.getId();
            getRegion(Domain.UNIVERSAL_RESOURCE).add(resource);
            return;
        }
        getRegion(Domain.RESOURCE).add(resource);
    }

    public static <T extends IdModel> CacheRegion<T> getRegion(Domain domain) {
        return (CacheRegion<T>) values.get(domain);
    }

    public static void add(Domain domain, IdModel object) {
        if (object == null) {
            return;
        }
        if (object instanceof ResourceM) {
            addResource((ResourceM) object);
            return;
        }
        getRegion(domain).add(object);
    }

    public static <T> Optional<T> getDomainValue(Domain domain, String key) {
        if (getRegion(domain) instanceof CodeCacheRegion) {
            return ((CodeCacheRegion) getRegion(domain)).getByCode(key);
        }
        return Optional.empty();
    }

    public static <T extends IdModel> List<T> getAll(Domain domain) {
        return (List) getRegion(domain).getAll();
    }

    public static <T> Optional<T> getCacheObject(Domain domain, Object argument) {
        CacheRegion<IdModel> region = getRegion(domain);
        if (argument instanceof String && region instanceof CodeCacheRegion) {
            return ((CodeCacheRegion) region).getByCode((String) argument);
        }
        if (argument instanceof Long) {
            return (Optional<T>) region.get((Long) argument);
        }
        return Optional.empty();
    }

    @NoArgsConstructor
    private static class CacheRegion<T extends IdModel> {

        protected final Map<Long, T> map = new HashMap<>();

        protected final Map<Long, Long> time = new HashMap<>();

        protected static final Object LOCKER = new Object();

        private String limitParamCode;

        public CacheRegion(String limitParamCode) {
            this.limitParamCode = limitParamCode;
        }

        public List<T> getAll() {
            return new ArrayList<>(map.values());
        }

        public Optional<T> get(Long id) {
            markUsed(id);
            return Optional.ofNullable(map.get(id));
        }

        public void add(T object) {
            synchronized (LOCKER) {
                if (isSizeExceeded()) {
                    removeLatest();
                }
                map.put(object.getId(), object);
                markUsed(object.getId());
            }
        }

        protected void removeLatest() {
            Optional<Map.Entry<Long, Long>> min = time.entrySet().stream().min(Map.Entry.comparingByValue());
            if (min.isPresent()) {
                time.remove(min.get().getKey());
                Long id = min.get().getKey();
                if (!map.containsKey(id)) {
                    return;
                }
                map.remove(id);
            }
        }

        protected boolean isSizeExceeded() {
            if (limitParamCode == null) {
                return false;
            }
            int size = Parameters.get(limitParamCode).getValue();
            return map.size() > size;
        }

        protected void markUsed(Long id) {
            time.put(id, System.currentTimeMillis());
        }
    }

    @NoArgsConstructor
    private static class CodeCacheRegion<T extends CodeModel> extends CacheRegion<T> {

        private final Map<String, T> codeMap = new HashMap<>();

        public CodeCacheRegion(String limitParamCode) {
            super(limitParamCode);
        }

        public Optional<T> getByCode(String code) {
            Optional<T> objectOp = Optional.ofNullable(codeMap.get(code));
            objectOp.ifPresent(t -> markUsed(t.getId()));
            return objectOp;
        }

        @Override
        public void add(T object) {
            synchronized (LOCKER) {
                if (isSizeExceeded()) {
                    removeLatest();
                }
                map.put(object.getId(), object);
                codeMap.put(object.getCode(), object);
                markUsed(object.getId());
            }
        }

        @Override
        protected void removeLatest() {
            Optional<Map.Entry<Long, Long>> min = time.entrySet().stream().min(Map.Entry.comparingByValue());
            if (min.isPresent()) {
                time.remove(min.get().getKey());
                Long id = min.get().getKey();
                log.debug("Clearing object from cache with id: " + id);
                if (!map.containsKey(id)) {
                    return;
                }
                T removed = map.remove(id);
                codeMap.remove(removed.getCode());
            }
        }
    }
}
