package kacper.barszczewski.adnotation;

import kacper.barszczewski.aspect.AccessChecker;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface HaveAccess {

    interface Level {
        String PRIVATE = "P";
        String SHARE = "S";
        String ALL = "A";
    }

    String level() default Level.ALL;

    int[] params() default {};

    Class<? extends AccessChecker> checker();

}
