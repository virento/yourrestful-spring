package kacper.barszczewski.adnotation;

import kacper.barszczewski.controllers.exceptions.ErrorCode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Method annotated with this annotation will cause to throw an {@link kacper.barszczewski.controllers.exceptions.ApplicationException}
 * when returned null as result. {@link #errorCode} determinate which message will be set as exception message.
 * <p>
 * It is possible to format message with parameters value
 * If in the message exists charset {}, then they will be replaced with primitive parameters values
 * in order which parameters occurs.
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExceptionOnResultNull {

    /**
     * Indicate what message will be set to exception
     */
    ErrorCode errorCode();
}
