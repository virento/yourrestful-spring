package kacper.barszczewski.adnotation.validators;

import kacper.barszczewski.validators.MultiFieldNotNullValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * At least one of the field provided in {@link #fields()} must be not null
 */
@Constraint(validatedBy = MultiFieldNotNullValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MultiFieldNotNull {

    String message() default "at least one of the fields need to be filled";

    String[] fields();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default { };
}
