package kacper.barszczewski.utils;

import kacper.barszczewski.utils.factories.AbstractFactory;

import java.util.HashMap;
import java.util.Map;

public class EntityFactory {

    public static Map<String, Object> createRecordObjectMap() {
        return new HashMap<>();
    }

    public static Map<String, AbstractFactory> createStructureMap() {
        return new HashMap<>();
    }

    public static Map<String, String> createStructureValueMap() {
        return new HashMap<>();
    }
}
