package kacper.barszczewski.utils;

import com.udojava.evalex.AbstractFunction;
import com.udojava.evalex.Expression;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class ExpressionUtils {

    public static final Pattern MATH_PATTER = Pattern.compile("^([0-9.n\\s()+\\-/*^%]|(?i)rand\\([0-9]*\\))*$");

    public static final Pattern BOOLEAN_PATTER = Pattern.compile("^(([0-9.n\\s()+\\-/*^%=<>!&|]|(?i)rand\\([0-9]*\\))*|(?i)true|(?i)false)$");

    private static final BigDecimal DEFAULT_RAND_MAX = BigDecimal.valueOf(100);

    public static Expression getExpression(String expressionValue) {
        Expression expression = new Expression(expressionValue);
        expression.addFunction(new AbstractFunction("rand", -1) {
            @Override
            public BigDecimal eval(List<BigDecimal> parameters) {
                if (parameters.size() == 0) {
                    return DEFAULT_RAND_MAX;
                }
                return BigDecimal.valueOf(new Random().nextInt(parameters.get(0).intValue()));
            }
        });
        return expression;
    }

    public static BigDecimal prepareDecimal(BigDecimal decimal) {
        return decimal.setScale(2, RoundingMode.HALF_DOWN);
    }

}
