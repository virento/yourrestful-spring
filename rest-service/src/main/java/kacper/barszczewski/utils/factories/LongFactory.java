package kacper.barszczewski.utils.factories;

import com.udojava.evalex.Expression;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.utils.ExpressionUtils;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public class LongFactory extends AbstractFactory {

    private static final LongFactory instance = new LongFactory();

    public static LongFactory getInstance() {
        return instance;
    }

    @Override
    public Object createObject(String value) {
        return Long.parseLong(value);
    }

    @Override
    public ErrorCode validateValue(Object object) {
        if (object instanceof Number) {
            return null;
        }
        return ErrorCode.LONG_VALUE;
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public Long generate(int i, String expressionValue) throws ApplicationException {
        if (!ExpressionUtils.MATH_PATTER.matcher(expressionValue).find()) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, expressionValue);
        }

        Expression expression = ExpressionUtils.getExpression(expressionValue);
        expression.setVariable("n", BigDecimal.valueOf(i));

        try {
            BigDecimal result = expression.eval();
            return result.longValueExact();
        } catch (ArithmeticException e) {
            throw new ApplicationException(ErrorCode.EXPRESSION_EVALUATION, expressionValue);
        } catch (Expression.ExpressionException e) {
            log.warn("Expression '" + expressionValue + "' evaluation failed");
            e.printStackTrace();
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, expressionValue);
        }
    }
}
