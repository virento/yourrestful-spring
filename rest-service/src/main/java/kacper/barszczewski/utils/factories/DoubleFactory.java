package kacper.barszczewski.utils.factories;

import com.udojava.evalex.Expression;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.utils.ExpressionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;

@Slf4j
public class DoubleFactory extends AbstractFactory {

    private static final DoubleFactory instance = new DoubleFactory();

    public static DoubleFactory getInstance() {
        return instance;
    }

    @Override
    public Object createObject(String value) {
        return Double.parseDouble(value);
    }

    @Override
    public ErrorCode validateValue(Object object) {
        if (object instanceof Double) {
            return null;
        }
        if (NumberUtils.isParsable(String.valueOf(object))) {
            return null;
        }
        return ErrorCode.DOUBLE_VALUE;
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public Object generate(int i, String expressionValue) throws ApplicationException {
        if (!ExpressionUtils.MATH_PATTER.matcher(expressionValue).find()) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, expressionValue);
        }

        Expression expression = ExpressionUtils.getExpression(expressionValue);
        expression.setVariable("n", BigDecimal.valueOf(i));

        try {
            return ExpressionUtils.prepareDecimal(expression.eval()).doubleValue();
        } catch (ArithmeticException e) {
            throw new ApplicationException(ErrorCode.EXPRESSION_EVALUATION, expressionValue);
        } catch (Expression.ExpressionException e) {
            log.warn("Expression '" + expressionValue + "' evaluation failed");
            e.printStackTrace();
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, expressionValue);
        }
    }
}
