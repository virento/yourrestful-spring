package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.controllers.exceptions.ProgramException;

import java.util.HashMap;
import java.util.Map;

public class ObjectFactory extends HashMap<String, AbstractFactory> implements Map<String, AbstractFactory> {

    public enum Type {
        TEXT, BOOLEAN, LONG, DOUBLE, DATE
    }

    private static final ObjectFactory factory = new ObjectFactory();

    public static ObjectFactory getInstance() {
        return factory;
    }

    public static void register(String type, AbstractFactory factory) {
        getInstance().put(type.toUpperCase(), factory);
    }

    public static AbstractFactory getFactory(String type) throws ApplicationException {
        type = type.toUpperCase();
        if (!getInstance().containsKey(type)) {
            throw new ApplicationException(ErrorCode.WRONG_FIELD_TYPE, type);
        }
        return getInstance().get(type);
    }

}
