package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;

public abstract class AbstractFactory {

    public abstract Object createObject(String value);

    public abstract ErrorCode validateValue(Object object) throws ApplicationException;

    public abstract Object generate(int i, String generator) throws ApplicationException;
}
