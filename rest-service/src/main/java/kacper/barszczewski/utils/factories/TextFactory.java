package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.utils.TextParser;

import java.util.List;
import java.util.Random;

public class TextFactory extends AbstractFactory {

    private static final TextFactory instance = new TextFactory();

    public static TextFactory getInstance() {
        return instance;
    }

    @Override
    public Object createObject(String value) {
        value = value.trim();
        if (value.startsWith("'") && value.endsWith("'")) {
            value = value.substring(1, value.length() - 1);
        }
        return value;
    }

    @Override
    public ErrorCode validateValue(Object object) {
        if (object instanceof String) {
            return null;
        }
        return ErrorCode.STRING_VALUE;
    }

    @Override
    public Object generate(int i, String generator) throws ApplicationException {
        generator = putRandomText(i, generator);

        return generator.replaceAll("\\{n}", String.valueOf(i));
    }

    private String putRandomText(int i, String generator) {
        List<String> randomValues = Parameters.RANDOM_TEXT.getValue();
        Random rand = new Random();

        TextParser parser = new TextParser(generator);
        int beginIndex;
        while ((beginIndex = parser.nextSequence("{")) != -1) {
            String word = parser.getNextWord();
            String newText = "";
            int endIndex = -1;
            switch (word) {
                case "n":
                    endIndex = parser.nextSequence("}");
                    newText = String.valueOf(i);
                    break;
                case "random":
                    newText = randomValues.get(rand.nextInt(randomValues.size()));
                    if (parser.getNextChar() == '%') {
                        int numbIndex = parser.nextSequence("%");
                        endIndex = parser.nextSequence("}");
                        int number = Integer.parseInt(generator.substring(numbIndex + 1, endIndex).replaceAll("\\s+", ""));
                        if (number < 0 || number > 100) {
                            throw new ApplicationException(ErrorCode.GENERATOR_MAX_TEXT, number, 100);
                        }
                        newText = prepareText(newText, number);
                    }
            }
            if (endIndex == -1) {
                endIndex = parser.nextSequence("}");
            }
            generator = generator.substring(0, beginIndex) + "{" + word + "}" + generator.substring(endIndex + 1);
            generator = generator.replaceFirst("\\{" + word + "}", newText);
            parser = new TextParser(generator);
        }
        return generator;
    }

    private String prepareText(String randomText, int length) {
        StringBuilder randomTextBuilder = new StringBuilder(randomText);
        while (randomTextBuilder.length() < length) {
            randomTextBuilder.append(" ").append(randomTextBuilder);
        }
        randomText = randomTextBuilder.toString();
        if (randomText.length() > length) {
            return randomText.substring(0, length);
        }
        return randomText;
    }

}
