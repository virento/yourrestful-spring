package kacper.barszczewski.utils.factories;

import com.udojava.evalex.Expression;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.utils.ExpressionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;

@Slf4j
public class BooleanFactory extends AbstractFactory {

    private static final BooleanFactory instance = new BooleanFactory();

    public static AbstractFactory getInstance() {
        return instance;
    }

    @Override
    public Boolean createObject(String value) {
        if (value.equals("F") || value.equals("0")) {
            return Boolean.FALSE;
        }
        if (value.equals("T") || NumberUtils.isDigits(value)) {
            return Boolean.TRUE;
        }
        return Boolean.valueOf(value);
    }

    @Override
    public ErrorCode validateValue(Object object) {
        if (object instanceof Boolean) {
            return null;
        }
        if (!(object instanceof String)) {
            return ErrorCode.BOOLEAN_VALUE;
        }
        String value = (String) object;
        if (value.equals("F") || value.equals("0") || value.equals("T") || NumberUtils.isDigits(value) ||
                "true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value)) {
            return null;
        }
        return ErrorCode.BOOLEAN_VALUE;
    }

    @Override
    public Boolean generate(int i, String expressionValue) throws ApplicationException {
        if (!ExpressionUtils.BOOLEAN_PATTER.matcher(expressionValue).find()) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, expressionValue);
        }
        if (isStaticBooleanValue(expressionValue)) {
            return createObject(expressionValue);
        }

        Expression expression = ExpressionUtils.getExpression(expressionValue);
        expression.setVariable("n", BigDecimal.valueOf(i));

        try {
            BigDecimal result = expression.eval();
            return result.intValue() != 0;
        } catch (ArithmeticException e) {
            throw new ApplicationException(ErrorCode.EXPRESSION_EVALUATION, expressionValue);
        } catch (Expression.ExpressionException e) {
            log.warn("Expression '" + expressionValue + "' evaluation failed");
            e.printStackTrace();
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, expressionValue);
        }
    }

    private boolean isStaticBooleanValue(String expressionValue) {
        return (expressionValue.length() == 1 && (expressionValue.equalsIgnoreCase("T") || expressionValue.equalsIgnoreCase("F"))) ||
                expressionValue.equalsIgnoreCase("false") || expressionValue.equalsIgnoreCase("true");
    }
}
