package kacper.barszczewski.utils.factories;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import org.apache.commons.lang3.math.NumberUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFactory extends AbstractFactory {

    private final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    private static final DateFactory instance = new DateFactory();

    public static DateFactory getInstance() {
        return instance;
    }

    @Override
    public Object createObject(String value) {
        value = value.trim();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_FORMAT);
        try {
            Date date = simpleDateFormat.parse(value);
            simpleDateFormat.applyPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return value;
    }

    @Override
    public ErrorCode validateValue(Object object) {
        if (!(object instanceof String)) {
            return ErrorCode.DATE_VALUE;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            simpleDateFormat.parse((String) object);
        } catch (ParseException e) {
            return ErrorCode.DATE_VALUE;
        }
        return null;
    }

    @Override
    public String generate(int i, String generator) throws ApplicationException {
        String[] split = generator.split(";");
        if (split.length != 2) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, generator);
        }
        String operation = split[1].replaceAll("\\s+", "");
        if (operation.length() < 3) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, generator);
        }
        int mult = operation.charAt(0) == '+' ? 1 : -1;
        operation = operation.substring(1);
        String numberText = operation.substring(0, operation.length() - 1);
        String type = String.valueOf(operation.charAt(operation.length() - 1));

        if (!NumberUtils.isParsable(numberText)) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, generator);
        }
        int number = Integer.parseInt(numberText) * i;
        Calendar calendar = Calendar.getInstance();
        if (split[0].equalsIgnoreCase("now()")) {
            calendar.setTime(new Date());
        } else {
            try {
                calendar.setTime(new SimpleDateFormat(DATETIME_FORMAT).parse(split[0]));
            } catch (ParseException e) {
                throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, generator);
            }
        }
        int field = getField(type);
        if (field == -1) {
            throw new ApplicationException(ErrorCode.UNPARSABLE_GENERATOR, generator);
        }
        //noinspection MagicConstant
        calendar.add(field, mult * number);
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(calendar.getTime());
    }

    private int getField(String type) {
        switch (type) {
            case "d":
                return Calendar.DAY_OF_MONTH;
            case "M":
                return Calendar.MONTH;
            case "y":
                return Calendar.YEAR;
            case "H":
                return Calendar.HOUR_OF_DAY;
            case "s":
                return Calendar.SECOND;
            case "m":
                return Calendar.MINUTE;
        }
        return -1;
    }
}
