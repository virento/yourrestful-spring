package kacper.barszczewski.utils;

import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.Parameters;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.resources.ParameterRepository;
import kacper.barszczewski.resources.ResourceRepository;
import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.MyCache;
import kacper.barszczewski.utils.factories.BooleanFactory;
import kacper.barszczewski.utils.factories.DateFactory;
import kacper.barszczewski.utils.factories.DoubleFactory;
import kacper.barszczewski.utils.factories.LongFactory;
import kacper.barszczewski.utils.factories.ObjectFactory;
import kacper.barszczewski.utils.factories.TextFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class InitializationBean implements InitializingBean {

    private ResourceRepository resourceRepository;

    private ParameterRepository parameterRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.debug("Cache initializing");
        log.debug("Initializing factories");
        initFactory();
        log.debug("Initializing cache");
        initCache();
    }

    private void initCache() {
        Parameters[] values = Parameters.values();
        for (Parameters value : values) {
            ParameterM parameterValue = parameterRepository.getParameterMByCode(value.name());
            MyCacheImpl.add(MyCache.Domain.PARAMETER, parameterValue);
        }

        ResourceM universal = resourceRepository.getUniversal(ResourceM.ResourceType.UNIVERSAL);
        MyCacheImpl.addResource(universal);
    }

    private void initFactory() {
        ObjectFactory.register(ObjectFactory.Type.BOOLEAN.name(), BooleanFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DOUBLE.name(), DoubleFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.LONG.name(), LongFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.TEXT.name(), TextFactory.getInstance());
        ObjectFactory.register(ObjectFactory.Type.DATE.name(), DateFactory.getInstance());
    }

    @Autowired
    public void setResourceRepository(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    @Autowired
    public void setParameterRepository(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }
}
