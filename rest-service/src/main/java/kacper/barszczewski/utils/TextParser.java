package kacper.barszczewski.utils;

public class TextParser {

    private final char[] separators = new char[]{'{', '%', '}'};

    private final String text;
    private final String originalText;

    private int lastIndex = -1, lastOriginIndex = -1;

    private int lastEndIndex;

    public TextParser(String text) {
        this.originalText = text;
        this.text = text.replaceAll("\\s+", "");
    }

    public int getNextChar() {
        return text.charAt(lastEndIndex);
    }

    public String getNextWord() {
        int index = text.length();
        for (char separator : separators) {
            int tempIndex = text.indexOf(separator, lastIndex + 1);
            if (tempIndex != -1 && tempIndex < index) {
                index = tempIndex;
            }
        }
        String value = text.substring(lastIndex + 1, index);
        nextSequence(value);
        return value;
    }

    public int nextSequence(String sequence) {
        int i = text.indexOf(sequence, lastIndex + 1);
        if (i == -1) {
            return -1;
        }
        lastEndIndex = i + sequence.length();
        lastIndex = i;
        moveOriginIndex(sequence);
        return lastOriginIndex;
    }

    private void moveOriginIndex(String sequence) {
        lastOriginIndex = originalText.indexOf(sequence.charAt(0), lastOriginIndex + 1);
        while (!originalText.substring(lastOriginIndex).replaceAll("\\s+", "").startsWith(sequence)) {
            lastOriginIndex += originalText.substring(lastOriginIndex + 1).indexOf(sequence.charAt(0));
        }
    }

    public int getOriginTextIndex() {
        return lastOriginIndex;
    }

}
