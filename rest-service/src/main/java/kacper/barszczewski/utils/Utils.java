package kacper.barszczewski.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {

    public static FastDateFormat getDateFormat() {
        return DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT;
    }

    public static String convertToJSON(Map<String, Object> object) throws ApplicationException {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new ApplicationException(ErrorCode.WRONG_RECORD_STRUCTURE);
        }
    }


//    public static <T> T earialize(Object o) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
////        mapper.configure(DeserializationFeature.FAIL)
//        try {
//            return (T) mapper.readValue(mapper.writeValueAsString(o), o.getClass());
//        } catch (IOException e) {
//            e.printStackTrace();
//            return (T) o;
//        }
//    }

    public static boolean stringEqualIgnoreCaseRemoveSpace(String s1, String s2) {
        if (s1 == null) {
            return s2 == null;
        }
        return s1.replaceAll("\\s+", "").equalsIgnoreCase(s2.replaceAll("\\s+", ""));
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> getMapValue(Object object, String fullKey) {
        String[] split = fullKey.split("\\.");
        List<T> list = new ArrayList<>();
        if (object instanceof List) {
            for (Object obj : (List) object) {
                list.addAll(getMapValue(obj, fullKey));
            }
            return list;
        }
        Map map = (Map) object;
        for (int i = 0; i < split.length; i++) {
            Object item = map.get(split[i]);
            if (item instanceof Map) {
                map = (Map) item;
            } else if (item instanceof List) {
                for (Object t : (List) item) {
                    list.addAll(getMapValue(t, getKey(split, i)));
                }
                return list;
            } else {
                list.add((T) item);
            }
        }
        return list;
    }

    private static String getKey(String[] split, int i) {
        StringBuilder builder = new StringBuilder();
        for (int j = ++i; j < split.length; j++) {
            builder.append(split[j]).append(".");
        }
        return builder.substring(0, builder.length() - 1);
    }

    private static final int PLUS = 0;
    private static final int MINUS = 1;
    private static final int MULTIPLY = 2;
    private static final int DIVISION = 3;

    public static int getOperation(String operation) {
        switch (operation) {
            case "+":
                return PLUS;
            case "-":
                return MINUS;
            case "*":
                return MULTIPLY;
            case "/":
                return DIVISION;
        }
        return -1;
    }

    public static boolean putMapValue(Object originalMap, Object newMap, String key, Object value) {
        String[] split = key.split("\\.");
        if (originalMap instanceof List) {
            List map = (List) originalMap;
            for (int i = 0; i < map.size(); i++) {
                Object obj = map.get(i);
                Object newMapObj;
                if (((List) newMap).size() <= i) {
                    if (obj instanceof Map) {
                        newMapObj = new HashMap<>();
                    } else {
                        newMapObj = new ArrayList<>();
                    }
                    ((List) newMap).add(newMapObj);
                } else {
                    newMapObj = ((List) newMap).get(i);
                }
                if (!putMapValue(obj, newMapObj, key, value)) {
                    continue;
                }
                return true;
            }
            return false;
        }
        Map oldMap = (Map) originalMap;
        Map map = (Map) newMap;
        for (int i = 0; i < split.length - 1; i++) {
            Object item = oldMap.get(split[i]);
            if (item instanceof Map) {
                oldMap = (Map) item;
                if (!map.containsKey(split[i])) {
                    map.put(split[i], new HashMap<>());
                }
                map = (Map) map.get(split[i]);
            } else if (item instanceof List) {
                if (!map.containsKey(split[i])) {
                    map.put(split[i], new ArrayList<>());
                }
                List newObjectList = (List) map.get(split[i]);
                Object newValue;
                List list = (List) item;
                for (int j = 0; j < list.size(); j++) {
                    Object t = list.get(j);
                    if (newObjectList.size() <= j) {
                        if (t instanceof Map) {
                            newValue = new HashMap<>();
                        } else {
                            newValue = new ArrayList<>();
                        }
                        newObjectList.add(newValue);
                    } else {
                        newValue = newObjectList.get(j);
                    }
                    if (!putMapValue(t, newValue, getKey(split, i), value)) continue;
                    return true;
                }
                return false;
            }
        }
        if (map.containsKey(split[split.length - 1])) {
            return false;
        }
        map.put(split[split.length - 1], value);
        return true;
    }

    public static Map<String, Object> merge(Map<String, Object> currMap, Map<String, Object> body) {
        if (body == null) {
            return currMap;
        }
        for (Map.Entry<String, Object> stringObjectEntry : body.entrySet()) {
            if (stringObjectEntry.getValue() instanceof Map) {
                merge((Map) stringObjectEntry.getValue(), (Map) body.get(stringObjectEntry.getKey()));
                continue;
            }
            currMap.put(stringObjectEntry.getKey(), stringObjectEntry.getValue());
        }
        return currMap;
    }
}
