package kacper.barszczewski;

import kacper.barszczewski.validators.CustomRequestMappingHandlerAdapter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.validation.Validator;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class CustomWebMvcConfigurationSupport extends WebMvcConfigurationSupport {

    @NotNull
    @Override
    @Bean
    @Primary
    public RequestMappingHandlerAdapter requestMappingHandlerAdapter(
            @NotNull @Qualifier("mvcContentNegotiationManager") ContentNegotiationManager contentNegotiationManager,
            @NotNull @Qualifier("mvcConversionService") FormattingConversionService conversionService,
            @NotNull @Qualifier("mvcValidator") Validator validator) {
        return super.requestMappingHandlerAdapter(contentNegotiationManager, conversionService, validator);
    }

    @NotNull
    @Override
    protected RequestMappingHandlerAdapter createRequestMappingHandlerAdapter() {
        return new CustomRequestMappingHandlerAdapter();
    }

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("kacper.barszczewski.controllers"))
                .build();
    }
}
