package kacper.barszczewski.pojo;


import kacper.barszczewski.models.UserM;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserForm implements Serializable {

    private static final long serialVersionUID = -6589742347852367845L;

    @Size(min = 6, max = 20)
    private String firstName;

    @Size(min = 6, max = 35)
    private String lastName;

    private String type;

    private String status;

    @Email
    private String email;

    public void modifyModel(UserM user) {
        if (firstName != null) {
            user.setFirstName(firstName);
        }
        if (lastName != null) {
            user.setLastName(lastName);
        }
        if (email != null) {
            user.setEmail(email);
        }
        UserM.UserType typeEnum = getEnumValue(UserM.UserType.class, type);
        if (typeEnum != null) {
            user.setType(typeEnum);
        }
        UserM.UserStatus statusEnum = getEnumValue(UserM.UserStatus.class, status);
        if (status != null) {
            user.setStatus(statusEnum);
        }
    }

    private <T> T getEnumValue(Class<?> userStatusClass, String enumValue) {
        Object[] enumConstants = userStatusClass.getEnumConstants();
        for (Object enumConstant : enumConstants) {
            if (enumConstant.toString().equalsIgnoreCase(enumValue)) {
                return (T) enumConstant;
            }
        }
        return null;
    }
}
