package kacper.barszczewski.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import kacper.barszczewski.models.UserM;
import kacper.barszczewski.pojo.resource.AbstractPojoObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserObject extends AbstractPojoObject<UserM> implements Serializable {

    private String username;

    private Date registrationDate;

    private String firstName;

    private String lastName;

    private String email;

    private String status;

    private String type;

    public UserObject(UserM user) {
        fromObject(user);
    }

    public static UserObject wrapDetailsUser(UserM entity) {
        UserObject object = new UserObject();
        object.setUsername(entity.getUsername());
        object.setRegistrationDate(entity.getInfoCD());
        object.setFirstName(entity.getFirstName());
        object.setLastName(entity.getLastName());
        object.setEmail(entity.getEmail());
        object.setStatus(entity.getStatus().name());
        object.setType(entity.getType().name());
        object.fromObject(entity);
        return object;
    }

    public static UserObject wrapListUser(UserM entity) {
        UserObject userObject = wrapDetailsUser(entity);
        userObject.setRegistrationDate(null);
        return userObject;
    }

    public void fromObject(UserM user) {
        setUsername(user.getUsername());
        setEmail(user.getEmail());
    }

}
