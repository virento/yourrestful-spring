package kacper.barszczewski.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude()
public class ValidationError {

    private String field;
    private ErrorCode errorCode;
    private String message;

    public ValidationError(String field, ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.field = field;
        this.message = errorCode.getMessage();
    }

    public ValidationError(String field, String defaultMessage) {
        this.message = defaultMessage;
        this.field = field;
        this.errorCode = ErrorCode.VALIDATION_ERROR;
    }
}
