package kacper.barszczewski.pojo.authentication;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class NewPasswordForm {

    @NotNull
    @Size(min = 6, max = 35)
    private String password;

}
