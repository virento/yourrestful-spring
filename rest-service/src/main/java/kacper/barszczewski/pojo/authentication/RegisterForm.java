package kacper.barszczewski.pojo.authentication;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RegisterForm implements Serializable {

    private static final long serialVersionUID = -1770532587697862160L;

    @NotNull
    @Size(min = 6, max = 20)
    private String username;

    @NotNull
    @Size(min = 6, max = 35)
    private String password;

    @NotNull
    @Email
    private String email;

    @Size(max = 100)
    private String name;

    public String getSurname() {
        return null;
    }
}
