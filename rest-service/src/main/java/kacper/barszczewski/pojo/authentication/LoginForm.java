package kacper.barszczewski.pojo.authentication;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LoginForm implements Serializable {

    private static final long serialVersionUID = -2899029572185252118L;

    @NotNull
    private String login;

    @NotNull
    private String password;

    public String getAsString() {
        if (login == null || password == null) {
            return "";
        }
        return login + ":" + password;
    }
}
