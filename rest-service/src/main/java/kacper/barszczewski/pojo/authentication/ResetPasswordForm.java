package kacper.barszczewski.pojo.authentication;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ResetPasswordForm {

    @NotNull
    @Email
    private String email;

}
