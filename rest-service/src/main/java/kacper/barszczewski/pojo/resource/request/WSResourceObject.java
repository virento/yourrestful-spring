package kacper.barszczewski.pojo.resource.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.controllers.exceptions.ErrorCode;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.structure.StructureParser;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class WSResourceObject implements Serializable {

    @NotBlank
    @Length(min = 1, max = 50)
    private String title;

    @NotNull
    private Map<String, Object> structure;

    private List<Map<String, Object>> records;

    @NotNull
    private Boolean active = false;

    @NotNull
    private Boolean editable = false;

    private WSResourceGeneration generation;

    public ResourceM toEntity(Long userId) throws ApplicationException {
        ResourceM resource = new ResourceM();
        resource.setActive(true);
        resource.setOwnerId(userId);
        resource.setType(ResourceM.ResourceType.PRIVATE);
        resource.setName(getTitle());
        resource.setActive(getActive() != null && getActive());
        resource.setEditable(getEditable() != null && getEditable());
        if (getStructure() != null) {
            if (!getStructure().containsKey("id")) {
                getStructure().put("id", "Long");
            }
            try {
                resource.setStructure(new ObjectMapper().writeValueAsString(structure));
            } catch (JsonProcessingException e) {
                throw new ApplicationException(ErrorCode.WRONG_RESOURCE_STRUCTURE);
            }
            if (getRecords() != null) {
                StructureParser parser = new StructureParser(resource.getStructure());
                parser.validateRecords(getRecords());
            }
        }


        return resource;
    }
}
