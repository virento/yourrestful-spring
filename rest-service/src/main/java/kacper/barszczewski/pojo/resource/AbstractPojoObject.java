package kacper.barszczewski.pojo.resource;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import java.util.Arrays;

public abstract class AbstractPojoObject<T> extends RepresentationModel<AbstractPojoObject<T>> {

    public abstract void fromObject(T object);

    public AbstractPojoObject() {

    }

    public AbstractPojoObject(T entity, Link... links) {
        super(Arrays.asList(links));
        if (entity != null) {
            fromObject(entity);
        }
    }
}
