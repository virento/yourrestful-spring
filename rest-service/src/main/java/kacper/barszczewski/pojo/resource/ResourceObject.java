package kacper.barszczewski.pojo.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import kacper.barszczewski.models.ResourceM;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.Relation;

import java.util.Date;

@Getter
@Setter
@Relation(itemRelation = "resource", collectionRelation = "resources")
@AllArgsConstructor
@NoArgsConstructor
public class ResourceObject extends AbstractPojoObject<ResourceM> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String title;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String url;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date createDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date modificationDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean active;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean editable;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String structure;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer generated;

    public ResourceObject(ResourceM entity, Link... links) {
        super(entity, links);
    }

    public static ResourceObject wrapStructureOnly(ResourceM entity) {
        ResourceObject object = new ResourceObject();
        object.setStructure(entity.getStructureAsJson());
        return object;
    }

    @Override
    public void fromObject(ResourceM object) {
        setTitle(object.getName());
        setUrl(object.getUrl());
        setCreateDate(object.getInfoCD());
        setActive(object.getActive() == null ? false : object.getActive());
        setEditable(object.getEditable() == null ? false : object.getEditable());
        setModificationDate(object.getInfoMD());
        setGenerated(object.getGeneratedRecords());
    }
}
