package kacper.barszczewski.pojo.resource;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import kacper.barszczewski.models.ResourceRecordM;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.Relation;

import java.util.Map;

@Getter
@Setter
@Relation(itemRelation = "record", collectionRelation = "records")
public class RecordObject extends AbstractPojoObject<ResourceRecordM> {

    @JsonIgnore
    private Map<String, Object> values;

    public RecordObject() {

    }

    public RecordObject(ResourceRecordM entity, Link... links) {
        super(entity, links);
    }

    public static RecordObject wrap(ResourceRecordM record) {
        RecordObject recordObject = new RecordObject();
        recordObject.fromObject(record);
        return recordObject;
    }

    @Override
    public void fromObject(ResourceRecordM object) {
        this.values = object.getParsedValue();
    }

    @JsonAnyGetter
    private Map<String, Object> getMapContent() {
        return values;
    }

    @JsonAnySetter
    private void setPropertiesAsMap(String key, Object value) {
        values.put(key, value);
    }
}
