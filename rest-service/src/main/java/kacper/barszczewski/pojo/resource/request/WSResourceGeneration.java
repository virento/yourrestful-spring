package kacper.barszczewski.pojo.resource.request;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@Setter
public class WSResourceGeneration {

    @NotNull
    @Min(0)
    private int generate;

    @NotNull
    private Map<String, Object> content;
}
