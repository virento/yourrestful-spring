package kacper.barszczewski.pojo.resource.request;

import kacper.barszczewski.adnotation.validators.MultiFieldNotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@MultiFieldNotNull(fields = {"nick", "email"})
public class WSResourceShare implements Serializable {

    private static final long serialVersionUID = 2808231335634500080L;

    private String nick;

    private String email;

}
