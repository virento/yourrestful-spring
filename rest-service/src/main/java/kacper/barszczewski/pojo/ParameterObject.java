package kacper.barszczewski.pojo;

import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.models.assemblers.AssemblerObject;
import kacper.barszczewski.pojo.resource.AbstractPojoObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.Relation;

@Getter
@Setter
@Relation(itemRelation = "parameter", collectionRelation = "parameters")
@AllArgsConstructor
@NoArgsConstructor
public class ParameterObject extends AbstractPojoObject<ParameterM> {

    private String code;

    private String value;

    public ParameterObject(ParameterM entity, Link... links) {
        super(entity, links);
    }

    @Override
    public void fromObject(ParameterM object) {
        setCode(object.getCode());
        setValue(object.getValue());
    }
}
