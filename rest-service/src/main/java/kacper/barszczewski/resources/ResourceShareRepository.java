package kacper.barszczewski.resources;

import kacper.barszczewski.models.ResourceShareM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ResourceShareRepository extends JpaRepository<ResourceShareM, Long> {

    @Query("SELECT CASE WHEN COUNT(id) > 0 THEN true ELSE false END FROM ResourceShareM WHERE userId = ?1 AND resource.url = ?2")
    Boolean haveAccess(Long userId, String resourceUrl);

    @Query("SELECT data FROM ResourceShareM data WHERE data.resourceId = ?1 AND (data.user.username = ?2 OR data.user.email = ?3)")
    ResourceShareM getByUserNickOrEmail(Long resourceId, String nick, String email);

}
