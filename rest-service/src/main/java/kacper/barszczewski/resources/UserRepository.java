package kacper.barszczewski.resources;

import kacper.barszczewski.models.UserM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<UserM, Long>, JpaSpecificationExecutor<UserM> {

    UserM getUserMByEmailOrUsername(String email, String username);

    Boolean existsByEmailOrUsername(String email, String username);

    Boolean existsByEmail(String email);

    Boolean existsByUsername(String username);

    @Query("SELECT data.user FROM ResourceShareM data WHERE data.resourceId = ?1 order by data.infoCD")
    Page<UserM> findAllByShareResourceUrl(Long resourceUrl, Pageable pageable);


    /**
     * SPECIFICATIONS
     */

    static Specification<UserM> fieldContains(String field, String value) {
        return (root, query, cb) -> cb.like(root.get(field), "%" + value + "%");
    }

    static Specification<UserM> enumEqual(String field, Object enumValue) {
        return (root, query, cb) -> cb.equal(root.get(field), enumValue);
    }
}
