package kacper.barszczewski.resources;

import kacper.barszczewski.models.ResourceM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<ResourceM, Long> {

    @Query(value = "SELECT data FROM ResourceM data WHERE data.type = :type AND data.infoRD IS NULL")
    ResourceM getUniversal(@Param("type") ResourceM.ResourceType type);

    @Query("select data from ResourceM data where data.ownerId = ?1 and data.infoRD is null order by data.infoCD")
    Page<ResourceM> findAllByOwnerId(Long ownerId, Pageable pageable);

    @Query("select data from ResourceM data where data.url = ?1 and data.infoRD is null")
    ResourceM getByUrl(String url);

    Boolean existsByNameAndOwnerIdAndInfoRDIsNull(String name, Long ownerId);

    ResourceM findByOwnerIdAndName(Long ownerId, String name);

    @Query("SELECT ownerId FROM ResourceM WHERE url = ?1")
    Long getResourceOwner(String resourceUrl);

    int countResourceMByOwnerIdAndInfoRDIsNull(Long ownerId);
}
