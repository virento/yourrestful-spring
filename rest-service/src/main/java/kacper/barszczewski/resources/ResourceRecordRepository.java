package kacper.barszczewski.resources;

import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.persistence.ResourceRecordId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRecordRepository extends JpaRepository<ResourceRecordM, ResourceRecordId> {

    Page<ResourceRecordM> findAllByResourceIdOrderById(Long resourceId, Pageable pageable);

    @Query("SELECT CASE WHEN COUNT(data.id) > 0 THEN true ELSE false END from ResourceRecordM data where data.infoRD is null AND data.resourceId = ?1")
    boolean isResourceRecordsExists(Long resourceId);

    int countResourceRecordMByResourceIdAndInfoRDIsNull(Long resourceId);

}
