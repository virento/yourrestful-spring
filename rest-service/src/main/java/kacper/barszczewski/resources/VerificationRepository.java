package kacper.barszczewski.resources;

import kacper.barszczewski.models.VerificationM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface VerificationRepository extends JpaRepository<VerificationM, Long> {

    @Query(value = "SELECT data FROM VerificationM data WHERE data.userId = ?1 AND data.type = ?2")
    VerificationM getVerification(Long userId, VerificationM.Type type);
}
