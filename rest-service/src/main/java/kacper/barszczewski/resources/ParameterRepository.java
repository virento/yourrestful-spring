package kacper.barszczewski.resources;

import kacper.barszczewski.models.ParameterM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ParameterRepository extends JpaRepository<ParameterM, Long> {

    @Query(value = "select CAST(spr_value AS varchar) from sys_parameters where spr_code = ?1", nativeQuery = true)
    String getParameterValue(String code);

    ParameterM getParameterMByCode(String code);
}
