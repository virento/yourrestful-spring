package kacper.barszczewski.models.result;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ResultList<T> extends ArrayList<T> {

    private Long totalSize;
    private Long page;
    private Long totalPage;

    public ResultList(List<T> content) {
        super(content);
    }

    public ResultList() {
        super();
    }

    public static <T> List<T> wrap(Page<T> content) {
        ResultList<T> resultList = new ResultList<>(content.getContent());
        resultList.setTotalSize(content.getTotalElements());
        resultList.setPage((long) content.getNumber());
        resultList.setTotalPage((long) content.getTotalPages());
        return resultList;
    }

    public static <T> ResultList<T> wrap(List<T> collect) {
        return new ResultList<>(collect);
    }

    public Long getTotalPage() {
        if (totalPage == null) {
            totalPage = size() / getTotalSize();
        }
        return totalPage;
    }

    public Long getTotalSize() {
        if (totalSize == null) {
            setTotalSize((long) size());
        }
        return totalSize;
    }
}
