package kacper.barszczewski.models;

import kacper.barszczewski.controllers.exceptions.ProgramException;
import kacper.barszczewski.models.persistence.ResourceRecordId;
import kacper.barszczewski.utils.Utils;
import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

@Getter
@Setter
@Entity
@Table(name = "usr_resources_records")
@IdClass(ResourceRecordId.class)
public class ResourceRecordM extends AbstractModel {

    private static final long serialVersionUID = 6193614261291738880L;

    @Id
    @Column(name = "urr_id", nullable = false)
    private Long id;

    @Column(name = "urr_ure_id")
    @Id
    private Long resourceId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "urr_info_cd", nullable = false)
    private Date infoCD;

    @Column(name = "urr_info_cu")
    private Long infoCU;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "urr_info_md", nullable = false)
    private Date infoMD;

    @Column(name = "urr_info_mu")
    private Long infoMU;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "urr_info_rd", nullable = false)
    private Date infoRD;

    @Column(name = "urr_info_ru")
    private Long infoRU;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "urr_ure_id", referencedColumnName = "ure_id", insertable = false, updatable = false)
    private ResourceM resource;

    @Column(name = "urr_value")
    private String value;

    @Transient
    private Map<String, Object> parsedValue;

    @PrePersist
    public void prePersist() {
        if (getInfoCD() == null) {
            setInfoCD(new Date());
        }
    }

    @PreUpdate
    public void preUpdate() {
        if (getInfoCD() == null) {
            setInfoCD(new Date());
        }
        setInfoMD(new Date());
    }

    public Map<String, Object> merge(Map<String, Object> body) {
        if (value == null) {
            return body;
        }
        Map<String, Object> currMap;
        try {
            currMap = new JSONParser(value).object();
        } catch (ParseException e) {
            e.printStackTrace();
            throw new ProgramException(e.getMessage());
        }
        return Utils.merge(currMap, body);
    }
}
