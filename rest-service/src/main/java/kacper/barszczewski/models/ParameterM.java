package kacper.barszczewski.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@Entity
@ToString
@Table(name = "sys_parameters")
@SequenceGenerator(name = "seq_spr_id", sequenceName = "seq_spr_id")
public class ParameterM extends AbstractModel implements CodeModel {

    private static final long serialVersionUID = -5926354325324124152L;

    @Id
    @Column(name = "spr_Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_spr_id")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "spr_info_cd", nullable = false)
    private Date infoCD;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "spr_info_md")
    private Date infoMD;

    @Column(name = "spr_code", nullable = false)
    private String code;

    @Column(name = "spr_value", nullable = false)
    private String value;

    @Column(name = "spr_public", nullable = false)
    private Boolean isPublic;

}
