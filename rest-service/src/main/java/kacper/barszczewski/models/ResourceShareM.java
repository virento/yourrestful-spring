package kacper.barszczewski.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "usr_resources_share")
@SequenceGenerator(name = "seq_urs_id", sequenceName = "seq_urs_id")
public class ResourceShareM extends AbstractModel {

    private static final long serialVersionUID = 5610333969033719003L;

    @Id
    @Column(name = "urs_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_urs_id")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "urs_info_cd")
    private Date infoCD;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "urs_sur_id", referencedColumnName = "sur_id", insertable = false, updatable = false)
    private UserM user;

    @Column(name = "urs_sur_id")
    private Long userId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "urs_ure_id", referencedColumnName = "ure_id", insertable = false, updatable = false)
    private ResourceM resource;

    @Column(name = "urs_ure_id")
    private Long resourceId;

    @PrePersist
    public void prePersist() {
        setInfoCD(new Date());
    }
}
