package kacper.barszczewski.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sys_verification")
@SequenceGenerator(name = "seq_svr_id", sequenceName = "seq_svr_id")
public class VerificationM {

    private static final long serialVersionUID = -5432547893253424312L;

    public enum Type {
        EMAIL, PASSWORD
    }

    public enum Status {
        NEW, USED
    }

    @Id
    @Column(name = "svr_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sur_id")
    private Long id;

    @Column(name = "svr_info_cd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date infoCD;

    @Column(name = "svr_info_md")
    @Temporal(TemporalType.TIMESTAMP)
    private Date infoMD;

    @Column(name = "svr_sur_id")
    private Long userId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "svr_sur_id", referencedColumnName = "sur_id", insertable = false, updatable = false)
    private UserM user;

    @Enumerated(EnumType.STRING)
    @Column(name = "svr_type", length = 10, nullable = false)
    private Type type;

    @Column(name = "svr_value", nullable = false)
    private String value;

    @Column(name = "svr_status", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "svr_token_exp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @PrePersist
    public void prePersist() {
        if (getInfoCD() == null) {
            setInfoCD(new Date());
        }
    }

    @PreUpdate
    public void preUpdate() {
        setInfoMD(new Date());
    }
}
