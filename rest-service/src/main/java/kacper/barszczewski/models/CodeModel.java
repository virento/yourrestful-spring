package kacper.barszczewski.models;

public interface CodeModel extends IdModel {

    String getCode();
}
