package kacper.barszczewski.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sys_users")
@SequenceGenerator(name = "seq_sur_id", sequenceName = "seq_sur_id")
public class UserM extends AbstractModel implements UserDetails {

    private static final long serialVersionUID = -5803839491082533770L;

    @SuppressWarnings("unused")
    public enum UserStatus {
        NEW, VERIFIED, ACTIVE, BANNED
    }

    @SuppressWarnings("unused")
    public enum UserType {
        USER, ADMIN
    }

    @Id
    @Column(name = "sur_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sur_id")
    private Long id;

    @Column(name = "sur_info_cd", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date infoCD;

    @Column(name = "sur_info_md")
    @Temporal(TemporalType.TIMESTAMP)
    private Date infoMD;

    @Column(name = "sur_info_rd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date infoRD;

    @Column(name = "sur_username", length = 50, nullable = false)
    private String username;

    @Column(name = "sur_firstname", length = 100)
    private String firstName;

    @Column(name = "sur_lastname", length = 100)
    private String lastName;

    @Column(name = "sur_password", nullable = false)
    private String password;

    @Column(name = "sur_email", nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "sur_status", length = 10, nullable = false)
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "sur_type", length = 10, nullable = false)
    private UserType type;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !UserStatus.BANNED.equals(getStatus());
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return UserStatus.ACTIVE.equals(getStatus());
    }

    @PrePersist
    public void prePersist() {
        if (getInfoCD() == null) {
            setInfoCD(new Date());
        }
    }

    @PreUpdate
    public void preUpdate() {
        setInfoMD(new Date());
    }
}
