package kacper.barszczewski.models.persistence;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ResourceRecordId implements Serializable {

    private static final long serialVersionUID = 8578745475445176480L;

    private Long resourceId;
    private Long id;

}
