package kacper.barszczewski.models.assemblers;

import kacper.barszczewski.controllers.BasicController;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.pojo.resource.RecordObject;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ResourceRecordModelAssembler extends CommonModelAssembler<ResourceRecordM, RecordObject> {

    @SneakyThrows
    @Override
    protected Link[] getObjectLink(ResourceRecordM object) {
        return new Link[]{WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(BasicController.class).getData(object.getId())).withSelfRel()};
    }

    @SneakyThrows
    @Override
    protected Link[] getAllObjectsLink(@Nullable ResourceRecordM object) {
        return new Link[]{WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(BasicController.class).getDataAll(null)).withRel("all")};
    }

    @Override
    protected RecordObject getNewObject(ResourceRecordM entity) {
        return new RecordObject(entity);
    }
}
