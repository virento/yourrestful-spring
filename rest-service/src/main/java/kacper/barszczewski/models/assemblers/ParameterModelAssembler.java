package kacper.barszczewski.models.assemblers;

import kacper.barszczewski.controllers.SystemController;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ParameterM;
import kacper.barszczewski.pojo.ParameterObject;
import org.jetbrains.annotations.Nullable;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ParameterModelAssembler extends CommonModelAssembler<ParameterM, ParameterObject> {

    @Override
    protected Link[] getObjectLink(ParameterM object) throws ApplicationException {
        return new Link[]{
                linkTo(methodOn(SystemController.class).getSystemParameter(object.getCode())).withSelfRel()
        };
    }

    @Override
    protected Link[] getAllObjectsLink(@Nullable ParameterM object) {
        return new Link[]{
                linkTo(methodOn(SystemController.class).getSystemParameters(null)).withRel("all")
        };
    }

    @Override
    protected ParameterObject getNewObject(ParameterM entity) {
        return new ParameterObject(entity);
    }
}
