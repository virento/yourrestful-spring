package kacper.barszczewski.models.assemblers;

import kacper.barszczewski.models.UserM;
import kacper.barszczewski.pojo.UserObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

@Component
public class UserModelAssembler extends CommonModelAssembler<UserM, UserObject> {

    public enum Variant {
        DETAILS, LIST
    }

    @Override
    protected Link[] getObjectLink(UserM object) {
        return new Link[0];
    }

    @Override
    protected Link[] getAllObjectsLink(@Nullable UserM object) {
        return new Link[0];
    }

    @Override
    protected UserObject getNewObject(UserM entity) {
        return new UserObject(entity);
    }

    @Override
    protected UserObject getNewObject(UserM entity, @NotNull Enum variant) {
        if (variant.equals(Variant.DETAILS)) {
            return UserObject.wrapDetailsUser(entity);
        } else if (variant.equals(Variant.LIST)) {
            return UserObject.wrapListUser(entity);
        } else {
            return super.getNewObject(entity, variant);
        }
    }
}
