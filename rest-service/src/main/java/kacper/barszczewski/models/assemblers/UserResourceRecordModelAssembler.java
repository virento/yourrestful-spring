package kacper.barszczewski.models.assemblers;

import kacper.barszczewski.controllers.UserController;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.assemblers.helper.UserResourceHelper;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.pojo.resource.RecordObject;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserResourceRecordModelAssembler extends CommonModelAssembler<UserResourceHelper, RecordObject> {

    @SneakyThrows
    @Override
    protected Link[] getObjectLink(UserResourceHelper object) {
        return new Link[]{linkTo(methodOn(UserController.class).getResourceRecord(object.getResource().getUrl(), object.getId())).withSelfRel()};
    }

    @SneakyThrows
    @Override
    protected Link[] getAllObjectsLink(@Nullable UserResourceHelper object) {
        if (object == null) {
            return null;
        }
        return new Link[]{linkTo(methodOn(UserController.class).getResourceRecords(object.getResource().getUrl(), null)).withRel("all")};
    }

    @Override
    protected RecordObject getNewObject(UserResourceHelper entity) {
        return new RecordObject(entity.getRecord());
    }

    @SneakyThrows
    public CollectionModel<RecordObject> toPageCollection(ResourceM resource, List<ResourceRecordM> records) {
        List<UserResourceHelper> collect = records.stream()
                .map(resourceRecordM -> new UserResourceHelper(resource, resourceRecordM))
                .collect(Collectors.toList());
        CollectionModel<RecordObject> recordObject = null;
        if (records instanceof ResultList) {
            ResultList<ResourceRecordM> recordsResultList = (ResultList<ResourceRecordM>) records;
            ResultList<UserResourceHelper> resultList = ResultList.wrap(collect);
            resultList.setTotalSize(recordsResultList.getTotalSize());
            resultList.setPage(recordsResultList.getPage());
            resultList.setTotalPage(recordsResultList.getTotalPage());
            recordObject = toPageCollection(resultList);
        }
        if (recordObject == null) {
            recordObject = toPageCollection(collect);
        }
        recordObject.add(linkTo(methodOn(UserController.class).getResource(resource.getUrl())).withRel("resource"));
        return recordObject;
    }

    public ResponseEntity<RecordObject> toOkModel(ResourceM resource, ResourceRecordM record) {
        return toOkModel(new UserResourceHelper(resource, record));
    }

    public ResponseEntity<RecordObject> toCreateModel(ResourceM resource, ResourceRecordM resourceRecordM) {
        return toCreateModel(new UserResourceHelper(resource, resourceRecordM));
    }
}
