package kacper.barszczewski.models.assemblers;

import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.result.ResultList;
import kacper.barszczewski.pojo.resource.AbstractPojoObject;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class CommonModelAssembler<T extends AssemblerObject, N extends AbstractPojoObject<?>> implements RepresentationModelAssembler<T, N> {

    @NotNull
    @Override
    public N toModel(@Nullable T entity) {
        return toModel(entity, null);
    }

    @SneakyThrows
    @NotNull
    public N toModel(@Nullable T entity, Enum variant) {
        N object;
        if (variant == null) {
            object = getNewObject(entity);
        } else {
            object = getNewObject(entity, variant);
        }
        object.add(getAllObjectsLink(entity));
        if (entity == null) {
            return object;
        }
        if (entity.getId() != null) {
            object.add(getObjectLink(entity));
        }
        return object;
    }

    public ResponseEntity<N> toOkModel(T entity, Enum variant) {
        N object;
        if (variant != null) {
            object = toModel(entity, variant);
        } else {
            object = toModel(entity);
        }
        return ResponseEntity.ok(object);
    }

    public ResponseEntity<N> toOkModel(T entity) {
        return toOkModel(entity, null);
    }

    @SneakyThrows
    public CollectionModel<N> toPageCollection(List<T> entities, Enum variant) {
        List<N> objects = new ArrayList<>();
        for (T entity : entities) {
            N object;
            if (variant == null) {
                object = getNewObject(entity);
            } else {
                object = getNewObject(entity, variant);
            }
            object.add(getObjectLink(entity)); // add links
            objects.add(object);
        }
        if (entities instanceof ResultList) {
            ResultList<T> resultList = (ResultList<T>) entities;
            PagedModel<N> ns = new PagedModel<>(objects,
                    new PagedModel.PageMetadata(entities.size(), resultList.getPage(), resultList.getTotalSize(), resultList.getTotalPage()));
            if (entities.size() > 0) {
                ns.add(getAllObjectsLink(entities.get(0)));
            }
            return ns;
        }
        return new PagedModel<>(Collections.emptyList(),
                new PagedModel.PageMetadata(entities.size(), 0, entities.size(), 1));
    }

    @SneakyThrows
    public CollectionModel<N> toPageCollection(List<T> entities) {
        return toPageCollection(entities, null);
    }

    public ResponseEntity<N> toCreateModel(T entity) {
        N object = toModel(entity);
        if (entity.getId() == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(object);
        }
        return ResponseEntity.created(object.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(object);
    }

    /**
     * Link to object {@code object}
     *
     * @param object Object to which new link will be pointing
     * @return Link to object {@code object}
     */
    protected abstract Link[] getObjectLink(T object) throws ApplicationException;

    /**
     * Return link to all collection which contains all entities
     *
     * @param object Null if no object is related to list
     * @return Link to collection
     */
    protected abstract Link[] getAllObjectsLink(@Nullable T object);

    /**
     * Create new object and return new instance for every request
     *
     * @param entity Entity that should be represented by new object
     * @return new object instance
     */
    protected abstract N getNewObject(T entity);

    protected N getNewObject(T entity, @NotNull Enum variant) {
        return getNewObject(entity);
    }

}
