package kacper.barszczewski.models.assemblers;

import kacper.barszczewski.controllers.UserController;
import kacper.barszczewski.controllers.exceptions.ApplicationException;
import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.pojo.resource.ResourceObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ResourceModelAssembler extends CommonModelAssembler<ResourceM, ResourceObject> {

    public enum Variant {
        RESOURCE_ONLY
    }

    @Override
    protected Link[] getObjectLink(ResourceM object) throws ApplicationException {
        return new Link[]{WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getResource(object.getUrl())).withSelfRel(),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getResourceRecords(object.getUrl(), null)).withRel(IanaLinkRelations.CONTENTS)};
    }

    @Override
    protected Link[] getAllObjectsLink(@Nullable ResourceM object) {
        return new Link[]{WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getResources(null)).withRel("all")};
    }

    @Override
    protected ResourceObject getNewObject(ResourceM entity) {
        return new ResourceObject(entity);
    }

    @Override
    protected ResourceObject getNewObject(ResourceM entity, @NotNull Enum variant) {
        Variant variantEnum = (Variant) variant;
        switch (variantEnum) {
            case RESOURCE_ONLY:
                return ResourceObject.wrapStructureOnly(entity);
        }
        return super.getNewObject(entity, variant);
    }
}
