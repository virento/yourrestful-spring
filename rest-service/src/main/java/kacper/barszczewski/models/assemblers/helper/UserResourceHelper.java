package kacper.barszczewski.models.assemblers.helper;

import kacper.barszczewski.models.ResourceM;
import kacper.barszczewski.models.ResourceRecordM;
import kacper.barszczewski.models.assemblers.AssemblerObject;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class UserResourceHelper implements AssemblerObject {

    private ResourceM resource;
    private ResourceRecordM record;

    @Override
    public Long getId() {
        return record.getId();
    }
}
