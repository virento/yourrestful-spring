package kacper.barszczewski.models;

import kacper.barszczewski.services.MyCacheImpl;
import kacper.barszczewski.services.iterfaces.MyCache;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Arrays;
import java.util.List;

public enum Parameters {

    MAX_RECORDS(Integer.class),
    MAX_RESOURCES(Integer.class),
    VERIFICATION_MESSAGE(String.class),
    VERIFICATION_TITLE(String.class),
    PASSWORD_VERIFICATION_TITLE(String.class),
    PASSWORD_VERIFICATION_MESSAGE(String.class),
    MESSAGE_FROM_NAME(String.class),
    RANDOM_TEXT(List.class),
    CACHE_SIZE(10, Integer.class),
    STRUCTURE_MAX_NESTING(10, Integer.class),
    STRUCTURE_MAX_FIELDS(10, Integer.class),
    MAIL_HOST(String.class),
    MAIL_HOST_PORT(String.class),
    MAIL_SOCKET_FACTORY_PORT(String.class),
    MAIL_SOCKET_FACTORY_CLASS(String.class),
    MAIL_EMAIL(String.class),
    MAIL_PASSWORD(String.class);

    private Class<?> clazz;
    private String defaultValue;

    Parameters(Object defaultValue, Class<?> clazz) {
        this(clazz);
        this.defaultValue = String.valueOf(defaultValue);
    }

    Parameters(Class<?> clazz) {
        this.clazz = clazz;
    }

    public <T> T getValue() {
        ParameterM parameter = getParameter();
        if (parameter == null) {
            return getValue(defaultValue);
        }
        return getValue(parameter.getValue());
    }

    public <T> T getValue(String textValue) {
        if (clazz == List.class) {
            return (T) Arrays.asList(textValue.split(";"));
        }
        if (String.class.equals(clazz)) {
            return (T) textValue;
        }
        if (Integer.class.equals(clazz)) {
            return (T) (Integer) Integer.parseInt(textValue);
        }
        throw new UnsupportedOperationException("Can't cast parameter to " + clazz);
    }

    public boolean validateValue(String value) {
        if (clazz == List.class) {
            return true;
        }
        if (String.class.equals(clazz)) {
            return true;
        }
        if (Integer.class.equals(clazz)) {
            return NumberUtils.isParsable(value);
        }
        return false;
    }

    public <T> T getParameter() {
        return (T) MyCacheImpl.getDomainValue(MyCache.Domain.PARAMETER, name()).orElse(null);
    }

    public static Parameters get(String code) {
        if (code == null) {
            return null;
        }
        for (Parameters value : Parameters.values()) {
            if (code.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }

    public String getType() {
        if (clazz == List.class) {
            return "list";
        }
        if (String.class.equals(clazz)) {
            return "text";
        }
        if (Integer.class.equals(clazz)) {
            return "number";
        }
        return "undefined";
    }
}
