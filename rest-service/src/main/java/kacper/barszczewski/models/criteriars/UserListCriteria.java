package kacper.barszczewski.models.criteriars;

import kacper.barszczewski.models.UserM;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import static kacper.barszczewski.resources.UserRepository.enumEqual;
import static kacper.barszczewski.resources.UserRepository.fieldContains;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserListCriteria extends ListSearchCriteria {

    private String name;

    private String surname;

    private String email;

    private String type;

    private String status;

    private String username;

    public Specification<UserM> buildSpecification() {
        Specification<UserM> specification;
        specification = addSpecification(null, name, "firstName");
        specification = addSpecification(specification, surname, "lastName");
        specification = addSpecification(specification, email, "email");
        specification = addEnumSpecification(specification, type, "type", UserM.UserType.class);
        specification = addEnumSpecification(specification, status, "status", UserM.UserStatus.class);
        specification = addSpecification(specification, username, "username");
        return specification;
    }

    private Specification<UserM> addEnumSpecification(Specification<UserM> specification, String value, String fieldName, Class<?> userTypeClass) {
        Object[] enumConstants = userTypeClass.getEnumConstants();
        for (Object enumConstant : enumConstants) {
            if (enumConstant.toString().equalsIgnoreCase(value)) {
                Specification<UserM> condition = enumEqual(fieldName, enumConstant);
                return specification == null ? condition : specification.and(condition);
            }
        }
        return specification;
    }

    private Specification<UserM> addSpecification(Specification<UserM> specification, String value, String fieldName) {
        if (value == null) {
            return specification;
        }
        Specification<UserM> condition = fieldContains(fieldName, value);
        return specification == null ? condition : specification.and(condition);
    }

    @Override
    public Pageable getPageable() {
        return PageRequest.of(getPage(), getMaxResult(), Sort.by("id"));
    }

}
