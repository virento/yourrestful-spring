package kacper.barszczewski.models.criteriars;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.ModelAttribute;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ListSearchCriteria {

    private Integer maxResult = 100;
    private Integer page = 0;


    @ModelAttribute("maxResult")
    public Integer getMaxResult() {
        return maxResult;
    }

    @ModelAttribute("page")
    public Integer getPage() {
        return page;
    }

    public Pageable getPageable() {
        return PageRequest.of(getPage(), getMaxResult());
    }
}
