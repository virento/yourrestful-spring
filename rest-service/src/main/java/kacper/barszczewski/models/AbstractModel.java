package kacper.barszczewski.models;

import kacper.barszczewski.models.assemblers.AssemblerObject;

import java.io.Serializable;

public abstract class AbstractModel implements Serializable, AssemblerObject {

    private static final long serialVersionUID = 3435260454215209863L;

    abstract public Long getId();

}
