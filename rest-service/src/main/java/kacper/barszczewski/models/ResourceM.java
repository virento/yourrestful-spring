package kacper.barszczewski.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "usr_resources")
@SequenceGenerator(name = "seq_ure_id", sequenceName = "seq_ure_id")
public class ResourceM extends AbstractModel implements CodeModel {

    private static final long serialVersionUID = 4473933235317570219L;

    @SuppressWarnings("unused")
    public enum ResourceType {
        PRIVATE, UNIVERSAL;
    }

    @Id
    @Column(name = "ure_Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ure_id")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ure_info_cd", nullable = false)
    private Date infoCD;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ure_info_md")
    private Date infoMD;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ure_info_rd")
    private Date infoRD;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ure_sur_id", referencedColumnName = "sur_id", insertable = false, updatable = false)
    private UserM owner;

    @Column(name = "ure_sur_id", nullable = false)
    private Long ownerId;

    @Enumerated(EnumType.STRING)
    @Column(name = "ure_type", nullable = false)
    private ResourceType type;

    @Column(name = "ure_structure", nullable = false)
    private String structure;

    @Column(name = "ure_active")
    private Boolean active = Boolean.FALSE;

    @Column(name = "ure_url", length = 50)
    private String url;

    @Column(name = "ure_name")
    private String name;

    @Column(name = "ure_editable")
    private Boolean editable;

    @Transient
    private Integer generatedRecords = null;

    @PrePersist
    public void prePersist() {
        if (getInfoCD() == null) {
            setInfoCD(new Date());
        }
        if (getUrl() == null) {
            setUrl(UUID.randomUUID().toString());
        }
    }

    @PreUpdate
    public void preUpdate() {
        setInfoMD(new Date());
    }

    public String getStructureAsJson() {
        return structure;
    }

    @Override
    public String getCode() {
        return url;
    }
}
