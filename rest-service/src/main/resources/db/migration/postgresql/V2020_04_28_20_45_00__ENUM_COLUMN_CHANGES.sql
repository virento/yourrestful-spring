ALTER TABLE SYS_USERS ALTER COLUMN SUR_STATUS TYPE VARCHAR(10);
ALTER TABLE SYS_USERS ALTER COLUMN SUR_TYPE TYPE VARCHAR(10);
ALTER TABLE sys_verification ALTER COLUMN SVR_STATUS TYPE VARCHAR(10);
ALTER TABLE SYS_VERIFICATION ALTER COLUMN SVR_TYPE TYPE VARCHAR(10);
ALTER TABLE USR_RESOURCES ALTER COLUMN URE_TYPE TYPE VARCHAR(10);