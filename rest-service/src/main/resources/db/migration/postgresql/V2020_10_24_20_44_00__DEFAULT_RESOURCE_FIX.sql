DELETE FROM USR_RESOURCES_RECORDS;

INSERT INTO USR_RESOURCES_RECORDS(urr_ure_id, urr_value)
VALUES ((SELECT URE_ID FROM USR_RESOURCES WHERE URE_TYPE = 'UNIVERSAL'),
        '{id: 1, name: "Janek", surname: "Jankowski", age: 20, paycheck: 20.22}');
INSERT INTO USR_RESOURCES_RECORDS(urr_ure_id, urr_value)
VALUES ((SELECT URE_ID FROM USR_RESOURCES WHERE URE_TYPE = 'UNIVERSAL'),
        '{id: 2, name: "Bartek", surname: "Barkowski", age: 10, paycheck: 20.00}');
INSERT INTO USR_RESOURCES_RECORDS(urr_ure_id, urr_value)
VALUES ((SELECT URE_ID FROM USR_RESOURCES WHERE URE_TYPE = 'UNIVERSAL'),
        '{id: 3, name: "Marek", surname: "Markowski", age: 9, paycheck: 20}');
INSERT INTO USR_RESOURCES_RECORDS(urr_ure_id, urr_value)
VALUES ((SELECT URE_ID FROM USR_RESOURCES WHERE URE_TYPE = 'UNIVERSAL'),
        '{id: 4, name: "Wartek", surname: "Wartkowski", age: 24, paycheck: 20.12}');
INSERT INTO USR_RESOURCES_RECORDS(urr_ure_id, urr_value)
VALUES ((SELECT URE_ID FROM USR_RESOURCES WHERE URE_TYPE = 'UNIVERSAL'),
        '{id: 5, name: "Kamil", surname: "Kamilowski", age: 50, paycheck: 200.22}');
INSERT INTO USR_RESOURCES_RECORDS(urr_ure_id, urr_value)
VALUES ((SELECT URE_ID FROM USR_RESOURCES WHERE URE_TYPE = 'UNIVERSAL'),
        '{id: 6, name: "Ola", surname: "Olowska", age: 60, paycheck: 0.22}');
