ALTER TABLE USR_RESOURCES_RECORDS DROP CONSTRAINT usr_resources_records_pkey;
ALTER TABLE usr_resources_records ADD CONSTRAINT usr_resources_records_pkey PRIMARY KEY (URR_URE_ID, URR_ID);