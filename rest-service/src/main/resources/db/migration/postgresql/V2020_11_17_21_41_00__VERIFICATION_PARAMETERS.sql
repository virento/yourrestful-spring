ALTER TABLE SYS_PARAMETERS ALTER COLUMN spr_value TYPE text;

INSERT INTO SYS_PARAMETERS(SPR_CODE, SPR_VALUE) VALUES ('VERIFICATION_MESSAGE', 'Welcome in Cinema Club!<br/>Please activate your account by clicking the following link:<br/>');
INSERT INTO SYS_PARAMETERS(SPR_CODE, SPR_VALUE) VALUES ('VERIFICATION_TITLE', 'Confirm your email');